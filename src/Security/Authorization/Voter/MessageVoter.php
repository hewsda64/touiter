<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Security\Authorization\Voter;

use Hewsda64\Touiter\Domain\Account\Command\ActivateAccount;
use Hewsda64\Touiter\Domain\Account\Command\ChangeLocalPassword;
use Hewsda64\Touiter\Domain\Account\Command\RegisterAccount;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Favorite\Command\AccountFavoriteTouit;
use Hewsda64\Touiter\Domain\Follower\Command\FollowAccount;
use Hewsda64\Touiter\Domain\Touit\Command\PostTouit;
use Hewsda64\Touiter\Domain\Touit\Command\ReplyToTouit;
use Illuminate\Contracts\Logging\Log;
use Thrust\Security\Authorization\Voter\Voter;
use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\User;

class MessageVoter extends Voter
{
    const MESSAGE_NAME = [
        PostTouit::class,
        ReplyToTouit::class,
        AccountFavoriteTouit::class,
        FollowAccount::class,
        RegisterAccount::class,
        ActivateAccount::class,
        ChangeLocalPassword::class
    ];

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * @var Log
     */
    private $log;

    /**
     * MessageVoter constructor.
     *
     * @param TrustResolver $trustResolver
     * @param Log $log
     */
    public function __construct(TrustResolver $trustResolver, Log $log)
    {
        $this->trustResolver = $trustResolver;
        $this->log = $log;
    }

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, self::MESSAGE_NAME);
    }

    protected function voteOnAttribute(string $attribute, $subject, Tokenable $token): bool
    {
        if (!is_object($subject)) {
            $this->log->info(sprintf('No context for message %s', $attribute));
            return false;
        }

        $method = 'can' . class_basename($attribute);

        if (!method_exists($this, $method)) {
            throw new \RuntimeException(
                sprintf('Method fort attribute %s does not exists on class "%s"', $attribute, get_class($this))
            );
        }

        $result = $this->{$method}($token, $subject);

        $this->log->info(
            sprintf('Vote %s on attribute %s for subject %s and token class %s',
                $result ? 'granted' : 'denied',
                $attribute,
                get_class($subject),
                class_basename($token)
            )
        );

        return $result;
    }

    protected function isOwner(User $account, AccountId $accountId): bool
    {
        return $account->getId()->sameValueAs($accountId);
    }

    protected function canPostTouit(Tokenable $token, PostTouit $command): bool
    {
        if ($this->trustResolver->isAnonymous($token)) {
            return false;
        }

        return $this->isOwner($token->user(), $command->authorId());
    }

    protected function canFollowAccount(Tokenable $token, FollowAccount $command): bool
    {
        if ($this->trustResolver->isAnonymous($token)) {
            return false;
        }

        if (!$this->isOwner($token->user(), $command->getFollowerId())) {
            return false;
        }

        if ($command->getFollowerId()->sameValueAs($command->getFollowingId())) {
            return false;
        }

        // following account must be fully enabled,
        // follower account should not be blocked by following

        return true;
    }

    protected function canRegisterAccount(Tokenable $token, RegisterAccount $command): bool
    {
        return $this->trustResolver->isAnonymous($token);
    }

    protected function canActivateAccount(Tokenable $token, ActivateAccount $command): bool
    {
        return $this->trustResolver->isAnonymous($token);
    }

    protected function canChangeLocalPassword(Tokenable $token, ChangeLocalPassword $command = null): bool
    {
        if ($this->trustResolver->isAnonymous($token)) {
            return false;
        }

        return $this->isOwner($token->user(), $command->accountId());
    }

    protected function canReplyToTouit(Tokenable $token, ReplyToTouit $command): bool
    {
        if ($this->trustResolver->isAnonymous($token)) {
            return false;
        }

        if (!$this->isOwner($token->user(), $command->authorId())) {
            return false;
        }

        // need original poster id in RelyToTouit command

        // todo author is allowed to see moderated touit
        // todo author is blocked by poster

        return true;
    }

    protected function canAccountFavoriteTouit(Tokenable $token, AccountFavoriteTouit $command): bool
    {
        if ($this->trustResolver->isAnonymous($token)) {
            return false;
        }

        if (!$this->isOwner($token->user(), $command->accountId())) {
            return false;
        }

        // to prevent account to fav his own touit, we need the owner of touit in command
        // or we have to make a query

        // is he moderated by touit status and his settings
        // is he blocked by touit owner

        return true;
    }
}