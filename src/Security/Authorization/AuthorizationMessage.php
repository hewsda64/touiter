<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Security\Authorization;

use Laraprooph\ServiceBus\HasBus;
use Prooph\Common\Messaging\Message;
use Thrust\Firewall\Foundation\Support\Secure;

trait AuthorizationMessage
{
    use Secure, HasBus;

    public function dispatchAuthorizedMessage(Message $message, string $bus = null)
    {
        $this->requireGranted(get_class($message), $message);

        return $this->dispatchMessage($message, $bus);
    }

    private function dispatchMessage(Message $message, string $bus = null)
    {
        switch ($message->messageType()) {
            case Message::TYPE_COMMAND:
                $this->dispatchCommand($message, $bus);
                break;
            case Message::TYPE_EVENT:
                $this->dispatchEvent($message, $bus);
                break;
            case Message::TYPE_QUERY:
                return $this->dispatchQuery($message, $bus);
            default:
                throw new \RuntimeException(
                    sprintf('Unknown type of message %s', $message->messageType())
                );
        }
    }
}