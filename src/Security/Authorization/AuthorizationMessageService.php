<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Security\Authorization;

use Prooph\ServiceBus\Async\AsyncMessage;
use Prooph\ServiceBus\Plugin\Guard\AuthorizationService;
use Thrust\Security\Foundation\Authorizer;
use Thrust\Security\Foundation\Guard;

class AuthorizationMessageService implements AuthorizationService
{
    use Guard, Authorizer;

    public function isGranted(string $messageName, $context = null): bool
    {
        $token = $this->requireToken();

        return $this->grant($token, $messageName, $context);
    }
}