<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Security\Context;

use Thrust\Firewall\Foundation\Contracts\Factory\FirewallContext;
use Thrust\Security\Foundation\Value\AnonymousKey;
use Thrust\Security\Foundation\Value\ContextKey;
use Thrust\Security\Foundation\Value\ProviderKey;
use Thrust\Security\Foundation\Value\RecallerKey;
use Thrust\Security\Response\Authorization\DefaultAuthorizationDenied;
use Thrust\Security\Response\Entrypoint\DefaultFormEntrypoint;

class FrontEndFirewallContext implements FirewallContext
{
    /**
     * @var string
     */
    protected $anonymousKey = 'frontend_anonymous';

    /**
     * @var string
     */
    protected $providerKey = 'eloquent_account';

    /**
     * @var bool
     */
    protected $anonymous = true;

    /**
     * @var bool
     */
    protected $stateless = false;

    /**
     * @var string
     */
    protected $contextKey = 'frontend';

    /**
     * @var string
     */
    protected $entryPoint = DefaultFormEntrypoint::class;

    /**
     * @var string
     */
    protected $deniedHandler = DefaultAuthorizationDenied::class;

    /**
     * @var string|callable
     */
    protected $logout;

    /**
     * @var
     */
    protected $switchUser;

    /**
     * @var array
     */
    protected $recaller = [
        //'account-login' => 'front_login_recaller_key',
    ];

    public function anonymousKey(): ?AnonymousKey
    {
        return new AnonymousKey($this->anonymousKey);
    }

    public function setAnonymousKey(string $anonymousKey): FirewallContext
    {
        return $this;
    }

    public function providerKey(): ProviderKey
    {
        return new ProviderKey($this->providerKey);
    }

    public function setProviderKey(string $providerKey): FirewallContext
    {
        return $this;
    }

    public function isAnonymous(): bool
    {
        return $this->anonymous;
    }

    public function setAnonymous(bool $anonymous): FirewallContext
    {
        return $this;
    }

    public function isStateless(): bool
    {
        return $this->stateless;
    }

    public function setStateless(bool $stateless): FirewallContext
    {
        return $this;
    }

    public function contextKey(): ContextKey
    {
        return new ContextKey($this->contextKey);
    }

    public function setContextKey(string $contextKey): FirewallContext
    {
        return $this;
    }

    public function entryPoint(): ?string
    {
        return $this->entryPoint;
    }

    public function setEntryPoint(string $entryPoint): FirewallContext
    {
        return $this;
    }

    public function deniedHandler(): ?string
    {
        return $this->deniedHandler;
    }

    public function setDeniedHandler(string $deniedHandler): FirewallContext
    {
        return $this;
    }

    public function logout()
    {
        return null;
    }

    public function setLogout($logout): FirewallContext
    {
        return $this;
    }

    public function switchUser()
    {
        return null;
    }

    public function setSwitchUser(array $config): FirewallContext
    {
        return $this;
    }

    public function recaller(string $serviceKey): ?RecallerKey
    {
        if ($this->hasRecallerForService($serviceKey)) {
            return new RecallerKey($this->recaller[$serviceKey]);
        }

        return null;
    }

    public function setRecaller(array $recaller): FirewallContext
    {
        $this->recaller = $recaller;

        return $this;
    }

    public function hasRecallerForService(string $serviceKey): bool
    {
        return isset($this->recaller[$serviceKey]);
    }
}