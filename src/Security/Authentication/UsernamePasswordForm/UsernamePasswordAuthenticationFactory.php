<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Security\Authentication\UsernamePassword;

use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Thrust\Firewall\Factory\Payload\PayloadService;
use Thrust\Firewall\Foundation\Authentication\FormAuthenticationFactory;
use Thrust\Security\Request\Firewall\GenericFormFirewall;

class UsernamePasswordAuthenticationFactory extends FormAuthenticationFactory
{
    public function key(): string
    {
        return 'account-login';
    }

    public function registerListener(PayloadService $payload): string
    {
        $id = 'firewall.' . $this->position() . '.' . $this->key() . '_authentication_listener.' . $payload->firewallKey->getKey();

        $this->container->bindIf($id, function (Application $app) use ($payload) {
            return new GenericFormFirewall(
                $payload->firewallKey,
                $app->make($this->registerAuthenticationSuccess('/')),
                $app->make($this->registerAuthenticationFailure('/auth/login')),
                $this->matcher(),
                $payload->context->isStateless()
            );
        });

        return $id;
    }

    public function matcher(): ?RequestMatcherInterface
    {
        return new UsernamePasswordAuthenticationRequest();
    }
}