<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Security\Authentication\UsernamePassword;

use Illuminate\Http\Request as IlluminateRequest;
use Symfony\Component\HttpFoundation\Request;
use Thrust\Security\Authentication\Token\Value\SimpleCredentials;
use Thrust\Security\Contract\Http\Request\AuthenticationRequest;
use Thrust\Security\User\Value\EmailAddress;

class UsernamePasswordAuthenticationRequest implements AuthenticationRequest
{
    public function extract(IlluminateRequest $request): ?array
    {
        if ($this->matches($request)) {
            return [
                EmailAddress::fromString($request->input('identifier')),
                SimpleCredentials::fromString($request->input('password'))
            ];
        }

        return null;
    }

    public function matches(Request $request): bool
    {
        return $request->route()->named('frontend.auth.login.post');
    }
}