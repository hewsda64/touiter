<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Security\Services;

use Thrust\Security\Contract\User\User as SecurityUser;
use Thrust\Security\Contract\User\UserChecker;
use Thrust\Security\Contract\User\UserThrottle;
use Thrust\Security\Exception\UserNotActivated;

class AccountChecker implements UserChecker
{

    public function checkPreAuthentication(SecurityUser $user): void
    {
        if (!$user instanceof UserThrottle) {
            return;
        }

        if (!$user->isActivated()) {
            throw new UserNotActivated('Account is not activated.');
        }
    }

    public function checkPostAuthentication(SecurityUser $user): void
    {
    }
}