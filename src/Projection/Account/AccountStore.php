<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Projection\Account;

use Hewsda64\Touiter\Domain\Account\Event\AccountWasActivated;
use Hewsda64\Touiter\Domain\Account\Event\AccountWasRegistered;
use Hewsda64\Touiter\Domain\Account\Event\LocalPasswordWasChanged;
use Hewsda64\Touiter\Projection\Table;
use Illuminate\Database\Connection;
use Laraprooph\ModelEvent\ModelChanged;

class AccountStore
{

    /**
     * @var Connection
     */
    private $connection;

    /**
     * AccountStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    protected function register(AccountWasRegistered $event): void
    {
        $this->connection->table(Table::ACCOUNT_TABLE)->insert([
            'id' => $event->aggregateId(),
            'name' => $event->payload()['name'],
            'email' => $event->payload()['email'],
            'password' => $event->payload()['password'],
            'created_at' => $this->formatDate($event->createdAt())
        ]);
    }

    protected function updatePassword(LocalPasswordWasChanged $event): void
    {
        $this->connection->table(Table::ACCOUNT_TABLE)
            ->where('id', $event->accountId()->identify())
            ->update([
                'password' => $event->password()->toString(),
                'updated_at' => $this->formatDate($event->createdAt())
            ]);
    }

    protected function activateAccount(AccountWasActivated $event): void
    {
        $this->connection->table(Table::ACCOUNT_TABLE)
            ->where('id', $event->accountId()->identify())
            ->update([
                'activated' => $event->accountStatus()->getValue(),
                'updated_at' => $this->formatDate($event->createdAt())
            ]);
    }

    protected function formatDate(\DateTimeImmutable $dateTimeImmutable): string
    {
        return $dateTimeImmutable->format('Y-m-d H:i:s');
    }

    public function onEvent(ModelChanged $event): void
    {
        if ($event instanceof AccountWasRegistered) {
            $this->register($event);
        } elseif ($event instanceof LocalPasswordWasChanged) {
            $this->updatePassword($event);
        } elseif ($event instanceof AccountWasActivated) {
            $this->activateAccount($event);
        } else {
            throw new \RuntimeException(
                sprintf('Missing event handler %s for class %s', get_class($event), static::class)
            );
        }
    }
}