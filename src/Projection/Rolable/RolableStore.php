<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Projection\Rolable;

use Hewsda64\Touiter\Domain\Rolable\Event\RoleWasGiven;
use Hewsda64\Touiter\Projection\Table;
use Illuminate\Database\Connection;
use Laraprooph\ModelEvent\ModelChanged;
use Ramsey\Uuid\Uuid;

class RolableStore
{

    /**
     * @var Connection
     */
    private $connection;

    /**
     * RolableStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    protected function attach(RoleWasGiven $event): void
    {
        $this->connection->table(Table::ROLABLE)->insert([
            'id' => Uuid::uuid4()->toString(),
            'account_id' => $event->payload()['account_id'],
            'role_id' => $event->payload()['role_id']
        ]);
    }

    public function onEvent(ModelChanged $event): void
    {
        if ($event instanceof RoleWasGiven) {
            $this->attach($event);
        } else {
            throw new \RuntimeException(
                sprintf('Missing event handler %s for class %s', get_class($event), static::class)
            );
        }
    }
}