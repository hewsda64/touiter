<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Projection\AccountStat;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\AccountStat\Event\AccountStatWasCreated;
use Hewsda64\Touiter\Domain\AccountStat\Event\AccountFavoritesWasIncremented;
use Hewsda64\Touiter\Domain\AccountStat\Event\TouitCounterWasIncremented;
use Hewsda64\Touiter\Domain\Follower\Event\AccountWasFollowed;
use Hewsda64\Touiter\Projection\Table;
use Illuminate\Database\Connection;
use Laraprooph\ModelEvent\ModelChanged;

class AccountStatStore
{

    /**
     * @var Connection
     */
    private $connection;

    /**
     * AccountStatStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    protected function create(AccountStatWasCreated $event): void
    {
        $this->connection->table(Table::ACCOUNT_STAT_TABLE)
            ->insert(['id' => $event->accountId()->identify()]);
    }

    protected function increment(AccountId $accountId, string $field): void
    {
        $this->connection->table(Table::ACCOUNT_STAT_TABLE)
            ->where('id', $accountId->identify())->increment($field);
    }

    protected function decrement(AccountId $accountId, string $field): void
    {
        $this->connection->table(Table::ACCOUNT_STAT_TABLE)
            ->where('id', $accountId->identify())->decrement($field);
    }

    public function onEvent(ModelChanged $event): void
    {
        if ($event instanceof AccountStatWasCreated) {
            $this->create($event);
        } elseif ($event instanceof TouitCounterWasIncremented) {
            $this->increment($event->accountId(), 'touits');
        } elseif ($event instanceof AccountFavoritesWasIncremented) {
            $this->increment($event->accountId(), 'likes');
        } elseif ($event instanceof AccountWasFollowed) {
            $this->increment($event->getFollowingId(), 'followers');
            $this->increment($event->getFollowerId(), 'followings');
        } else {
            throw new \RuntimeException(
                sprintf('Missing event handler %s for class %s', get_class($event), static::class)
            );
        }
    }
}