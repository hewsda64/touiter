<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Projection\Favorite;

use Hewsda64\Touiter\Domain\Favorite\Event\TouitWasFavoriteByAccount;
use Hewsda64\Touiter\Projection\Table;
use Illuminate\Database\Connection;
use Laraprooph\ModelEvent\ModelChanged;

class FavoriteStore
{

    /**
     * @var Connection
     */
    private $connection;

    /**
     * FavoriteStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    protected function create(TouitWasFavoriteByAccount $event): void
    {
        $this->connection->table(Table::FAVORITE_TABLE)->insert([
                'id' => $event->aggregateId(),
                'account_id' => $event->accountId()->identify(),
                'touit_id' => $event->touitId()->toString(),
                'created_at' => $event->createdAt()->format('Y-m-d H:i:s')]
        );
    }

    public function onEvent(ModelChanged $event): void
    {
        if ($event instanceof TouitWasFavoriteByAccount) {
            $this->create($event);
        }  else {
            throw new \RuntimeException(
                sprintf('Missing event handler %s for class %s', get_class($event), static::class)
            );
        }
    }

}