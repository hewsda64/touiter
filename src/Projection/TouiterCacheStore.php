<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Projection;

use Hewsda64\Touiter\Domain\Follower\Event\AccountWasFollowed;
use Hewsda64\Touiter\Domain\Touit\Event\TouitWasPosted;
use Illuminate\Contracts\Cache\Repository;
use Laraprooph\ModelEvent\ModelChanged;

class TouiterCacheStore
{
    const SUGGEST_FOLLOWING_TO_ACCOUNT = 'suggest_following_for_account:';
    const FOLLOWING_TIMELINE_OF_ACCOUNT = 'following_timeline_of_account:';
    const FOLLOWERS_OF_ACCOUNT = 'followers_of_account:';
    const TIMELINE_OF_ACCOUNT = 'timeline_of_account:';

    /**
     * @var Repository
     */
    private $repository;

    /**
     * TouiterCacheStore constructor.
     *
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    protected function updateSuggestFollowingToAccount(string $accountId): void
    {
        $cacheKey = $this->getCacheName(self::SUGGEST_FOLLOWING_TO_ACCOUNT . $accountId);

        if ($this->repository->has($cacheKey)) {
            $this->repository->forget($cacheKey);
        }
    }

    protected function updateFollowingTimelineOfAccount(string $accountId): void
    {
        $cacheKey = $this->getCacheName(self::FOLLOWING_TIMELINE_OF_ACCOUNT . $accountId);

        if ($this->repository->has($cacheKey)) {
            $this->repository->forget($cacheKey);
        }
    }

    protected function updateTimelineOfAccount(string $accountId): void
    {
        $cacheKey = $this->getCacheName(self::TIMELINE_OF_ACCOUNT . $accountId);

        if ($this->repository->has($cacheKey)) {
            $this->repository->forget($cacheKey);
        }
    }

    public function onEvent(ModelChanged $event): void
    {
        switch ($event) {
            case $event instanceof AccountWasFollowed:
                $this->updateSuggestFollowingToAccount($event->getFollowerId()->identify());
                $this->updateFollowingTimelineOfAccount($event->getFollowingId()->identify());
                $this->updateFollowingTimelineOfAccount($event->getFollowerId()->identify());
                break;

            case $event instanceof TouitWasPosted:
                $this->updateTimelineOfAccount($event->authorId()->identify());
                $this->updateFollowingTimelineOfAccount($event->authorId()->identify());
                // need to kill cache of all followers
                break;

            default:
                throw new \RuntimeException(
                    sprintf('Missing event handler %s for class %s', get_class($event), static::class)
                );
        }
    }

    private function getCacheName(string $key): string
    {
        return sha1($key);
    }
}