<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Projection;

class Table
{
    const ACCOUNT_TABLE = 'local_accounts';

    const ACCOUNT_STAT_TABLE = 'account_stat';

    const ACCOUNT_ACTIVATION_TABLE = 'account_activation';

    const FAVORITE_TABLE = 'like_touits';

    const FOLLOWER_TABLE = 'followers';

    const ROLABLE = 'account_role';

    const ROLE_TABLE = 'roles';

    const TOUIT_TABLE = 'touits';
}