<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Projection\Follower;

use Hewsda64\Touiter\Domain\Follower\Event\AccountWasFollowed;
use Hewsda64\Touiter\Projection\Table;
use Illuminate\Database\Connection;
use Laraprooph\ModelEvent\ModelChanged;

class FollowerStore
{

    /**
     * @var Connection
     */
    private $connection;

    /**
     * FollowerStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    protected function insert(AccountWasFollowed $event): void
    {
        $this->connection->table(Table::FOLLOWER_TABLE)
            ->insert([
                'id' => $event->aggregateId(),
                'following_id' => $event->payload()['following_id'],
                'follower_id' => $event->payload()['follower_id'],
                'created_at' => $this->formatDate($event->createdAt())
            ]);
    }

    protected function formatDate(\DateTimeImmutable $dateTimeImmutable): string
    {
        return $dateTimeImmutable->format('Y-m-d H:i:s');
    }

    public function onEvent(ModelChanged $event): void
    {
        if ($event instanceof AccountWasFollowed) {
            $this->insert($event);
        } else {
            throw new \RuntimeException(
                sprintf('Missing event handler %s for class %s', get_class($event), static::class)
            );
        }
    }

}