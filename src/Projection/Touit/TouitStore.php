<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Projection\Touit;

use Hewsda64\Touiter\Domain\Touit\Event\RepliesCounterWasIncremented;
use Hewsda64\Touiter\Domain\Touit\Event\ReplyToTouitWasPosted;
use Hewsda64\Touiter\Domain\Touit\Event\TouitWasFavorite;
use Hewsda64\Touiter\Domain\Touit\Event\TouitWasPosted;
use Hewsda64\Touiter\Projection\Table;
use Illuminate\Database\Connection;
use Laraprooph\ModelEvent\ModelChanged;

class TouitStore
{

    /**
     * @var Connection
     */
    private $connection;

    /**
     * TouitStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    protected function post(TouitWasPosted $event): void
    {
        $this->connection->table(Table::TOUIT_TABLE)
            ->insert([
                'id' => $event->aggregateId(),
                'author_id' => $event->payload()['author_id'],
                'body' => $event->payload()['body'],
                'created_at' => $this->formatDate($event->createdAt())
            ]);
    }

    protected function reply(ReplyToTouitWasPosted $event): void
    {
        $this->connection->table(Table::TOUIT_TABLE)
            ->insert([
                'id' => $event->getUid()->toString(),
                'author_id' => $event->authorId()->identify(),
                'body' => $event->getBody(),
                'reply_to_touit' => $event->getTouitId()->toString(),
                'created_at' => $this->formatDate($event->createdAt())
            ]);
    }

    protected function incrementFavorite(TouitWasFavorite $event): void
    {
        $this->connection->table(Table::TOUIT_TABLE)
            ->where('id', $event->aggregateId())
            ->increment('favorite_count', 1, [
                'updated_at' => $this->formatDate($event->createdAt())
            ]);
    }

    protected function incrementReplyCounter(RepliesCounterWasIncremented $event): void
    {
        $this->connection->table(Table::TOUIT_TABLE)
            ->where('id', $event->touitId()->toString())
            ->increment('reply_count', 1, [
                'updated_at' => $this->formatDate($event->createdAt())
            ]);
    }

    protected function formatDate(\DateTimeImmutable $dateTimeImmutable): string
    {
        return $dateTimeImmutable->format('Y-m-d H:i:s');
    }

    public function onEvent(ModelChanged $event): void
    {
        if ($event instanceof TouitWasPosted) {
            $this->post($event);
        } elseif ($event instanceof ReplyToTouitWasPosted) {
            $this->reply($event);
        } elseif ($event instanceof TouitWasFavorite) {
            $this->incrementFavorite($event);
        } elseif ($event instanceof RepliesCounterWasIncremented) {
            $this->incrementReplyCounter($event);
        } else {
            throw new \RuntimeException(
                sprintf('Missing event handler %s for class %s', get_class($event), static::class)
            );
        }
    }
}