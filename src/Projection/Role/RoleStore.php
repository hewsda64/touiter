<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Projection\Role;

use Hewsda64\Touiter\Domain\Role\Event\RoleWasCreated;
use Hewsda64\Touiter\Projection\Table;
use Illuminate\Database\Connection;
use Laraprooph\ModelEvent\ModelChanged;

class RoleStore
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * RoleStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function createRole(RoleWasCreated $event): void
    {
        $this->connection->table(Table::ROLE_TABLE)->insert([
            'id' => $event->aggregateId(),
            'name' => $event->name(),
            'slug' => $event->slug(),
            'description' => $event->description()
        ]);
    }

    public function onEvent(ModelChanged $event): void
    {
        if ($event instanceof RoleWasCreated) {
            $this->createRole($event);
        } else {
            throw new \RuntimeException(
                sprintf('Missing event handler %s for class %s', get_class($event), static::class)
            );
        }
    }
}