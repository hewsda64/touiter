<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Projection\Activation;

use Hewsda64\Touiter\Domain\Activation\Event\AccountActivationWasDeleted;
use Hewsda64\Touiter\Domain\Activation\Event\NotActivatedAccountWasRegistered;
use Hewsda64\Touiter\Projection\Table;
use Illuminate\Database\Connection;
use Laraprooph\ModelEvent\ModelChanged;

class ActivationStore
{

    /**
     * @var Connection
     */
    private $connection;

    /**
     * ActivationStore constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    protected function createNotActivatedAccount(NotActivatedAccountWasRegistered $event): void
    {
        $activation = $event->activationToken();

        $this->connection->table(Table::ACCOUNT_ACTIVATION_TABLE)
            ->insert([
                'id' => $event->accountId()->identify(),
                'email' => $event->email()->identify(),
                'activation_token' => $activation->token(),
                'created_at' => $activation->createdAt()->format('Y-m-d H:i:s')
            ]);
    }

    protected function delete(AccountActivationWasDeleted $event): void
    {
        $this->connection->table(Table::ACCOUNT_ACTIVATION_TABLE)
            ->delete($event->accountId()->identify());
    }

    public function onEvent(ModelChanged $event): void
    {
        if ($event instanceof NotActivatedAccountWasRegistered) {
            $this->createNotActivatedAccount($event);
        } elseif ($event instanceof AccountActivationWasDeleted) {
            $this->delete($event);
        } else {
            throw new \RuntimeException(
                sprintf('Missing event handler %s for class %s', get_class($event), static::class)
            );
        }
    }
}