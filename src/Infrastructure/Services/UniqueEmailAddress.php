<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Services;

use Hewsda64\Touiter\Domain\Account\Services\EmailAddressAvailability;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Infrastructure\Repository\Account\AccountCollection;
use Thrust\Security\Contract\User\Value\EmailAddress;

class UniqueEmailAddress implements EmailAddressAvailability
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * UniqueEmailAddress constructor.
     *
     * @param AccountCollection $accountCollection
     */
    public function __construct(AccountCollection $accountCollection)
    {
        $this->accountCollection = $accountCollection;
    }

    public function __invoke(EmailAddress $emailAddress): ?AccountId
    {
        if (null !== $account = $this->accountCollection->accountOfEmail((string)$emailAddress)) {
            return $account->getId();
        }

        return null;
    }
}