<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Services;

use Hewsda64\Touiter\Domain\Account\Services\PasswordEncoder;
use Hewsda64\Touiter\Domain\Account\Values\BcryptPassword;
use Hewsda64\Touiter\Domain\Account\Values\ClearPassword;
use Illuminate\Contracts\Hashing\Hasher;
use Thrust\Security\Contract\User\Value\EncodedPassword;

class BcryptPasswordEncoder implements PasswordEncoder
{
    /**
     * @var Hasher
     */
    private $encoder;

    /**
     * BcryptPasswordEncoder constructor.
     *
     * @param Hasher $encoder
     */
    public function __construct(Hasher $encoder)
    {
        $this->encoder = $encoder;
    }

    public function match(string $clearPassword, EncodedPassword $encodedPassword): bool
    {
        return $this->encoder->check($clearPassword, $encodedPassword->toString());
    }

    public function __invoke(ClearPassword $password): EncodedPassword
    {
        return BcryptPassword::fromString(
            $this->encoder->make($password->toString())
        );
    }
}