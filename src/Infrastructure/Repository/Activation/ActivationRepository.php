<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Activation;

use Hewsda64\Touiter\Domain\Activation\Activation;
use Hewsda64\Touiter\Domain\Activation\Repository\ActivationCollection;
use Illuminate\Database\Eloquent\Builder;
use Laraprooph\ModelEvent\Model\Repository\ModelRepository;
use Prooph\ServiceBus\EventBus;

class ActivationRepository extends ModelRepository implements ActivationCollection
{
    /**
     * @var Activation
     */
    private $model;

    /**
     * ActivationRepository constructor.
     *
     * @param Activation $model
     * @param EventBus $eventBus
     */
    public function __construct(Activation $model, EventBus $eventBus)
    {
        parent::__construct($eventBus);
        $this->model = $model;
    }

    public function accountActivationOfId(string $accountId): ?Activation
    {
        return $this->createModel()->find($accountId);
    }

    public function accountActivationOfToken(string $activationToken): ?Activation
    {
        return $this->createModel()->where('activation_token', $activationToken)->first();
    }

    public function save(Activation $activation): void
    {
        $this->saveAggregateRoot($activation);
    }

    public function createModel(): Builder
    {
        return $this->model->newInstance()->newQuery();
    }
}