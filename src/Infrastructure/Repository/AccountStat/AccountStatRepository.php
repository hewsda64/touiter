<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\AccountStat;

use Hewsda64\Touiter\Domain\AccountStat\AccountStat;
use Hewsda64\Touiter\Domain\AccountStat\Repository\AccountStatList;
use Illuminate\Database\Eloquent\Builder;
use Laraprooph\ModelEvent\Model\Repository\ModelRepository;
use Prooph\ServiceBus\EventBus;

class AccountStatRepository extends ModelRepository implements AccountStatList
{
    /**
     * @var AccountStat
     */
    private $model;

    /**
     * AccountStatRepository constructor.
     *
     * @param AccountStat $model
     * @param EventBus $eventBus
     */
    public function __construct(AccountStat $model, EventBus $eventBus)
    {
        parent::__construct($eventBus);
        $this->model = $model;
    }

    public function accountOfId(string $accountId): ?AccountStat
    {
        return $this->createModel()->find($accountId);
    }

    public function save(AccountStat $root): void
    {
        $this->saveAggregateRoot($root);
    }

    public function createModel(): Builder
    {
        return $this->model->newInstance()->newQuery();
    }
}