<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Favorite;

use Hewsda64\Touiter\Domain\Favorite\Favorite;
use Hewsda64\Touiter\Domain\Favorite\Repository\FavoriteList;
use Illuminate\Support\Collection;
use Laraprooph\ModelEvent\Model\Repository\ModelRepository;
use Prooph\ServiceBus\EventBus;

class FavoriteRepository extends ModelRepository implements FavoriteList
{
    /**
     * @var Favorite
     */
    private $model;

    /**
     * FavoriteRepository constructor.
     *
     * @param Favorite $model
     * @param EventBus $eventBus
     */
    public function __construct(Favorite $model, EventBus $eventBus)
    {
        parent::__construct($eventBus);
        $this->model = $model;
    }

    public function favoriteTouitListOfAccountId(string $accountId): ?Collection
    {
        return $this->model->newInstance()->newQuery()
            ->where('account_id', $accountId)
            ->get();
    }

    public function save(Favorite $root): void
    {
        $this->saveAggregateRoot($root);
    }
}