<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Role;

use Hewsda64\Touiter\Domain\Role\Repository\RoleList;
use Hewsda64\Touiter\Domain\Role\Role;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Laraprooph\ModelEvent\Model\Repository\ModelRepository;
use Prooph\ServiceBus\EventBus;

class RoleCollection extends ModelRepository implements RoleList
{

    /**
     * @var Role|Model
     */
    private $role;

    /**
     * RoleCollection constructor.
     *
     * @param Role $role
     * @param EventBus $eventBus
     */
    public function __construct(Role $role, EventBus $eventBus)
    {
        $this->role = $role;

        parent::__construct($eventBus);
    }

    public function save(Role $role): void
    {
        $this->saveAggregateRoot($role);
    }

    public function roleOfId(string $roleId): ?Role
    {
        return $this->newModel()->find($roleId);
    }

    public function roleOfName(string $roleName): ?Role
    {
        return $this->newModel()->where('name', $roleName)->first();
    }

    public function has(string $roleName): bool
    {
        return null !== $this->roleOfName($roleName);
    }

    public function newModel(): Builder
    {
        return $this->role->newInstance()->newQuery();
    }
}