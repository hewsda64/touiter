<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Role;

use Hewsda64\Touiter\Domain\Role\Repository\RoleList;
use Hewsda64\Touiter\Domain\Role\Role;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Collection;

class RoleCacheCollection implements RoleList
{
    /**
     * @var Repository
     */
    private $cache;

    /**
     * @var RoleCollection
     */
    private $roleCollection;

    /**
     * RoleCacheCollection constructor.
     *
     * @param RoleCollection $roleCollection
     * @param Repository $cache
     */
    public function __construct(RoleCollection $roleCollection, Repository $cache)
    {
        $this->cache = $cache;
        $this->roleCollection = $roleCollection;
    }

    public function roleOfId(string $roleId): ?Role
    {
        return $this->cache()->where('id', $roleId)->first();
    }

    public function roleOfName(string $roleName): ?Role
    {
        return $this->cache()->where('name', $roleName)->first();
    }

    public function has(string $roleName): bool
    {
        return null !== $this->roleOfName($roleName);
    }

    public function save(Role $role): void
    {
        $this->roleCollection->save($role);

        $this->cache->forget($this->cacheName());
    }

    protected function cacheName(): string
    {
        return sha1(static::class);
    }

    protected function cache(): Collection
    {
        if ($this->cache->has($this->cacheName())) {
            return $this->cache->get($this->cacheName());
        }

        $roles = $this->roleCollection->newModel()->get();

        $this->cache->forever($this->cacheName(), $roles);

        return $roles;
    }
}