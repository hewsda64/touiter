<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Follower;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Follower\Follower;
use Hewsda64\Touiter\Domain\Follower\Repository\FollowerCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Laraprooph\ModelEvent\Model\Repository\ModelRepository;
use Prooph\ServiceBus\EventBus;

class FollowerRepository extends ModelRepository implements FollowerCollection
{

    /**
     * @var Follower
     */
    private $model;

    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * FollowerRepository constructor.
     *
     * @param Follower $model
     * @param AccountCollection $accountCollection
     * @param EventBus $eventBus
     */
    public function __construct(Follower $model, AccountCollection $accountCollection, EventBus $eventBus)
    {
        parent::__construct($eventBus);

        $this->model = $model;
        $this->accountCollection = $accountCollection;
    }

    public function followersForAccount(string $followingId): Collection
    {
        return $this->createModel()
            ->with('follower.statistic')
            ->where('following_id', $followingId)
            ->orderBy('created_at')
            ->get();
    }

    public function followingsForAccount(string $followerId): Collection
    {
        return $this->createModel()
            ->with('following.statistic')
            ->where('follower_id', $followerId)
            ->orderBy('created_at')
            ->get();
    }

    public function suggestFiveAccountsToFollow(string $followerId): Collection
    {
        $alreadyFollowed = $this->followingsForAccount($followerId)->pluck('following_id');
        $alreadyFollowed->push($followerId);

        return $this->accountCollection
            ->createModel()
            ->whereNotIn('id', $alreadyFollowed)
            ->limit(5)
            ->get();
    }

    public function isAlreadyFollowed(string $followingId, string $followerId): bool
    {
        return null !== $this->createModel()
                ->where('following_id', $followingId)
                ->where('follower_id', $followerId)
                ->first();
    }

    public function save(Follower $root): void
    {
        $this->saveAggregateRoot($root);
    }

    public function createModel(): Builder
    {
        return $this->model->newInstance()->newQuery();
    }
}