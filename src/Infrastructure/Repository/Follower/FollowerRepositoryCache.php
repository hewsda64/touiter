<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Follower;

use Hewsda64\Touiter\Domain\Follower\Repository\FollowerCollection;
use Hewsda64\Touiter\Projection\TouiterCacheStore;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Collection;

class FollowerRepositoryCache
{
    /**
     * @var FollowerCollection
     */
    private $followerCollection;

    /**
     * @var Repository
     */
    private $store;

    /**
     * FollowerRepositoryCache constructor.
     *
     * @param FollowerCollection $followerCollection
     * @param Repository $store
     */
    public function __construct(FollowerCollection $followerCollection, Repository $store)
    {
        $this->followerCollection = $followerCollection;
        $this->store = $store;
    }

    public function suggestFiveAccountsToFollow(string $followerId): Collection
    {
        $cacheName = sha1(TouiterCacheStore::SUGGEST_FOLLOWING_TO_ACCOUNT . $followerId);

        if ($this->store->has($cacheName)) {
            return $this->store->get($cacheName);
        }

        $suggest = $this->followerCollection->suggestFiveAccountsToFollow($followerId);

        $this->store->forever($cacheName, $suggest);

        return $suggest;
    }
}