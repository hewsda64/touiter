<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Account;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Laraprooph\ModelEvent\Model\Repository\ModelRepository;
use Thrust\Security\Contract\User\User as SecurityUser;
use Thrust\Security\Contract\User\Value\EmailAddress as EmailContract;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Exception\UnsupportedUser;
use Thrust\Security\Exception\UserNotFound;

abstract class AccountRepository extends ModelRepository implements AccountCollection
{
    public function requireByIdentifier(Identifier $identifier): SecurityUser
    {
        if (!$identifier instanceof EmailContract) {
            throw new \InvalidArgumentException('Only support email as identifier.');
        }

        if (null !== $account = $this->accountOfEmail($identifier->identify())) {
            return $account;
        }

        throw new UserNotFound(
            sprintf('Account not found with identifier %s', $identifier->identify()));
    }

    public function refreshUser(SecurityUser $user): SecurityUser
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUser(
                sprintf('User class %s is not supported.', get_class($user)));
        }

        return $this->requireByIdentifier($user->getIdentifier());
    }
}