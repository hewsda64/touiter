<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Account;

use Hewsda64\Touiter\Domain\Account\Account;
use Illuminate\Database\Eloquent\Builder;
use Prooph\ServiceBus\EventBus;
use Thrust\Security\Contract\User\User as SecurityUser;

class AccountCollection extends AccountRepository
{
    /**
     * @var Account
     */
    private $model;

    /**
     * AccountCollection constructor.
     *
     * @param Account $model
     * @param EventBus $eventBus
     */
    public function __construct(Account $model, EventBus $eventBus)
    {
        parent::__construct($eventBus);

        $this->model = $model;
    }

    public function save(Account $account): void
    {
        $this->saveAggregateRoot($account);
    }

    public function accountOfId(string $id): ?SecurityUser
    {
        return $this->createModel()->with('roles')->find($id);
    }

    public function accountOfEmail(string $email): ?SecurityUser
    {
        return $this->createModel()->with('roles')->where('email', $email)->first();
    }

    public function accountOfUserName(string $userName): ? SecurityUser
    {
        return $this->createModel()->with('roles')->where('name', $userName)->first();
    }

    public function supportsClass(string $class): bool
    {
        return $class === Account::class;
    }

    public function createModel(): Builder
    {
        return $this->model->newInstance()->newQuery();
    }
}