<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Touit;

use Hewsda64\Touiter\Domain\Follower\Repository\FollowerCollection;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;
use Hewsda64\Touiter\Domain\Touit\Touit;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Laraprooph\ModelEvent\Model\Repository\ModelRepository;
use Prooph\ServiceBus\EventBus;

class TouitRepository extends ModelRepository implements TouitCollection
{
    /**
     * @var Touit
     */
    private $model;

    /**
     * @var FollowerCollection
     */
    private $followerCollection;

    /**
     * TouitRepository constructor.
     *
     * @param Touit $model
     * @param FollowerCollection $followerCollection
     * @param EventBus $eventBus
     */
    public function __construct(Touit $model, FollowerCollection $followerCollection, EventBus $eventBus)
    {
        parent::__construct($eventBus);

        $this->model = $model;
        $this->followerCollection = $followerCollection;
    }

    public function touitOfId(string $touitId): ?Touit
    {
        return $this->createModel()->find($touitId);
    }

    public function timelineOfTouitId(string $touitId): ?Touit
    {
        return $this->createModel()
            ->with(['author', 'replies' => function ($q) {
                $q->with('author');
                $q->orderBy('created_at');
            }])
            ->find($touitId);
    }

    public function timelineOfAuthorId(string $authorId): Collection
    {
        return $this->createModel()
            ->with(['author', 'replies' => function ($q) {
                $q->with('author');
                $q->orderBy('created_at');
            }])
            ->where('author_id', $authorId)
            ->whereNull('reply_to_touit')
            ->orderByDesc('created_at')
            ->limit(10)
            ->get();
    }

    public function followingTimelineOfAuthor(string $authorId): Collection
    {
        $followings = $this->followerCollection->followingsForAccount($authorId);

        return $this->createModel()
            ->with('author')
            ->whereNull('reply_to_touit')
            ->whereIn('author_id', $followings->pluck('following_id'))
            ->orderByDesc('created_at')
            ->limit(10)
            ->get();
    }


    public function save(Touit $root): void
    {
        $this->saveAggregateRoot($root);
    }

    public function createModel(): Builder
    {
        return $this->model->newInstance()->newQuery();
    }
}