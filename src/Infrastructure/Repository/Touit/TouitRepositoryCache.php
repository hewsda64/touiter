<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Touit;

use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;
use Hewsda64\Touiter\Projection\TouiterCacheStore;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Collection;

class TouitRepositoryCache
{
    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * @var Repository
     */
    private $store;

    /**
     * TouitCacheFinder constructor.
     *
     * @param TouitCollection $touitCollection
     * @param Repository $store
     */
    public function __construct(TouitCollection $touitCollection, Repository $store)
    {
        $this->touitCollection = $touitCollection;
        $this->store = $store;
    }

    public function timelineOfAuthorId(string $authorId): Collection
    {
        $cacheName = sha1(TouiterCacheStore::TIMELINE_OF_ACCOUNT . $authorId);

        if ($this->store->has($cacheName)) {
            return $this->store->get($cacheName);
        }

        $timeline = $this->touitCollection->timelineOfAuthorId($authorId);

        $this->store->forever($cacheName, $timeline);

        return $timeline;
    }

    public function followingTimelineOfAuthor(string $authorId): Collection
    {
        $cacheName = sha1(TouiterCacheStore::FOLLOWING_TIMELINE_OF_ACCOUNT . $authorId);

        if ($this->store->has($cacheName)) {
            return $this->store->get($cacheName);
        }

        $timeline = $this->touitCollection->followingTimelineOfAuthor($authorId);

        $this->store->forever($cacheName, $timeline);

        return $timeline;
    }
}