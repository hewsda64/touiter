<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Infrastructure\Repository\Rolable;

use Hewsda64\Touiter\Domain\Account\Account;
use Hewsda64\Touiter\Domain\Rolable\Rolable;
use Hewsda64\Touiter\Domain\Rolable\RolableList;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Laraprooph\ModelEvent\Model\Repository\ModelRepository;
use Prooph\ServiceBus\EventBus;

class RolableCollection extends ModelRepository implements RolableList
{
    /**
     * @var Account
     */
    private $model;

    /**
     * RolableCollection constructor.
     *
     * @param Account $model
     * @param EventBus $eventBus
     */
    public function __construct(Account $model, EventBus $eventBus)
    {
        parent::__construct($eventBus);
        $this->model = $model;
    }

    public function rolesOfAccount(string $accountId): Collection
    {
        $account = $this->createModel()->with('roles')->find($accountId);

        if ($account) {
            return $account->roles;
        }

        return new Collection();
    }

    public function save(Rolable $rolable): void
    {
        $this->saveAggregateRoot($rolable);
    }

    public function createModel(): Builder
    {
        return $this->model->newInstance()->newQuery();
    }
}