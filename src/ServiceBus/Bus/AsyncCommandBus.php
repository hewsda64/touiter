<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\ServiceBus\Bus;

use Prooph\ServiceBus\CommandBus;

class AsyncCommandBus extends CommandBus
{
}