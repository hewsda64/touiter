<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\ServiceBus\Bus;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AsyncProcessMessage implements ShouldQueue
{
    use InteractsWithQueue, Queueable;

    /**
     * @var array
     */
    private $payload;

    /**
     * AsyncProcessMessage constructor.
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->payload = $payload;
    }

    public function handle(AsyncCommandBus $asyncBus): void
    {
        $asyncBus->dispatch($this->payload);
    }

    public function getPayload(): array
    {
        return $this->payload;
    }
}