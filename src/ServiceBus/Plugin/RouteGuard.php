<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\ServiceBus\Plugin;

use Prooph\Common\Event\ActionEvent;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\AbstractPlugin;
use Prooph\ServiceBus\Plugin\Guard\AuthorizationService;
use Thrust\Security\Contract\Exception\AuthorizationException;

class RouteGuard extends AbstractPlugin
{
    /**
     * @var AuthorizationService
     */
    private $authorizationService;

    /**
     * @var bool
     */
    private $exposeEventMessageName;

    /**
     * RouteGuard constructor.
     *
     * @param AuthorizationService $authorizationService
     * @param bool $exposeEventMessageName
     */
    public function __construct(AuthorizationService $authorizationService, bool $exposeEventMessageName = false)
    {
        $this->authorizationService = $authorizationService;
        $this->exposeEventMessageName = $exposeEventMessageName;
    }

    public function attachToMessageBus(MessageBus $messageBus): void
    {
        $this->listenerHandlers[] = $messageBus->attach(
            MessageBus::EVENT_DISPATCH,
            function (ActionEvent $actionEvent): void {
                $messageName = $actionEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE_NAME);

                if ($this->authorizationService->isGranted(
                    $messageName,
                    $actionEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE)
                )) {
                    return;
                }

                $actionEvent->stopPropagation(true);

                if (! $this->exposeEventMessageName) {
                    $messageName = '';
                }

                throw new AuthorizationException($messageName);
            },
            MessageBus::PRIORITY_ROUTE
        );
    }
}