<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\ServiceBus\Plugin;

use Illuminate\Database\Connection;
use Prooph\Common\Event\ActionEvent;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\AbstractPlugin;

class TransactionManager extends AbstractPlugin
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * TransactionManager constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function attachToMessageBus(MessageBus $messageBus): void
    {
        $this->attachOnDispatch($messageBus);

        $this->attachOnFinalize($messageBus);
    }

    private function attachOnDispatch(MessageBus $messageBus): void
    {
        $this->listenerHandlers[] = $messageBus->attach(
            CommandBus::EVENT_DISPATCH,
            function (ActionEvent $event): void {
                $this->connection->beginTransaction();
            },
            CommandBus::PRIORITY_INVOKE_HANDLER + 1000
        );
    }

    private function attachOnFinalize(MessageBus $messageBus): void
    {
        $this->listenerHandlers[] = $messageBus->attach(
            CommandBus::EVENT_FINALIZE,
            function (ActionEvent $event): void {
                if ($this->connection->transactionLevel() > 0) {
                    if ($event->getParam(CommandBus::EVENT_PARAM_EXCEPTION)) {
                        $this->connection->rollBack();
                    } else {
                        $this->connection->commit();
                    }
                }
            },
            1000
        );
    }
}