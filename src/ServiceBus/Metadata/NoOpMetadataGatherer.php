<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\ServiceBus\Metadata;

use Illuminate\Http\Request;

class NoOpMetadataGatherer implements MetadataGatherer
{
    public function getFromRequest(Request $request): array
    {
        return [];
    }
}