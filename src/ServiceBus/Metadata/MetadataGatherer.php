<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\ServiceBus\Metadata;

use Illuminate\Http\Request;

interface MetadataGatherer
{
    public function getFromRequest(Request $request): array;
}