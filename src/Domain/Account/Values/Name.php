<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Values;

use Hewsda64\Touiter\Application\Exceptions\Assertion;
use Thrust\Security\Contract\User\Value\UserName;
use Thrust\Security\Contract\Value\SecurityValue;

class Name implements UserName
{
    const MIN_LENGTH = 3;
    const MAX_LENGTH = 150;

    /**
     * @var string
     */
    private $name;

    /**
     * Name constructor.
     *
     * @param string $name
     */
    private function __construct(string $name)
    {
        $this->name = $name;
    }

    public static function fromString($name): self
    {
        Assertion::string($name);
        Assertion::betweenLength($name, self::MIN_LENGTH, self::MAX_LENGTH);

        return new self($name);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function toString(): string
    {
        return $this->getName();
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->name === $aValue->getName();
    }

    public function __toString(): string
    {
        return $this->getName();
    }
}