<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Values;

use Thrust\Security\Contract\User\Value\EncodedPassword;
use Thrust\Security\Contract\Value\SecurityValue;

class BcryptPassword extends Password implements EncodedPassword
{

    public static function fromString($password): self
    {
        return new self($password);
    }

    public function getCredentials(): string
    {
        return $this->password;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->password === $aValue->getCredentials();
    }
}