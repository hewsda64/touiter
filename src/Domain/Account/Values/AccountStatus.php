<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Values;

use Hewsda64\Touiter\Application\Support\Values\Enum;

/**
 * @method static AccountStatus NOTACTIVATED()
 * @method static AccountStatus ACTIVATED()
 */
class AccountStatus extends Enum
{
    const NOT_ACTIVATED = 0;
    
    const ACTIVATED = 1;
}