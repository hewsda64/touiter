<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Values;

use Hewsda64\Touiter\Application\Exceptions\Assertion;
use Thrust\Security\Contract\Value\SecurityValue;

class ClearPassword extends Password
{

    public static function fromStringWithConfirmation($password, $confirmedPassword): ClearPassword
    {
        Assertion::same($password, $confirmedPassword, 'Password and his confirmation does not match.');

        return new self($password);
    }

    public static function fromString($password): ClearPassword
    {
        return new self($password);
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof ClearPassword && $this->password === $aValue->toString();
    }
}