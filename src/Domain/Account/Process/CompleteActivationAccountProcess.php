<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Process;

use Hewsda64\Touiter\Domain\Account\Event\AccountWasActivated;
use Hewsda64\Touiter\Domain\Activation\Command\DeleteAccountActivation;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;

class CompleteActivationAccountProcess
{
    /**
     * @var AsyncCommandBus
     */
    private $bus;

    /**
     * CompleteActivationAccountProcess constructor.
     *
     * @param AsyncCommandBus $bus
     */
    public function __construct(AsyncCommandBus $bus)
    {
        $this->bus = $bus;
    }

    public function onEvent(AccountWasActivated $event): void
    {
        $this->bus->dispatch(
            DeleteAccountActivation::forAccount($event->accountId()->identify())
        );
    }
}