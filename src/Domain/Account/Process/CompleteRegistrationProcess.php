<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Process;

use Hewsda64\Touiter\Domain\Account\Command\GiveRole;
use Hewsda64\Touiter\Domain\Account\Event\AccountWasRegistered;
use Hewsda64\Touiter\Domain\AccountStat\Command\CreateAccountStat;
use Hewsda64\Touiter\Domain\Activation\Command\RegisterNotActivatedAccount;
use Hewsda64\Touiter\Domain\Activation\Values\ActivationToken;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;

class CompleteRegistrationProcess
{
    /**
     * @var AsyncCommandBus
     */
    private $asyncCommandBus;

    /**
     * CompleteRegistrationProcess constructor.
     *
     * @param AsyncCommandBus $asyncCommandBus
     */
    public function __construct(AsyncCommandBus $asyncCommandBus)
    {
        $this->asyncCommandBus = $asyncCommandBus;
    }

    public function onEvent(AccountWasRegistered $event): void
    {
        $accountId = $event->accountId()->identify();

        $this->asyncCommandBus->dispatch(CreateAccountStat::forAccount($accountId));

        $this->asyncCommandBus->dispatch(GiveRole::forAccount($accountId, ['ROLE_USER']));

        $this->asyncCommandBus->dispatch(RegisterNotActivatedAccount::forAccount(
            $accountId, $event->email()->identify(), ActivationToken::nextToken()->token()
        ));

        // send email or another process ?
    }
}