<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Services;

use Hewsda64\Touiter\Domain\Account\Values\ClearPassword;
use Thrust\Security\Contract\User\Value\EncodedPassword;

interface PasswordEncoder
{
    public function match(string $clearPassword, EncodedPassword $encodedPassword): bool;

    public function __invoke(ClearPassword $password): EncodedPassword;
}