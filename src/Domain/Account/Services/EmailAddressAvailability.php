<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Services;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Thrust\Security\Contract\User\Value\EmailAddress;

interface EmailAddressAvailability
{
    public function __invoke(EmailAddress $emailAddress): ?AccountId;
}