<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Repository;

use Hewsda64\Touiter\Domain\Account\Account;
use Thrust\Security\Contract\User\UserProvider;
use Thrust\Security\Contract\User\User as SecurityUser;

interface AccountCollection extends UserProvider
{
    public function accountOfId(string $id): ?SecurityUser;

    public function accountOfEmail(string $email): ?SecurityUser;

    public function accountOfUserName(string $userName): ?SecurityUser;

    public function save(Account $account): void;
}