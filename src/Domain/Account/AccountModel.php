<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Account\Values\AccountStatus;
use Hewsda64\Touiter\Domain\Account\Values\BcryptPassword;
use Hewsda64\Touiter\Domain\Account\Values\Name;
use Hewsda64\Touiter\Domain\AccountStat\AccountStat;
use Hewsda64\Touiter\Domain\Activation\Activation;
use Hewsda64\Touiter\Domain\Role\Role;
use Hewsda64\Touiter\Domain\Touit\Touit;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laraprooph\ModelEvent\ModelRoot;
use Thrust\Security\Contract\User\LocalUser;
use Thrust\Security\Contract\User\UserThrottle;
use Thrust\Security\Contract\User\Value\EmailAddress as EmailContract;
use Thrust\Security\Contract\User\Value\EncodedPassword;
use Thrust\Security\Contract\User\Value\UserId;
use Thrust\Security\Contract\User\Value\UserName;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\User\Value\EmailAddress;

abstract class AccountModel extends ModelRoot implements LocalUser, UserThrottle
{
    /**
     * @var string
     */
    protected $table = 'local_accounts';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'activated'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'account_role', 'account_id')
            ->withPivot(['assigner_id as assigner_id']);
    }

    public function touits(): HasMany
    {
        return $this->hasMany(Touit::class, 'author_id', 'id');
    }

    public function favorites(): BelongsToMany
    {
        return $this->belongsToMany(Touit::class, 'like_touits', 'account_id', 'id');
    }

    public function statistic(): HasOne
    {
        return $this->hasOne(AccountStat::class, 'id', 'id');
    }

    public function activation(): HasOne
    {
        return $this->hasOne(Activation::class, 'id', 'id');
    }

    public function getId(): UserId
    {
        if ($this->id instanceof UserId) {
            return $this->id;
        }

        return AccountId::fromString($this->getKey());
    }

    public function getIdentifier(): Identifier
    {
        return $this->getEmail();
    }

    public function getUserName(): UserName
    {
        if ($this->name instanceof Name) {
            return $this->name;
        }

        return Name::fromString($this->name);
    }

    public function getEmail(): EmailContract
    {
        if ($this->email instanceof EmailContract) {
            return $this->email;
        }

        return EmailAddress::fromString($this->email);
    }

    public function getPassword(): EncodedPassword
    {
        if ($this->password instanceof EncodedPassword) {
            return $this->password;
        }

        return BcryptPassword::fromString($this->password);
    }

    public function getRoles(): array
    {
        return $this->roles->pluck('name')->toArray();
    }

    public function getStatus(): AccountStatus
    {
        return AccountStatus::byValue($this->activated);
    }

    protected function aggregateId(): string
    {
        return $this->getKey();
    }
}