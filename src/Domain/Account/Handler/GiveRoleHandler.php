<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Handler;

use Hewsda64\Touiter\Application\Exceptions\TouiterException;
use Hewsda64\Touiter\Domain\Account\Command\GiveRole;
use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Rolable\RolableList;
use Hewsda64\Touiter\Domain\Role\Exceptions\RoleNotFound;
use Hewsda64\Touiter\Domain\Role\Repository\RoleList;

class GiveRoleHandler
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var RolableList
     */
    private $rolableList;

    /**
     * @var RoleList
     */
    private $roleList;

    /**
     * GiveRoleHandler constructor.
     *
     * @param AccountCollection $accountCollection
     * @param RolableList $rolableList
     * @param RoleList $roleList
     */
    public function __construct(AccountCollection $accountCollection, RolableList $rolableList, RoleList $roleList)
    {
        $this->accountCollection = $accountCollection;
        $this->rolableList = $rolableList;
        $this->roleList = $roleList;
    }

    public function __invoke(GiveRole $command): void
    {
        $accountId = $command->accountId()->identify();

        $account = $this->accountCollection->accountOfId($accountId);

        $roles = $this->getRoles($command->roles());

        $rolable = $this->rolableList->rolesOfAccount($accountId);

        foreach ($roles as $role) {
            if (!$rolable->contains($role)) {

                $givenRole = $account->giveRole($role);

                $this->rolableList->save($givenRole);
            }
        }
    }

    private function getRoles(array $roles): iterable
    {
        if (empty($roles)) {
            throw new TouiterException('Roles can not be empty.');
        }

        $exists = [];
        foreach ($roles as $role) {
            if (null === $role = $this->roleList->roleOfName($role)) {
                throw new RoleNotFound(
                    sprintf('Role %s not found.', $role)
                );
            }

            $exists[] = $role;
        }

        return $exists;
    }
}