<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Handler;

use Hewsda64\Touiter\Domain\Account\Command\ChangeLocalPassword;
use Hewsda64\Touiter\Domain\Account\Exceptions\AccountValidationException;
use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Account\Services\PasswordEncoder;
use Thrust\Security\Exception\UserNotFound;

class ChangeLocalPasswordHandler
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var PasswordEncoder
     */
    private $passwordEncoder;

    /**
     * ChangeLocalPasswordHandler constructor.
     *
     * @param AccountCollection $accountCollection
     * @param PasswordEncoder $passwordEncoder
     */
    public function __construct(AccountCollection $accountCollection, PasswordEncoder $passwordEncoder)
    {
        $this->accountCollection = $accountCollection;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function __invoke(ChangeLocalPassword $command): void
    {
        $accountId = $command->accountId()->identify();
        $account = $this->accountCollection->accountOfId($accountId);

        if (!$account) {
            throw new UserNotFound(sprintf('Account not found with id %s', $accountId));
        }

        if (!($this->passwordEncoder)->match(
            $command->currentPassword()->toString(),
            $account->getPassword()
        )) {
            throw new AccountValidationException('Current password does not match.');
        }

        $encodedPassword = ($this->passwordEncoder)($command->newPassword());

        $account->changePassword($encodedPassword);

        $this->accountCollection->save($account);
    }
}