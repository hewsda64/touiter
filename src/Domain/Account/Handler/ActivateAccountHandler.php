<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Handler;

use Hewsda64\Touiter\Domain\Account\Command\ActivateAccount;
use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Activation\Exceptions\ActivationTokenIsInvalid;
use Hewsda64\Touiter\Domain\Activation\Repository\ActivationCollection;
use Thrust\Security\Exception\UserNotFound;

class ActivateAccountHandler
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var ActivationCollection
     */
    private $activationCollection;

    /**
     * ActivateAccountHandler constructor.
     *
     * @param AccountCollection $accountCollection
     * @param ActivationCollection $activationCollection
     */
    public function __construct(AccountCollection $accountCollection, ActivationCollection $activationCollection)
    {
        $this->accountCollection = $accountCollection;
        $this->activationCollection = $activationCollection;
    }

    public function __invoke(ActivateAccount $command): void
    {
        $activation = $this->activationCollection->accountActivationOfToken($command->activationToken()->token());

        $exceptionMessage = 'Activation token is invalid or has been expired.';

        if (!$activation || !$activation->isStillValid()) {
            throw new ActivationTokenIsInvalid($exceptionMessage);
        }

        $account = $this->accountCollection->accountOfId($activation->getId()->identify());

        if (!$account || !$activation->getEmail()->sameValueAs($account->getEmail())) {
            throw new UserNotFound('Account not found.');
        }

        $account->activate();

        $this->accountCollection->save($account);
    }
}