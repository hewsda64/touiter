<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Handler;

use Hewsda64\Touiter\Domain\Account\Account;
use Hewsda64\Touiter\Domain\Account\Command\RegisterAccount;
use Hewsda64\Touiter\Domain\Account\Exceptions\AccountAlreadyExistsException;
use Hewsda64\Touiter\Domain\Account\Services\EmailAddressAvailability;
use Hewsda64\Touiter\Domain\Account\Services\PasswordEncoder;
use Hewsda64\Touiter\Infrastructure\Repository\Account\AccountCollection;

class RegisterAccountHandler
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var PasswordEncoder
     */
    private $encoder;

    /**
     * @var EmailAddressAvailability
     */
    private $uniqueEmail;

    /**
     * CreateAccountHandler constructor.
     *
     * @param AccountCollection $accountCollection
     * @param PasswordEncoder $encoder
     * @param EmailAddressAvailability $uniqueEmail
     */
    public function __construct(AccountCollection $accountCollection, PasswordEncoder $encoder, EmailAddressAvailability $uniqueEmail)
    {
        $this->accountCollection = $accountCollection;
        $this->encoder = $encoder;
        $this->uniqueEmail = $uniqueEmail;
    }

    public function __invoke(RegisterAccount $command): void
    {
        if (null !== ($this->uniqueEmail)($command->email())) {
            throw new AccountAlreadyExistsException(
                sprintf('Account with email %s already exists.', (string)$command->email())
            );
        }

        $password = ($this->encoder)($command->password());

        $account = Account::register($command->accountId(), $command->name(), $command->email(), $password);

        $this->accountCollection->save($account);
    }
}