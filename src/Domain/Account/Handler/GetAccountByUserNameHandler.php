<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Handler;

use Hewsda64\Touiter\Domain\Account\Query\GetAccountByUserName;
use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use React\Promise\Deferred;

class GetAccountByUserNameHandler
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * GetAccountByUserNameHandler constructor.
     *
     * @param AccountCollection $accountCollection
     */
    public function __construct(AccountCollection $accountCollection)
    {
        $this->accountCollection = $accountCollection;
    }

    public function __invoke(GetAccountByUserName $query, Deferred $deferred = null)
    {
        $account = $this->accountCollection->accountOfUserName($query->username()->toString());

        if (!$deferred) {
            return $account;
        }

        $deferred->resolve($account);
    }
}