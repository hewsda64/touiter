<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Exceptions;

use Hewsda64\Touiter\Application\Exceptions\TouiterException;

class AccountValidationException extends TouiterException
{
}