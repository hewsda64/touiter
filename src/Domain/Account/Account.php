<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account;

use Hewsda64\Touiter\Domain\Account\Event\AccountWasActivated;
use Hewsda64\Touiter\Domain\Account\Event\AccountWasRegistered;
use Hewsda64\Touiter\Domain\Account\Event\LocalPasswordWasChanged;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Account\Values\AccountStatus;
use Hewsda64\Touiter\Domain\Account\Values\Name;
use Hewsda64\Touiter\Domain\AccountStat\AccountStat;
use Hewsda64\Touiter\Domain\Activation\Activation;
use Hewsda64\Touiter\Domain\Activation\Exceptions\AccountAlreadyActivated;
use Hewsda64\Touiter\Domain\Activation\Values\ActivationToken;
use Hewsda64\Touiter\Domain\Favorite\Favorite;
use Hewsda64\Touiter\Domain\Follower\Follower;
use Hewsda64\Touiter\Domain\Rolable\Rolable;
use Hewsda64\Touiter\Domain\Role\Role;
use Hewsda64\Touiter\Domain\Touit\Touit;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Ramsey\Uuid\UuidInterface;
use Thrust\Security\Contract\User\Value\EmailAddress;
use Thrust\Security\Contract\User\Value\EncodedPassword;
use Thrust\Security\Contract\Value\Entity;

class Account extends AccountModel implements Entity
{
    public static function register(AccountId $accountId, Name $name, EmailAddress $emailAddress, EncodedPassword $password): self
    {
        $self = new self();
        $self->recordThat(AccountWasRegistered::withData($accountId, $name, $emailAddress, $password));

        return $self;
    }

    public function createStat(): AccountStat
    {
        return AccountStat::initialize($this->getId());
    }

    public function registerNotActivatedAccount(ActivationToken $activationToken): Activation
    {
        if ($this->isActivated()) {
            throw new AccountAlreadyActivated('Account already activated.');
        }

        return Activation::createNotActivatedAccount($this->getId(), $this->getEmail(), $activationToken);
    }

    public function giveRole(Role $role): Rolable
    {
        return Rolable::giveRole($this->getId(), $role);
    }

    public function activate(): void
    {
        if ($this->isActivated()) {
            throw new AccountAlreadyActivated('Account already activated.');
        }

        $this->recordThat(AccountWasActivated::forAccount($this->getId(), AccountStatus::ACTIVATED()));
    }

    public function postTouit(TouitId $touitId, string $body): Touit
    {
        return Touit::post($touitId, $this->getId(), $body);
    }

    public function replyTo(TouitId $touitId, string $body, UuidInterface $uid): Touit
    {
        return Touit::reply($touitId, $this->getId(), $uid, $body);
    }

    public function followAccount(UuidInterface $uid, AccountId $followingId): Follower
    {
        return Follower::follow($uid, $this->getId(), $followingId);
    }

    public function changePassword(EncodedPassword $password): void
    {
        $this->recordThat(LocalPasswordWasChanged::forAccount($this->getId(), $password));
    }

    public function favoriteTouit(TouitId $touitId): Favorite
    {
        return Favorite::createFavorite($this->getId(), $touitId);
    }

    protected function whenAccountWasRegistered(AccountWasRegistered $event): void
    {
        $this->id = $event->accountId();
        $this->name = $event->name();
        $this->email = $event->email();
        $this->password = $event->password();
        $this->created_at = $event->createdAt();
    }

    protected function whenLocalPasswordWasChanged(LocalPasswordWasChanged $event): void
    {
        $this->password = $event->password();
        $this->updated_at = $event->createdAt();
    }

    protected function whenAccountWasActivated(AccountWasActivated $event): void
    {
        $this->activated = $event->accountStatus();
        $this->updated_at = $event->createdAt();
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this && $this->getKey() === $aEntity->getKey();
    }

    public function isEnabled(): bool
    {
        return $this->isActivated();
    }

    public function isActivated(): bool
    {
        return AccountStatus::ACTIVATED()->sameValueAs($this->getStatus());
    }
}