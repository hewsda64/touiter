<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Account\Values\BcryptPassword;
use Hewsda64\Touiter\Domain\Account\Values\Name;
use Laraprooph\ModelEvent\ModelChanged;
use Thrust\Security\Contract\User\Value\EmailAddress;
use Thrust\Security\Contract\User\Value\EncodedPassword;
use Thrust\Security\Contract\User\Value\UserName;

class AccountWasRegistered extends ModelChanged
{
    public static function withData(AccountId $accountId, Name $name, EmailAddress $email, EncodedPassword $password): self
    {
        $self = self::occur($accountId->identify(), [
            'name' => (string)$name,
            'email' => (string)$email,
            'password' => (string)$password
        ]);

        return $self;
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->aggregateId());
    }

    public function name(): UserName
    {
        return Name::fromString($this->payload['name']);
    }

    public function email(): EmailAddress
    {
        return \Thrust\Security\User\Value\EmailAddress::fromString($this->payload['email']);
    }

    public function password(): EncodedPassword
    {
        return BcryptPassword::fromString($this->payload['password']);
    }
}