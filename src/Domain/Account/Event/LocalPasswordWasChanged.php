<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Account\Values\BcryptPassword;
use Laraprooph\ModelEvent\ModelChanged;
use Thrust\Security\Contract\User\Value\EncodedPassword;

class LocalPasswordWasChanged extends ModelChanged
{
    public static function forAccount(AccountId $accountId, EncodedPassword $password): self
    {
        $self = self::occur($accountId->identify(), [
            'password' => $password->toString()
        ]);

        return $self;
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->aggregateId());
    }

    public function password(): EncodedPassword
    {
        return BcryptPassword::fromString($this->payload['password']);
    }
}