<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Account\Values\AccountStatus;
use Laraprooph\ModelEvent\ModelChanged;

class AccountWasActivated extends ModelChanged
{
    public static function forAccount(AccountId $accountId, AccountStatus $accountStatus): AccountWasActivated
    {
        return self::occur($accountId->identify(), [
            'activated' => $accountStatus->getValue()
        ]);
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->aggregateId());
    }

    public function accountStatus(): AccountStatus
    {
        return AccountStatus::byValue($this->payload['activated']);
    }
}