<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Command;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Account\Values\ClearPassword;
use Hewsda64\Touiter\Domain\Account\Values\Name;
use Hewsda64\Touiter\Domain\Account\Values\Password;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Thrust\Security\Contract\User\Value\EmailAddress as EmailContract;
use Thrust\Security\Contract\User\Value\UserName;
use Thrust\Security\User\Value\EmailAddress;

class RegisterAccount extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function withData($accountId, $name, $email, $password, $passwordConfirmation): RegisterAccount
    {
        return new self([
            'id' => $accountId,
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $passwordConfirmation
        ]);
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->payload['id']);
    }

    public function name(): UserName
    {
        return Name::fromString($this->payload['name']);
    }

    public function email(): EmailContract
    {
        return EmailAddress::fromString($this->payload['email']);
    }

    public function password(): Password
    {
        return ClearPassword::fromStringWithConfirmation(
            $this->payload['password'], $this->payload['password_confirmation']
        );
    }
}