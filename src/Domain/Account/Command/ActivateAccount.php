<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Command;

use Hewsda64\Touiter\Domain\Activation\Values\ActivationToken;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;

class ActivateAccount extends Command implements PayloadConstructable, AsyncMessage
{
    use PayloadTrait;

    public static function withActivationToken($activation_token): self
    {
        return new self([
            'activation_token' => $activation_token
        ]);
    }

    public function activationToken(): ActivationToken
    {
        return ActivationToken::fromString(
            $this->payload['activation_token'],
            (new \DateTimeImmutable())->format('Y-m-d H:i:s')
        );
    }
}