<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Command;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Account\Values\ClearPassword;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class ChangeLocalPassword extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function forAccount($accountId, $currentPassword, $newPassword, $newPasswordConfirmation): self
    {
        return new self([
            'account_id' => $accountId,
            'current_password' => $currentPassword,
            'newPassword' => $newPassword,
            'newPasswordConfirmation' => $newPasswordConfirmation
        ]);
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->payload['account_id']);
    }

    public function currentPassword(): ClearPassword
    {
        return ClearPassword::fromString($this->payload['current_password']);
    }

    public function newPassword(): ClearPassword
    {
        return ClearPassword::fromStringWithConfirmation(
            $this->payload['newPassword'],
            $this->payload['newPasswordConfirmation']
        );
    }
}