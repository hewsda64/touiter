<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Command;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;

class GiveRole extends Command implements PayloadConstructable, AsyncMessage
{
    use PayloadTrait;

    public static function forAccount($accountId, array $roles = []): self
    {
        return new self([
            'account_id' => $accountId,
            'roles' => $roles
        ]);
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->payload['account_id']);
    }

    public function roles(): iterable
    {
        return $this->payload['roles'];
    }
}