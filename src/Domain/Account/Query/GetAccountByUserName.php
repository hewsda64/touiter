<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Account\Query;

use Hewsda64\Touiter\Domain\Account\Values\Name;
use Thrust\Security\Contract\User\Value\UserName;

class GetAccountByUserName
{
    /**
     * @var string
     */
    private $userName;

    /**
     * GetAccountByUserName constructor.
     *
     * @param $userName
     */
    public function __construct($userName)
    {
        $this->userName = $userName;
    }

    public function username(): UserName
    {
        return Name::fromString($this->userName);
    }
}