<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation;

use Hewsda64\Touiter\Domain\Account\Account;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Activation\Values\ActivationToken;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laraprooph\ModelEvent\ModelRoot;
use Thrust\Security\Contract\User\Value\EmailAddress as EmailContract;
use Thrust\Security\Contract\Value\Entity;
use Thrust\Security\User\Value\EmailAddress;

abstract class ActivationModel extends ModelRoot implements Entity
{
    /**
     * @var string
     */
    protected $table = 'account_activation';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'email', 'activation_token', 'created_at'
    ];

    /**
     * @var array
     */
    protected $hidden = ['activation_token'];

    /**
     * @var string
     */
    protected $keyType = 'string';

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, 'id', 'id');
    }

    public function getId(): AccountId
    {
        if ($this->id instanceof AccountId) {
            return $this->id;
        }

        return AccountId::fromString($this->id);
    }

    public function getEmail(): EmailContract
    {
        if ($this->email instanceof EmailContract) {
            return $this->email;
        }

        return EmailAddress::fromString($this->email);
    }

    public function getActivationToken(): ActivationToken
    {
        return ActivationToken::fromString(
            $this->activation_token,
            $this->createdAt()->format('Y-m-d H:i:s')
        );
    }

    public function getToken(): string
    {
        return $this->getActivationToken()->token();
    }

    public function isStillValid(): bool
    {
        return !$this->getActivationToken()->isExpired();
    }

    protected function aggregateId(): string
    {
        return $this->getId()->identify();
    }
}