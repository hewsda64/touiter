<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation\Command;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;

class DeleteAccountActivation extends Command implements PayloadConstructable, AsyncMessage
{
    use PayloadTrait;

    public static function forAccount($accountId): self
    {
        return new self([
            'account_id' => $accountId
        ]);
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->payload['account_id']);
    }
}