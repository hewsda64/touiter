<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation\Command;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Activation\Values\ActivationToken;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;
use Thrust\Security\Contract\User\Value\EmailAddress as EmailContract;
use Thrust\Security\User\Value\EmailAddress;

class RegisterNotActivatedAccount extends Command implements PayloadConstructable, AsyncMessage
{
    use PayloadTrait;

    public static function forAccount($accountId, $email, $activationToken): self
    {
        return new self([
            'account_id' => $accountId,
            'email' => $email,
            'activation_token' => $activationToken
        ]);
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->payload['account_id']);
    }

    public function email(): EmailContract
    {
        return EmailAddress::fromString($this->payload['email']);
    }

    public function activationToken(): ActivationToken
    {
        return ActivationToken::fromString(
            $this->payload['activation_token'],
            $this->createdAt()->format('Y-m-d H:i:s')
        );
    }
}