<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation\Repository;

use Hewsda64\Touiter\Domain\Activation\Activation;

interface ActivationCollection
{
    public function accountActivationOfId(string $accountId): ?Activation;

    public function accountActivationOfToken(string $activationToken): ?Activation;

    public function save(Activation $activation): void;
}