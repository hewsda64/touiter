<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation\Exceptions;

use Thrust\Security\Exception\UserStatusException;

class AccountAlreadyActivated extends UserStatusException
{

}