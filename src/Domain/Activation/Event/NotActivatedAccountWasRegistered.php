<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Activation\Values\ActivationToken;
use Laraprooph\ModelEvent\ModelChanged;
use Thrust\Security\Contract\User\Value\EmailAddress as EmailContract;
use Thrust\Security\User\Value\EmailAddress;

class NotActivatedAccountWasRegistered extends ModelChanged
{
    public static function forAccount(AccountId $accountId, EmailContract $emailAddress, ActivationToken $activationToken): self
    {
        $self = self::occur($accountId->identify(), [
            'email' => $emailAddress->identify(),
            'activation_token' => $activationToken->token(),
            'created_at' => $activationToken->createdAt()->format('Y-m-d H:i:s')
        ]);

        return $self;
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->aggregateId());
    }

    public function email(): EmailContract
    {
        return EmailAddress::fromString($this->payload['email']);
    }

    public function activationToken(): ActivationToken
    {
        return ActivationToken::fromString(
            $this->payload['activation_token'],
            (new \DateTimeImmutable($this->payload['created_at']))->format('Y-m-d H:i:s')
        );
    }
}