<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Laraprooph\ModelEvent\ModelChanged;

class AccountActivationWasDeleted extends ModelChanged
{
    public static function forAccount(AccountId $accountId): AccountActivationWasDeleted
    {
        return self::occur($accountId->identify());
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->aggregateId());
    }
}