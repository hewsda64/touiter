<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation\Values;

use Hewsda64\Touiter\Application\Exceptions\Assertion;
use Thrust\Security\Contract\Value\SecurityValue;

class ActivationToken implements SecurityValue
{
    const TOKEN_LENGTH = 32;

    const TOKEN_EXPIRATION = 'PT12H';

    /**
     * @var string
     */
    private $token;

    /**
     * @var \DateTimeImmutable
     */
    private $createdAt;

    /**
     * ActivationToken constructor.
     *
     * @param string $token
     * @param \DateTimeImmutable $createdAt
     */
    private function __construct(string $token, \DateTimeImmutable $createdAt)
    {
        $this->token = $token;
        $this->createdAt = $createdAt;
    }

    public static function fromString($token, $createdAt): self
    {
        $message = 'Activation token is invalid.';

        Assertion::string($token, $message);
        Assertion::length($token, self::TOKEN_LENGTH, $message);

        if (is_string($createdAt)) {
            $createdAt = new \DateTimeImmutable($createdAt);
        }

        return new self($token, clone $createdAt);
    }

    public static function nextToken(): self
    {
        return new self(str_random(self::TOKEN_LENGTH), new \DateTimeImmutable());
    }

    public function token(): string
    {
        return $this->token;
    }

    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function isExpired(): bool
    {
        return $this->createdAt()
                ->add(new \DateInterval(self::TOKEN_EXPIRATION))
            < new \DateTimeImmutable();
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this
            && $this->token() === $aValue->token()
            && $this->createdAt() === $aValue->createdAt();
    }

    public function __toString(): string
    {
        return $this->token();
    }
}