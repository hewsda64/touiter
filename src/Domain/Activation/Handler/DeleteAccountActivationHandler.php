<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation\Handler;

use Hewsda64\Touiter\Domain\Activation\Command\DeleteAccountActivation;
use Hewsda64\Touiter\Domain\Activation\Repository\ActivationCollection;
use Thrust\Security\Exception\UserNotFound;

class DeleteAccountActivationHandler
{
    /**
     * @var ActivationCollection
     */
    private $activationCollection;

    /**
     * DeleteAccountActivationHandler constructor.
     *
     * @param ActivationCollection $activationCollection
     */
    public function __construct(ActivationCollection $activationCollection)
    {
        $this->activationCollection = $activationCollection;
    }

    public function __invoke(DeleteAccountActivation $command): void
    {
        $accountId = $command->accountId()->identify();

        $activation = $this->activationCollection->accountActivationOfId($accountId);

        if (!$activation) {
            throw new UserNotFound('Account not found.');
        }

        $activation->deleteActivation();

        $this->activationCollection->save($activation);
    }
}