<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation\Handler;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Activation\Command\RegisterNotActivatedAccount;
use Hewsda64\Touiter\Domain\Activation\Exceptions\AccountAlreadyActivated;
use Hewsda64\Touiter\Domain\Activation\Repository\ActivationCollection;
use Thrust\Security\Exception\UserNotFound;

class RegisterNotActivatedAccountHandler
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var ActivationCollection
     */
    private $activationCollection;

    /**
     * RegisterNotActivatedAccountHandler constructor.
     *
     * @param AccountCollection $accountCollection
     * @param ActivationCollection $activationCollection
     */
    public function __construct(AccountCollection $accountCollection, ActivationCollection $activationCollection)
    {
        $this->accountCollection = $accountCollection;
        $this->activationCollection = $activationCollection;
    }

    public function __invoke(RegisterNotActivatedAccount $command): void
    {
        $accountId = $command->accountId()->identify();

        $account = $this->accountCollection->accountOfId($accountId);
        if (!$account) {
            throw new UserNotFound(sprintf('Account not found with id %s', $accountId));
        }

        $activation = $account->registerNotActivatedAccount($command->activationToken());

        $this->activationCollection->save($activation);
    }
}