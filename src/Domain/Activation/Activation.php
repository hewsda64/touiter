<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Activation;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Activation\Event\AccountActivationWasDeleted;
use Hewsda64\Touiter\Domain\Activation\Event\NotActivatedAccountWasRegistered;
use Hewsda64\Touiter\Domain\Activation\Values\ActivationToken;
use Thrust\Security\Contract\User\Value\EmailAddress;
use Thrust\Security\Contract\Value\Entity;

class Activation extends ActivationModel
{

    public static function createNotActivatedAccount(AccountId $accountId, EmailAddress $emailAddress, ActivationToken $activationToken): Activation
    {
        $self = new self();
        $self->recordThat(NotActivatedAccountWasRegistered::forAccount(
            $accountId, $emailAddress, $activationToken
        ));

        return $self;
    }

    public function deleteActivation(): void
    {
        $this->recordThat(AccountActivationWasDeleted::forAccount($this->getId()));
    }

    protected function whenNotActivatedAccountWasRegistered(NotActivatedAccountWasRegistered $event): void
    {
        $this->id = $event->accountId();
        $this->email = $event->email();
        $this->activation_token = $event->activationToken()->token();
        $this->created_at = $event->activationToken()->createdAt();
    }

    protected function whenAccountActivationWasDeleted(AccountActivationWasDeleted $event): void
    {
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this && $this->getId()->sameValueAs($aEntity->getId());
    }
}