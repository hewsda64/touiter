<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Exceptions;

use Hewsda64\Touiter\Application\Exceptions\TouiterException;

class TouitNotFound extends TouiterException
{

}