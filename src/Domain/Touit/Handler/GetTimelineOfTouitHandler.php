<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Handler;

use Hewsda64\Touiter\Domain\Touit\Query\GetTimelineOfTouit;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;
use React\Promise\Deferred;

class GetTimelineOfTouitHandler
{
    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * GetTimelineOfTouitHandler constructor.
     *
     * @param TouitCollection $touitCollection
     */
    public function __construct(TouitCollection $touitCollection)
    {
        $this->touitCollection = $touitCollection;
    }

    public function __invoke(GetTimelineOfTouit $query, Deferred $deferred = null)
    {
        $touit = $this->touitCollection->timelineOfTouitId($query->touitId()->toString());

        if (!$deferred) {
            return $touit;
        }

        $deferred->resolve($touit);
    }
}