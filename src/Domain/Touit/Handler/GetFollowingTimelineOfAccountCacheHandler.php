<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Handler;

use Hewsda64\Touiter\Domain\Touit\Query\GetFollowingTimelineOfAccountCache;
use Hewsda64\Touiter\Infrastructure\Repository\Touit\TouitRepositoryCache;
use React\Promise\Deferred;

class GetFollowingTimelineOfAccountCacheHandler
{

    /**
     * @var TouitRepositoryCache
     */
    private $repository;

    /**
     * GetFollowingTimelineOfAccountCacheHandler constructor.
     *
     * @param TouitRepositoryCache $repository
     */
    public function __construct(TouitRepositoryCache $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(GetFollowingTimelineOfAccountCache $query, Deferred $deferred = null)
    {
        $timeline = $this->repository->followingTimelineOfAuthor($query->accountId()->identify());

        if (!$deferred) {
            return $timeline;
        }

        $deferred->resolve($timeline);
    }
}