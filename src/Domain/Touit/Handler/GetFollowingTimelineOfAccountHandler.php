<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Handler;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Touit\Query\GetFollowingTimelineOfAccount;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;
use React\Promise\Deferred;

class GetFollowingTimelineOfAccountHandler
{

    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * GetFollowingTimelineOfAccountHandler constructor.
     *
     * @param AccountCollection $accountCollection
     * @param TouitCollection $touitCollection
     */
    public function __construct(AccountCollection $accountCollection, TouitCollection $touitCollection)
    {
        $this->accountCollection = $accountCollection;
        $this->touitCollection = $touitCollection;
    }

    public function __invoke(GetFollowingTimelineOfAccount $query, Deferred $deferred = null)
    {
        $touits = $this->touitCollection->followingTimelineOfAuthor($query->accountId()->identify());

        if (!$deferred) {
            return $touits;
        }

        $deferred->resolve($touits);
    }
}