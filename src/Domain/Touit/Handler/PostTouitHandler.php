<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Handler;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Touit\Command\PostTouit;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;
use Thrust\Security\Exception\UserNotFound;

class PostTouitHandler
{
    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * PostTouitHandler constructor.
     *
     * @param TouitCollection $touitCollection
     * @param AccountCollection $accountCollection
     */
    public function __construct(TouitCollection $touitCollection, AccountCollection $accountCollection)
    {
        $this->touitCollection = $touitCollection;
        $this->accountCollection = $accountCollection;
    }

    public function __invoke(PostTouit $command): void
    {
        $authorId = $command->authorId()->identify();
        $author = $this->accountCollection->accountOfId($authorId);

        if (!$author) {
            throw new UserNotFound(sprintf('Account not found with id', $authorId));
        }

        $touit = $author->postTouit($command->getTouitId(), $command->getBody());

        $this->touitCollection->save($touit);
    }
}