<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Handler;

use Hewsda64\Touiter\Domain\Touit\Query\GetTimelineOfAccount;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;
use React\Promise\Deferred;

class GetTimelineOfAccountHandler
{

    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * GetTimelineForAccountHandler constructor.
     *
     * @param TouitCollection $touitCollection
     */
    public function __construct(TouitCollection $touitCollection)
    {
        $this->touitCollection = $touitCollection;
    }

    public function __invoke(GetTimelineOfAccount $query, Deferred $deferred = null)
    {
        $timeline = $this->touitCollection->timelineOfAuthorId($query->accountId()->identify());

        if (!$deferred) {
            return $timeline;
        }

        $deferred->resolve($timeline);
    }
}