<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Handler;

use Hewsda64\Touiter\Domain\Touit\Query\GetTimelineOfAccountCache;
use Hewsda64\Touiter\Infrastructure\Repository\Touit\TouitRepositoryCache;
use React\Promise\Deferred;

class GetTimelineOfAccountCacheHandler
{
    /**
     * @var TouitRepositoryCache
     */
    private $repository;

    /**
     * GetCacheTimelineOfAuthorHandler constructor.
     *
     * @param TouitRepositoryCache $repository
     */
    public function __construct(TouitRepositoryCache $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(GetTimelineOfAccountCache $command, Deferred $deferred = null)
    {
        $timeline = $this->repository->timelineOfAuthorId($command->accountId()->identify());

        if (!$deferred) {
            return $timeline;
        }

        $deferred->resolve($timeline);
    }
}