<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Handler;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Touit\Command\ReplyToTouit;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;

class ReplyToTouitHandler
{
    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * ReplyToTouitHandler constructor.
     *
     * @param TouitCollection $touitCollection
     * @param AccountCollection $accountCollection
     */
    public function __construct(TouitCollection $touitCollection, AccountCollection $accountCollection)
    {
        $this->touitCollection = $touitCollection;
        $this->accountCollection = $accountCollection;
    }

    public function __invoke(ReplyToTouit $command)
    {
        $account = $this->accountCollection->accountOfId($command->authorId()->identify());

        $touit = $this->touitCollection->touitOfId($command->getTouitId()->toString());

        $touit = $account->replyTo($touit->getId(), $command->getBody(), $command->getUid());

        $this->touitCollection->save($touit);
    }
}