<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Handler;

use Hewsda64\Touiter\Domain\Touit\Command\IncrementRepliesTouit;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;

class IncrementRepliesTouitHandler
{
    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * IncrementRepliesTouitHandler constructor.
     *
     * @param TouitCollection $touitCollection
     */
    public function __construct(TouitCollection $touitCollection)
    {
        $this->touitCollection = $touitCollection;
    }

    public function __invoke(IncrementRepliesTouit $command): void
    {
        $touit = $this->touitCollection->touitOfId($command->touitId()->toString());

        $touit->incrementRepliesCount();

        $this->touitCollection->save($touit);
    }
}