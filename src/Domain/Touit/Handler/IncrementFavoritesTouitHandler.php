<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Handler;

use Hewsda64\Touiter\Domain\Touit\Command\IncrementFavoritesTouit;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;

class IncrementFavoritesTouitHandler
{
    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * IncrementTouitFavoriteCounterHandler constructor.
     *
     * @param TouitCollection $touitCollection
     */
    public function __construct(TouitCollection $touitCollection)
    {
        $this->touitCollection = $touitCollection;
    }

    public function __invoke(IncrementFavoritesTouit $command): void
    {
        $touit = $this->touitCollection->touitOfId($command->touitId()->toString());

        $touit->favorite();

        $this->touitCollection->save($touit);
    }
}