<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Repository;

use Hewsda64\Touiter\Domain\Touit\Touit;

interface TouitCollection
{
    public function save(Touit $root): void;
}