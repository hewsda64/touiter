<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Event;

use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Laraprooph\ModelEvent\ModelChanged;

class TouitWasFavorite extends ModelChanged
{
    public static function for(TouitId $touitId): self
    {
        return self::occur($touitId->toString());
    }

    public function touitId(): TouitId
    {
        return TouitId::fromString($this->aggregateId());
    }
}