<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Laraprooph\ModelEvent\ModelChanged;

class TouitWasPosted extends ModelChanged
{
    public static function withData(TouitId $touitId, AccountId $accountId, string $body): self
    {
        $self = self::occur($touitId->toString(), [
            'author_id' => $accountId->identify(),
            'body' => $body
        ]);

        return $self;
    }

    public function getTouitId(): TouitId
    {
        return TouitId::fromString($this->aggregateId());
    }

    public function authorId(): AccountId
    {
        return AccountId::fromString($this->payload['author_id']);
    }

    public function getBody(): string
    {
        return $this->payload['body'];
    }
}