<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Laraprooph\ModelEvent\ModelChanged;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ReplyToTouitWasPosted extends ModelChanged
{
    public static function forTouit(UuidInterface $uid, TouitId $touitId, AccountId $authorId, string $body): self
    {
        return self::occur($uid->toString(), [
            'touit_id' => $touitId->toString(),
            'author_id' => $authorId->identify(),
            'body' => $body
        ]);
    }

    public function getUid(): UuidInterface
    {
        return Uuid::fromString($this->aggregateId());
    }

    public function getTouitId(): TouitId
    {
        return TouitId::fromString($this->payload['touit_id']);
    }

    public function authorId(): AccountId
    {
        return AccountId::fromString($this->payload['author_id']);
    }

    public function getBody(): string
    {
        return $this->payload['body'];
    }
}