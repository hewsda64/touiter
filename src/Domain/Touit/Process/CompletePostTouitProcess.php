<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Process;

use Hewsda64\Touiter\Domain\AccountStat\Command\IncrementTouitCounter;
use Hewsda64\Touiter\Domain\Touit\Event\TouitWasPosted;
use Hewsda64\Touiter\Domain\Touit\Exceptions\TouitNotFound;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;

class CompletePostTouitProcess
{
    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * @var AsyncCommandBus
     */
    private $bus;

    /**
     * CompletePostTouitProcess constructor.
     *
     * @param TouitCollection $touitCollection
     * @param AsyncCommandBus $bus
     */
    public function __construct(TouitCollection $touitCollection, AsyncCommandBus $bus)
    {
        $this->touitCollection = $touitCollection;
        $this->bus = $bus;
    }

    public function onEvent(TouitWasPosted $event): void
    {
        $touitId = $event->getTouitId()->toString();
        $touit = $this->touitCollection->touitOfId($touitId);

        if (!$touit) {
            throw new TouitNotFound(sprintf('Touit not found with id %s', $touitId));
        }

        $this->bus->dispatch(IncrementTouitCounter::forAccount($touit->authorId()->identify()));
    }
}