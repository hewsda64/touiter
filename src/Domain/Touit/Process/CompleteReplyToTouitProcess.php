<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Process;

use Hewsda64\Touiter\Domain\AccountStat\Command\IncrementTouitCounter;
use Hewsda64\Touiter\Domain\Touit\Command\IncrementRepliesTouit;
use Hewsda64\Touiter\Domain\Touit\Event\ReplyToTouitWasPosted;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;

class CompleteReplyToTouitProcess
{

    /**
     * @var AsyncCommandBus
     */
    private $commandBus;

    /**
     * CompleteReplyToTouitProcess constructor.
     *
     * @param AsyncCommandBus $commandBus
     */
    public function __construct(AsyncCommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function onEvent(ReplyToTouitWasPosted $event): void
    {
        $this->commandBus->dispatch(IncrementRepliesTouit::forTouit($event->getTouitId()->toString()));

        $this->commandBus->dispatch(IncrementTouitCounter::forAccount($event->authorId()->identify()));
    }
}