<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Values;

use Hewsda64\Touiter\Application\Exceptions\Assertion;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Thrust\Security\Contract\Value\SecurityValue;

class TouitId implements SecurityValue
{
    /**
     * @var UuidInterface
     */
    private $uid;

    /**
     * TouitId constructor.
     *
     * @param UuidInterface $uid
     */
    private function __construct(UuidInterface $uid)
    {
        $this->uid = $uid;
    }

    public static function nextIdentity(): self
    {
        return new self(Uuid::uuid4());
    }

    public static function fromString($uid): self
    {
        Assertion::string($uid);
        Assertion::notEmpty($uid);

        return new self(Uuid::fromString($uid));
    }

    public function getUid(): UuidInterface
    {
        return $this->uid;
    }

    public function toString(): string
    {
        return $this->uid->toString();
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->getUid()->equals($aValue->getUid());
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}