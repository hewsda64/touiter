<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit;

use Hewsda64\Touiter\Domain\Account\Account;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laraprooph\ModelEvent\ModelRoot;

class TouitModel extends ModelRoot
{
    /**
     * @var string
     */
    protected $table = 'touits';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'author_id', 'body', 'status', 'reply_to_touit', 'reply_count', 'retouit_count', 'favorite_count'
    ];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string
     */
    public $incrementing = false;

    public function author(): BelongsTo
    {
        return $this->belongsTo(Account::class, 'author_id', 'id');
    }

    public function replies(): HasMany
    {
        return $this->hasMany(Touit::class, 'reply_to_touit', 'id');
    }

    protected function aggregateId(): string
    {
        return $this->id;
    }

    public function getId(): TouitId
    {
        if ($this->id instanceof TouitId) {
            return $this->id;
        }

        return TouitId::fromString($this->id);
    }

    public function authorId(): AccountId
    {
        if ($this->author_id instanceof AccountId) {
            return $this->author_id;
        }

        return AccountId::fromString($this->author_id);
    }

    public function getFavoriteCount(): int
    {
        return $this->favorite_count;
    }

    public function getReplyCount(): int
    {
        return $this->reply_count;
    }
}