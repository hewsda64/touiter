<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Command;

use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;

class IncrementFavoritesTouit extends Command implements PayloadConstructable, AsyncMessage
{
    use PayloadTrait;

    public static function forTouit($touitId): self
    {
        return new self(['touit_id' => $touitId]);
    }

    public function touitId(): TouitId
    {
        return TouitId::fromString($this->payload['touit_id']);
    }
}