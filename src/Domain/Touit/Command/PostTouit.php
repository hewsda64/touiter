<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Command;

use Hewsda64\Touiter\Application\Exceptions\Assertion;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;

class PostTouit extends Command implements PayloadConstructable, AsyncMessage
{
    use PayloadTrait;

    public static function withData($touitId, $authorId, $body): self
    {
        return new self([
            'id' => $touitId,
            'author_id' => $authorId,
            'body' => $body
        ]);
    }

    public function getTouitId(): TouitId
    {
        return TouitId::fromString($this->payload['id']);
    }

    public function authorId(): AccountId
    {
        return AccountId::fromString($this->payload['author_id']);
    }

    public function getBody(): string
    {
        Assertion::string($this->payload['body'], 'Bad format message.');
        Assertion::notEmpty($this->payload['body'], 'Message can not be empty');

        return $this->payload['body'];
    }
}