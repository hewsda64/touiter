<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Command;

use Hewsda64\Touiter\Application\Exceptions\Assertion;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ReplyToTouit extends Command implements PayloadConstructable, AsyncMessage
{
    use PayloadTrait;

    public static function forTouit($replyId, $touitId, $authorId, $body)
    {
        return new self([
            'uid' => $replyId,
            'touit_id' => $touitId,
            'author_id' => $authorId,
            'body' => $body
        ]);
    }

    public function getUid(): UuidInterface
    {
        return Uuid::fromString($this->payload['uid']);
    }

    public function getTouitId(): TouitId
    {
        return TouitId::fromString($this->payload['touit_id']);
    }

    public function authorId(): AccountId
    {
        return AccountId::fromString($this->payload['author_id']);
    }

    public function getBody(): string
    {
        Assertion::string($this->payload['body']);
        Assertion::notEmpty($this->payload['body']);

        return $this->payload['body'];
    }
}