<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Query;

use Hewsda64\Touiter\Domain\Touit\Values\TouitId;

class GetTimelineOfTouit
{
    /**
     * @var string
     */
    private $touitId;

    /**
     * GetTimelineOfTouit constructor.
     *
     * @param $touitId
     */
    public function __construct($touitId)
    {
        $this->touitId = $touitId;
    }

    public function touitId(): TouitId
    {
        return TouitId::fromString($this->touitId);
    }
}