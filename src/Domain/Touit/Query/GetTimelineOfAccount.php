<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit\Query;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;

class GetTimelineOfAccount
{
    /**
     * @var string
     */
    private $accountId;

    /**
     * GetTimelineForAuthor constructor.
     *
     * @param $accountId
     */
    public function __construct($accountId)
    {
        $this->accountId = $accountId;
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->accountId);
    }
}