<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Touit;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Touit\Event\RepliesCounterWasIncremented;
use Hewsda64\Touiter\Domain\Touit\Event\ReplyToTouitWasPosted;
use Hewsda64\Touiter\Domain\Touit\Event\TouitWasFavorite;
use Hewsda64\Touiter\Domain\Touit\Event\TouitWasPosted;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Ramsey\Uuid\UuidInterface;

class Touit extends TouitModel
{
    public static function post(TouitId $touitId, AccountId $authorId, string $body): self
    {
        $self = new self();
        $self->recordThat(TouitWasPosted::withData($touitId, $authorId, $body));

        return $self;
    }

    public static function reply(TouitId $touitId, AccountId $authorId, UuidInterface $uid, string $body): Touit
    {
        $self = new self();
        $self->recordThat(
            ReplyToTouitWasPosted::forTouit($uid, $touitId, $authorId, $body)
        );

        return $self;
    }

    public function favorite(): void
    {
        $this->recordThat(TouitWasFavorite::for ($this->getId()));
    }

    public function incrementRepliesCount(): void
    {
        $this->recordThat(RepliesCounterWasIncremented::forTouit($this->getId()));
    }

    protected function whenTouitWasPosted(TouitWasPosted $event): void
    {
        $this->id = $event->getTouitId();
        $this->author_id = $event->authorId();
        $this->body = $event->getBody();
        $this->created_at = $event->createdAt();

        $this->reply_count = 0;
        $this->retouit_count = 0;
        $this->favorite_count = 0;
        $this->moderated = 0;
        $this->reply_to_touit = null;
    }

    protected function whenReplyToTouitWasPosted(ReplyToTouitWasPosted $event): void
    {
        $this->id = $event->getUid();
        $this->author_id = $event->authorId();
        $this->body = $event->getBody();
        $this->created_at = $event->createdAt();
        $this->reply_to_touit = $event->getTouitId();

        $this->reply_count = 0;
        $this->retouit_count = 0;
        $this->favorite_count = 0;
        $this->moderated = 0;
    }

    protected function whenTouitWasFavorite(TouitWasFavorite $event): void
    {
        ++$this->favorite_count;

        $this->updated_at = $event->createdAt();
    }

    protected function whenRepliesCounterWasIncremented(RepliesCounterWasIncremented $event): void
    {
        ++$this->reply_count;

        $this->updated_at = $event->createdAt();
    }
}