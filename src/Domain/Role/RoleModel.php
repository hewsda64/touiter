<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Role;

use Hewsda64\Touiter\Domain\Role\Value\RoleName;
use Laraprooph\ModelEvent\ModelRoot;
use Thrust\Security\Contract\Role\Role as RoleContract;

abstract class RoleModel extends ModelRoot implements RoleContract
{
    /**
     * @var string
     */
    protected $table = 'roles';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'slug', 'description',
    ];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var bool
     */
    public $incrementing = false;

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getRole(): string
    {
        return $this->name;
    }

    protected function aggregateId(): string
    {
        return $this->getKey();
    }

    public function __toString(): string
    {
        return parent::__toString();
    }
}