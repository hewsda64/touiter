<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Role\Repository;

use Hewsda64\Touiter\Domain\Role\Role;

interface RoleList
{
    public function roleOfId(string $roleId): ?Role;

    public function roleOfName(string $roleName): ?Role;

    public function has(string $roleName): bool;

    public function save(Role $role): void;
}