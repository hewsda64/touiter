<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Role\Value;

use Thrust\Security\Contract\Value\SecurityValue;
use Thrust\Security\Foundation\Assertion\Assertion;

class RoleName implements SecurityValue
{
    const PREFIX = 'ROLE_';
    /**
     * @var string
     */
    private $name;

    /**
     * RoleName constructor.
     *
     * @param string $name
     */
    private function __construct(string $name)
    {
        $this->name = $name;
    }

    public static function fromString($roleName): self
    {
        Assertion::string($roleName);
        Assertion::startsWith($roleName, self::PREFIX);

        return new self(strtoupper($roleName));
    }

    public function toString(): string
    {
        return $this->name;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->name === $aValue->toString();
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}