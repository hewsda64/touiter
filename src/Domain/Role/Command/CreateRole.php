<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Role\Command;

use Hewsda64\Touiter\Application\Exceptions\Assertion;
use Hewsda64\Touiter\Domain\Role\Value\RoleName;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;
use Ramsey\Uuid\Uuid;

class CreateRole extends Command implements PayloadConstructable, AsyncMessage
{
    use PayloadTrait;

    public static function withData($roleId, $roleName, $description): self
    {
        return new self([
            'id' => $roleId,
            'name' => $roleName,
            'description' => $description
        ]);
    }

    public function roleId(): Uuid
    {
        return Uuid::fromString($this->payload['id']);
    }

    public function name(): RoleName
    {
        return RoleName::fromString($this->payload['name']);
    }

    public function description(): string
    {
        Assertion::string($this->payload['description']);

        return $this->payload['description'];
    }

    public function slug(): string
    {
        return str_slug($this->name()->toString());
    }
}