<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Role;

use Hewsda64\Touiter\Domain\Role\Event\RoleWasCreated;
use Hewsda64\Touiter\Domain\Role\Value\RoleName;
use Ramsey\Uuid\Uuid;
use Thrust\Security\Contract\Value\Entity;

class Role extends RoleModel implements Entity
{

    public static function createRole(Uuid $roleId, RoleName $name, string $description, string $slug): self
    {
        $self = new self();
        $self->recordThat(RoleWasCreated::withData($roleId, $name, $slug, $description));

        return $self;
    }

    protected function whenRoleWasCreated(RoleWasCreated $event): void
    {
        $this->id = $event->roleId();
        $this->name = $event->name();
        $this->description = $event->description();
        $this->slug = $event->slug();
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this && $this->getKey() === $aEntity->getKey();
    }
}