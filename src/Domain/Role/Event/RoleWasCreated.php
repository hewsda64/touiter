<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Role\Event;

use Hewsda64\Touiter\Domain\Role\Value\RoleName;
use Laraprooph\ModelEvent\ModelChanged;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class RoleWasCreated extends ModelChanged
{

    public static function withData(Uuid $roleId, RoleName $name, string $slug, string $description): self
    {
        $self = self::occur($roleId->toString(), [
            'name' => $name->toString(),
            'slug' => $slug,
            'description' => $description
        ]);

        return $self;
    }

    public function roleId(): UuidInterface
    {
        return Uuid::fromString($this->aggregateId());
    }

    public function name(): RoleName
    {
        return RoleName::fromString($this->payload['name']);
    }

    public function slug(): string
    {
        return $this->payload['slug'];
    }

    public function description(): string
    {
        return $this->payload['description'];
    }
}