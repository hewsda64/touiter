<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Role\Handler;

use Hewsda64\Touiter\Domain\Role\Command\CreateRole;
use Hewsda64\Touiter\Domain\Role\Exceptions\RoleAlreadyExists;
use Hewsda64\Touiter\Domain\Role\Repository\RoleList;
use Hewsda64\Touiter\Domain\Role\Role;

class CreateRoleHandler
{
    /**
     * @var RoleList
     */
    private $roleList;

    /**
     * CreateRoleHandler constructor.
     *
     * @param RoleList $roleList
     */
    public function __construct(RoleList $roleList)
    {
        $this->roleList = $roleList;
    }

    public function __invoke(CreateRole $command): void
    {
        $roleName = $command->name()->toString();

        if (null !== $this->roleList->roleOfName($roleName)) {
            throw new RoleAlreadyExists(
                sprintf('Role with name %s already exists.', $roleName)
            );
        }

        $role = Role::createRole($command->roleId(), $command->name(), $command->description(), $command->slug());

        $this->roleList->save($role);
    }
}