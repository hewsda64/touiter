<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Role\Exceptions;

use Hewsda64\Touiter\Application\Exceptions\TouiterException;

class RoleNotFound extends TouiterException
{
}