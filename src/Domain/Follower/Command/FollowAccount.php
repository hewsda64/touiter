<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower\Command;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class FollowAccount extends Command implements PayloadConstructable, AsyncMessage
{
    use PayloadTrait;

    public static function forAccount($uid, $followerId, $followingId): self
    {
        return new self([
            'uid' => $uid,
            'follower_id' => $followerId,
            'following_id' => $followingId
        ]);
    }

    public function getUid(): UuidInterface
    {
        return Uuid::fromString($this->payload['uid']);
    }

    public function getFollowerId(): AccountId
    {
        return AccountId::fromString($this->payload['follower_id']);
    }

    public function getFollowingId(): AccountId
    {
        return AccountId::fromString($this->payload['following_id']);
    }
}