<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower;

use Hewsda64\Touiter\Domain\Account\Account;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laraprooph\ModelEvent\ModelRoot;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Thrust\Security\Contract\Value\Entity;

abstract class FollowerModel extends ModelRoot implements Entity
{
    /**
     * @var string
     */
    protected $table = 'followers';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'following_id', 'follower_id', 'created_at'
    ];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string
     */
    public $incrementing = false;

    public function follower(): HasOne
    {
        return $this->hasOne(Account::class, 'id', 'follower_id');
    }

    public function following(): HasOne
    {
        return $this->hasOne(Account::class, 'id', 'following_id');
    }

    public function followers(): HasMany
    {
        return $this->hasMany(Account::class, 'id', 'follower_id');
    }

    public function followings(): HasMany
    {
        return $this->hasMany(Account::class, 'id', 'following_id');
    }

    public function getId(): UuidInterface
    {
        if( $this->id instanceof Uuid){
            return $this->id;
        }

        return Uuid::fromString($this->id);
    }

    public function getFollowingId(): AccountId
    {
        if( $this->following_id instanceof AccountId){
            return $this->following_id;
        }

        return AccountId::fromString($this->following_id);
    }

    public function getFollowerId(): AccountId
    {
        if( $this->follower_id instanceof AccountId){
            return $this->follower_id;
        }

        return AccountId::fromString($this->follower_id);
    }

    protected function aggregateId(): string
    {
        return $this->id;
    }
}