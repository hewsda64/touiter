<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower\Query;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;

class SuggestFollowingsToAccountCache
{

    /**
     * @var string
     */
    private $accountId;

    /**
     * SuggestFollowingsToAccount constructor.
     *
     * @param string $accountId
     */
    public function __construct($accountId)
    {
        $this->accountId = $accountId;
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->accountId);
    }
}