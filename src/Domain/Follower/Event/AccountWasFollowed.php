<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Laraprooph\ModelEvent\ModelChanged;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class AccountWasFollowed extends ModelChanged
{
    public static function withData(UuidInterface $uid, AccountId $followerId, AccountId $followingId): self
    {
        $self = self::occur($uid->toString(), [
            'following_id' => $followingId->identify(),
            'follower_id' => $followerId->identify()
        ]);

        return $self;
    }

    public function getUid(): UuidInterface
    {
        return Uuid::fromString($this->aggregateId());
    }

    public function getFollowerId(): AccountId
    {
        return AccountId::fromString($this->payload['follower_id']);
    }

    public function getFollowingId(): AccountId
    {
        return AccountId::fromString($this->payload['following_id']);
    }
}