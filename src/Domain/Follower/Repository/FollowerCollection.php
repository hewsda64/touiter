<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower\Repository;

use Hewsda64\Touiter\Domain\Follower\Follower;

interface FollowerCollection
{
    public function save(Follower $root): void;
}