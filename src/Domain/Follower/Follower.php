<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Follower\Event\AccountWasFollowed;
use Ramsey\Uuid\UuidInterface;
use Thrust\Security\Contract\Value\Entity;

class Follower extends FollowerModel
{
    public static function follow(UuidInterface $uid, AccountId $followerId, AccountId $followingId): self
    {
        $self = new self();
        $self->recordThat(AccountWasFollowed::withData($uid, $followerId, $followingId));

        return $self;
    }

    protected function whenAccountWasFollowed(AccountWasFollowed $event): void
    {
        $this->id = $event->getUid();
        $this->follower_id = $event->getFollowerId();
        $this->following_id = $event->getFollowingId();
        $this->created_at = $event->createdAt();
    }

    public function sameIdentityAs(Entity $aEntity): bool
    {
        return $aEntity instanceof $this && $this->getId()->equals($aEntity->getId());
    }
}