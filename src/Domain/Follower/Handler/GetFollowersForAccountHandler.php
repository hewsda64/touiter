<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower\Handler;

use Hewsda64\Touiter\Domain\Follower\Query\GetFollowersForAccount;
use Hewsda64\Touiter\Domain\Follower\Repository\FollowerCollection;
use React\Promise\Deferred;

class GetFollowersForAccountHandler
{

    /**
     * @var FollowerCollection
     */
    private $followerCollection;

    /**
     * GetFollowersForAccountHandler constructor.
     *
     * @param FollowerCollection $followerCollection
     */
    public function __construct(FollowerCollection $followerCollection)
    {
        $this->followerCollection = $followerCollection;
    }

    public function __invoke(GetFollowersForAccount $query, Deferred $deferred = null)
    {
        $followers = $this->followerCollection->followersForAccount($query->accountId()->identify());

        if (!$deferred) {
            return $followers;
        }

        $deferred->resolve($followers);
    }
}