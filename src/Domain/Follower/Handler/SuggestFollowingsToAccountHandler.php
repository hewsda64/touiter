<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower\Handler;

use Hewsda64\Touiter\Domain\Follower\Query\SuggestFollowingsToAccount;
use Hewsda64\Touiter\Domain\Follower\Repository\FollowerCollection;
use React\Promise\Deferred;

class SuggestFollowingsToAccountHandler
{
    /**
     * @var FollowerCollection
     */
    private $followerCollection;

    /**
     * SuggestFollowingsToAccountHandler constructor.
     *
     * @param FollowerCollection $followerCollection
     */
    public function __construct(FollowerCollection $followerCollection)
    {
        $this->followerCollection = $followerCollection;
    }

    public function __invoke(SuggestFollowingsToAccount $query, Deferred $deferred = null)
    {
        $suggest = $this->followerCollection->suggestFiveAccountsToFollow($query->accountId()->identify());

        if (!$deferred) {
            return $suggest;
        }

        $deferred->resolve($suggest);
    }
}