<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower\Handler;

use Hewsda64\Touiter\Domain\Follower\Query\GetFollowingsForAccount;
use Hewsda64\Touiter\Domain\Follower\Repository\FollowerCollection;
use React\Promise\Deferred;

class GetFollowingsForAccountHandler
{
    /**
     * @var FollowerCollection
     */
    private $followerCollection;

    /**
     * GetFollowersForAccountHandler constructor.
     *
     * @param FollowerCollection $followerCollection
     */
    public function __construct(FollowerCollection $followerCollection)
    {
        $this->followerCollection = $followerCollection;
    }

    public function __invoke(GetFollowingsForAccount $query, Deferred $deferred = null)
    {
        $followings = $this->followerCollection->followingsForAccount($query->accountId()->identify());

        if (!$deferred) {
            return $followings;
        }

        $deferred->resolve($followings);
    }
}