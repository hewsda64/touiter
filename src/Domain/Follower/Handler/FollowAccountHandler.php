<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower\Handler;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Follower\Command\FollowAccount;
use Hewsda64\Touiter\Domain\Follower\Repository\FollowerCollection;
use Thrust\Security\Exception\UserNotFound;

class FollowAccountHandler
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var FollowerCollection
     */
    private $followerCollection;

    /**
     * FollowAccountHandler constructor.
     *
     * @param AccountCollection $accountCollection
     * @param FollowerCollection $followerCollection
     */
    public function __construct(AccountCollection $accountCollection, FollowerCollection $followerCollection)
    {
        $this->accountCollection = $accountCollection;
        $this->followerCollection = $followerCollection;
    }

    public function __invoke(FollowAccount $command): void
    {
        if ($command->getFollowingId()->sameValueAs($command->getFollowerId())) {
            return;
        }

        $followerId = $command->getFollowerId()->identify();
        $followingId = $command->getFollowingId()->identify();

        if ($this->followerCollection->isAlreadyFollowed($followingId, $followerId)) {
            return;
        }

        $account = $this->accountCollection->accountOfId($followerId);

        if (!$account) {
            throw new UserNotFound(sprintf('Follower account not found with id %s', $followerId));
        }

        $following = $this->accountCollection->accountOfId($followingId);

        if (!$following) {
            throw new UserNotFound(sprintf('Following account not found with id %s', $followingId));
        }

        $follower = $account->followAccount($command->getUid(), $following->getId());

        $this->followerCollection->save($follower);
    }
}