<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Follower\Handler;

use Hewsda64\Touiter\Domain\Follower\Query\SuggestFollowingsToAccountCache;
use Hewsda64\Touiter\Infrastructure\Repository\Follower\FollowerRepositoryCache;
use React\Promise\Deferred;

class SuggestFollowingsToAccountCacheHandler
{
    /**
     * @var FollowerRepositoryCache
     */
    private $repository;

    /**
     * SuggestFollowingsToAccountCacheHandler constructor.
     *
     * @param FollowerRepositoryCache $repository
     */
    public function __construct(FollowerRepositoryCache $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(SuggestFollowingsToAccountCache $query, Deferred $deferred = null)
    {
        $suggest = $this->repository->suggestFiveAccountsToFollow($query->accountId()->identify());

        if (!$deferred) {
            return $suggest;
        }

        $deferred->resolve($suggest);
    }
}