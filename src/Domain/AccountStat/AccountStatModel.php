<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\AccountStat;

use Hewsda64\Touiter\Domain\Account\Account;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laraprooph\ModelEvent\ModelRoot;

abstract class AccountStatModel extends ModelRoot
{
    /**
     * @var string
     */
    protected $table = 'account_stat';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'touits', 'likes', 'followings', 'followers'
    ];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var bool
     */
    public $incrementing = false;

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, 'id', 'id');
    }

    public function getId(): AccountId
    {
        if ($this->id instanceof AccountId) {
            return $this->id;
        }

        return AccountId::fromString($this->id);
    }

    public function countTouits(): int
    {
        return $this->touits;
    }

    public function countFollowers(): int
    {
        return $this->followers;
    }

    public function countFollowings(): int
    {
        return $this->followings;
    }

    public function countLikes(): int
    {
        return $this->likes;
    }

    protected function incrementCounterForField(string $field): void
    {
        ++$this->{$field};
    }

    protected function aggregateId(): string
    {
        if ($this->id instanceof AccountId) {
            return $this->id->identify();
        }

        return $this->id;
    }
}