<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\AccountStat\Repository;

use Hewsda64\Touiter\Domain\AccountStat\AccountStat;

interface AccountStatList
{
    public function accountOfId(string $accountId): ?AccountStat;

    public function save(AccountStat $root): void;
}