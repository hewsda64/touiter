<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\AccountStat;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\AccountStat\Event\AccountStatWasCreated;
use Hewsda64\Touiter\Domain\AccountStat\Event\AccountFavoritesWasIncremented;
use Hewsda64\Touiter\Domain\AccountStat\Event\TouitCounterWasIncremented;

class AccountStat extends AccountStatModel
{

    public static function initialize(AccountId $accountId): self
    {
        $self = new self();
        $self->recordThat(AccountStatWasCreated::forAccount($accountId));

        return $self;
    }

    public function incrementTouits(): void
    {
        $this->recordThat(TouitCounterWasIncremented::forAccount($this->getId()));
    }

    public function incrementFavorites(): void
    {
        $this->recordThat(AccountFavoritesWasIncremented::for($this->getId()));
    }

    public function whenAccountStatWasCreated(AccountStatWasCreated $event): void
    {
        $this->id = $event->accountId();
        $this->touits = 0;
        $this->likes = 0;
        $this->followers = 0;
        $this->followings = 0;
    }

    protected function whenTouitCounterWasIncremented(TouitCounterWasIncremented $event): void
    {
        $this->incrementCounterForField('touits');
    }

    protected function whenAccountFavoritesWasIncremented(AccountFavoritesWasIncremented $event): void
    {
        $this->incrementCounterForField('likes');
    }
}