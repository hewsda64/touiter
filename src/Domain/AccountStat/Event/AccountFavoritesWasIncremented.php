<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\AccountStat\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Laraprooph\ModelEvent\ModelChanged;

class AccountFavoritesWasIncremented extends ModelChanged
{
    public static function for(AccountId $accountId): self
    {
        return self::occur($accountId->identify());
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->aggregateId());
    }
}