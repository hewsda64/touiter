<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\AccountStat\Handler;

use Hewsda64\Touiter\Domain\Account\Exceptions\AccountAlreadyExistsException;
use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\AccountStat\Command\CreateAccountStat;
use Hewsda64\Touiter\Domain\AccountStat\Repository\AccountStatList;
use Thrust\Security\Exception\UserNotFound;

class CreateAccountStatHandler
{
    /**
     * @var AccountStatList
     */
    private $statList;

    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * CreateAccountStatHandler constructor.
     *
     * @param AccountStatList $statList
     * @param AccountCollection $accountCollection
     */
    public function __construct(AccountStatList $statList, AccountCollection $accountCollection)
    {
        $this->statList = $statList;
        $this->accountCollection = $accountCollection;
    }

    public function __invoke(CreateAccountStat $command)
    {
        $accountId = $command->accountId()->identify();

        if (null === $account = $this->accountCollection->accountOfId($accountId)) {
            throw new UserNotFound(sprintf('Account not found with id %s', $accountId));
        }

        if (null !== $this->statList->accountOfId($accountId)) {
            throw new AccountAlreadyExistsException(
                sprintf('Account stat already exists with %id', $accountId)
            );
        }

        $this->statList->save($account->createStat());
    }
}