<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\AccountStat\Handler;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\AccountStat\Command\IncrementFavoritesAccount;
use Hewsda64\Touiter\Domain\AccountStat\Repository\AccountStatList;

class IncrementFavoritesAccountHandler
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var AccountStatList
     */
    private $statList;

    /**
     * IncrementAccountFavoriteTouitCounterHandler constructor.
     *
     * @param AccountCollection $accountCollection
     * @param AccountStatList $statList
     */
    public function __construct(AccountCollection $accountCollection, AccountStatList $statList)
    {
        $this->accountCollection = $accountCollection;
        $this->statList = $statList;
    }

    public function __invoke(IncrementFavoritesAccount $command): void
    {
        $account = $this->accountCollection->accountOfId($command->accountId()->identify());

        $stat = $this->statList->accountOfId($account->getId()->identify());

        $stat->incrementFavorites();

        $this->statList->save($stat);
    }
}