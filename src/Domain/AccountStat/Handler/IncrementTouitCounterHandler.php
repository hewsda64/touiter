<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\AccountStat\Handler;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\AccountStat\Command\IncrementTouitCounter;
use Hewsda64\Touiter\Domain\AccountStat\Repository\AccountStatList;

class IncrementTouitCounterHandler
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var AccountStatList
     */
    private $statList;

    /**
     * IncrementTouitCounterHandler constructor.
     *
     * @param AccountCollection $accountCollection
     * @param AccountStatList $statList
     */
    public function __construct(AccountCollection $accountCollection, AccountStatList $statList)
    {
        $this->accountCollection = $accountCollection;
        $this->statList = $statList;
    }

    public function __invoke(IncrementTouitCounter $command): void
    {
        $account = $this->accountCollection->accountOfId($command->accountId()->identify());

        $stat = $this->statList->accountOfId($account->getId()->identify());

        $stat->incrementTouits();

        $this->statList->save($stat);
    }
}