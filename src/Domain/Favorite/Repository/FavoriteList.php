<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Favorite\Repository;

use Hewsda64\Touiter\Domain\Favorite\Favorite;

interface FavoriteList
{
    public function save(Favorite $root): void;
}