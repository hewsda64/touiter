<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Favorite\Process;

use Hewsda64\Touiter\Domain\AccountStat\Command\IncrementFavoritesAccount;
use Hewsda64\Touiter\Domain\Favorite\Event\TouitWasFavoriteByAccount;
use Hewsda64\Touiter\Domain\Touit\Command\IncrementFavoritesTouit;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;

class FavoriteTouitProcess
{
    /**
     * @var AsyncCommandBus
     */
    private $bus;

    /**
     * FavoriteTouitProcess constructor.
     *
     * @param AsyncCommandBus $bus
     */
    public function __construct(AsyncCommandBus $bus)
    {
        $this->bus = $bus;
    }

    public function onEvent(TouitWasFavoriteByAccount $event): void
    {
        $accountId = $event->accountId()->identify();
        $touitId = $event->touitId()->toString();

        $this->bus->dispatch(IncrementFavoritesTouit::forTouit($touitId));

        $this->bus->dispatch(IncrementFavoritesAccount::forAccount($accountId));
    }
}