<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Favorite\Handler;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Favorite\Command\AccountFavoriteTouit;
use Hewsda64\Touiter\Domain\Favorite\Repository\FavoriteList;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;

class AccountFavoriteTouitHandler
{
    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * @var FavoriteList
     */
    private $favoriteList;

    /**
     * FavoriteTouitHandler constructor.
     *
     * @param AccountCollection $accountCollection
     * @param TouitCollection $touitCollection
     * @param FavoriteList $favoriteList
     */
    public function __construct(AccountCollection $accountCollection, TouitCollection $touitCollection, FavoriteList $favoriteList)
    {
        $this->accountCollection = $accountCollection;
        $this->touitCollection = $touitCollection;
        $this->favoriteList = $favoriteList;
    }

    public function __invoke(AccountFavoriteTouit $command): void
    {
        $account = $this->accountCollection->accountOfId($command->accountId()->identify());

        $touit = $this->touitCollection->touitOfId($command->touitId()->toString());

        // unique

        $favorite = $account->favoriteTouit($touit->getId());

        $this->favoriteList->save($favorite);
    }
}