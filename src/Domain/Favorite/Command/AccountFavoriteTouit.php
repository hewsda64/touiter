<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Favorite\Command;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\ServiceBus\Async\AsyncMessage;

class AccountFavoriteTouit extends Command implements PayloadConstructable, AsyncMessage
{
    use PayloadTrait;

    // todo touit owner id
    public static function forTouit($touitId, $accountId): self
    {
        return new self([
            'account_id' => $accountId,
            'touit_id' => $touitId
        ]);
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->payload['account_id']);
    }

    public function touitId(): TouitId
    {
        return TouitId::fromString($this->payload['touit_id']);
    }
}