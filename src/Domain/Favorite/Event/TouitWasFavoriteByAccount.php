<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Favorite\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Laraprooph\ModelEvent\ModelChanged;
use Ramsey\Uuid\Uuid;

class TouitWasFavoriteByAccount extends ModelChanged
{
    public static function forTouit(TouitId $touitId, AccountId $accountId): self
    {
        return self::occur(Uuid::uuid4()->toString(), [
            'account_id' => $accountId->identify(),
            'touit_id' => $touitId->toString()
        ]);
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->payload['account_id']);
    }

    public function touitId(): TouitId
    {
        return TouitId::fromString($this->payload['touit_id']);
    }

}