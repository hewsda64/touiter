<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Favorite;

use Hewsda64\Touiter\Domain\Account\Account;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Favorite\Event\TouitWasFavoriteByAccount;
use Hewsda64\Touiter\Domain\Touit\Touit;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laraprooph\ModelEvent\ModelRoot;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Favorite extends ModelRoot
{
    /**
     * @var string
     */
    protected $table = 'like_touits';

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'account_id', 'touit_id', 'created_at'
    ];

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var string
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, 'id', 'account_id');
    }

    public function touit(): BelongsTo
    {
        return $this->belongsTo(Touit::class, 'id', 'touit_id');
    }

    public static function createFavorite(AccountId $accountId, TouitId $touitId): self
    {
        $self = new self();
        $self->recordThat(TouitWasFavoriteByAccount::forTouit($touitId, $accountId));

        return $self;
    }

    // Favorite touit -> through acc -> return favorite -> TouitWasFavorite
    // PM
    //  dispatch update touit favorite counter
    //  dispatch update account stat favorite counter

    protected function whenTouitWasFavoriteByAccount(TouitWasFavoriteByAccount $event): void
    {
        $this->id = $event->uuid();
        $this->account_id = $event->accountId();
        $this->touit_id = $event->touitId();
        $this->created_at = $event->createdAt();
    }

    protected function aggregateId(): string
    {
        return $this->getId()->toString();
    }

    public function getId(): UuidInterface
    {
        if ($this->id instanceof UuidInterface) {
            return $this->id;
        }

        return Uuid::fromString($this->id);
    }
}