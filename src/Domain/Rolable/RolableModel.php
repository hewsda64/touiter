<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Rolable;

use Hewsda64\Touiter\Domain\Account\Account;
use Hewsda64\Touiter\Domain\Role\Role;
use Laraprooph\ModelEvent\ModelRoot;

abstract class RolableModel extends ModelRoot
{
    /**
     * @var string
     */
    protected $table = 'account_role';

    /**
     * @var array
     */
    protected $fillable = ['id', 'account_id', 'role_id'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $keyType = 'string';

    /**
     * @var bool
     */
    public $incrementing = false;

    public function roles(): HasMany
    {
        return $this->hasMany(Role::class, 'id', 'role_id');
    }

    public function account(): BelongsTo
    {
        return $this->belongsTo(Account::class, 'id', 'account_id');
    }

    protected function aggregateId(): string
    {
        return $this->id;
    }
}