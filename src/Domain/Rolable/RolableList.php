<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Rolable;

use Illuminate\Support\Collection;

interface RolableList
{
    public function rolesOfAccount(string $accountId): Collection;

    public function save(Rolable $rolable): void;
}