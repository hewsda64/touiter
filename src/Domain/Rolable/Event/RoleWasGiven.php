<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Rolable\Event;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Role\Role;
use Laraprooph\ModelEvent\ModelChanged;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class RoleWasGiven extends ModelChanged
{
    public static function forAccount(AccountId $accountId, Role $role): self
    {
        return self::occur(Uuid::uuid4()->toString(), [
            'account_id' => $accountId->identify(),
            'role_id' => (string)$role->getId()
        ]);
    }

    public function rolableId(): UuidInterface
    {
        return Uuid::fromString($this->aggregateId());
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->payload['account_id']);
    }

    public function roleId(): UuidInterface
    {
        return Uuid::fromString($this->payload['role_id']);
    }
}