<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Domain\Rolable;

use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Rolable\Event\RoleWasGiven;
use Hewsda64\Touiter\Domain\Role\Role;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Rolable extends RolableModel
{
    public static function giveRole(AccountId $accountId, Role $role): Rolable
    {
        $self = new self();
        $self->recordThat(RoleWasGiven::forAccount($accountId, $role));

        return $self;
    }

    public function getId(): UuidInterface
    {
        return Uuid::fromString($this->id);
    }

    public function accountId(): AccountId
    {
        return AccountId::fromString($this->account_id);
    }

    public function roleId(): UuidInterface
    {
        return Uuid::fromString($this->role_id);
    }

    public function whenRoleWasGiven(RoleWasGiven $event): void
    {
        $this->id = $event->rolableId();
        $this->account_id = $event->accountId();
        $this->role_id = $event->roleId();
    }
}