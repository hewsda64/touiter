<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use Hewsda64\Touiter\Application\Console\ActivateAccountCommand;
use Hewsda64\Touiter\Application\Console\ApplicationFromScratchCommand;
use Hewsda64\Touiter\Application\Console\CreateAccountsCommand;
use Hewsda64\Touiter\Application\Console\CreateActivityCommand;
use Hewsda64\Touiter\Application\Console\CreateRoleCommand;
use Hewsda64\Touiter\Application\Console\GiveRoleToAccountCommand;
use Hewsda64\Touiter\Application\Console\PostTouitCommand;
use Illuminate\Support\ServiceProvider;

class TouiterArtisanCommandServiceProvider extends ServiceProvider
{
    protected $commands = [
        ApplicationFromScratchCommand::class,
        ActivateAccountCommand::class,
        CreateAccountsCommand::class,
        CreateActivityCommand::class,
        CreateRoleCommand::class,
        PostTouitCommand::class,
        GiveRoleToAccountCommand::class,
    ];

    public function boot()
    {
        $this->commands($this->commands);
    }
}