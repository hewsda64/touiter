<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use GrahamCampbell\Markdown\MarkdownServiceProvider;
use Illuminate\Support\ServiceProvider;

class ExternalDependencyServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->register(MarkdownServiceProvider::class);
    }
}