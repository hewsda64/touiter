<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection as AccountCollectionContract;
use Hewsda64\Touiter\Domain\Account\Services\EmailAddressAvailability;
use Hewsda64\Touiter\Domain\Account\Services\PasswordEncoder;
use Hewsda64\Touiter\Domain\AccountStat\Repository\AccountStatList;
use Hewsda64\Touiter\Domain\Activation\Repository\ActivationCollection;
use Hewsda64\Touiter\Domain\Favorite\Repository\FavoriteList;
use Hewsda64\Touiter\Domain\Follower\Repository\FollowerCollection;
use Hewsda64\Touiter\Domain\Rolable\RolableList;
use Hewsda64\Touiter\Domain\Role\Repository\RoleList;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;
use Hewsda64\Touiter\Infrastructure\Repository\Account\AccountCollection;
use Hewsda64\Touiter\Infrastructure\Repository\AccountStat\AccountStatRepository;
use Hewsda64\Touiter\Infrastructure\Repository\Activation\ActivationRepository;
use Hewsda64\Touiter\Infrastructure\Repository\Favorite\FavoriteRepository;
use Hewsda64\Touiter\Infrastructure\Repository\Follower\FollowerRepository;
use Hewsda64\Touiter\Infrastructure\Repository\Rolable\RolableCollection;
use Hewsda64\Touiter\Infrastructure\Repository\Role\RoleCacheCollection;
use Hewsda64\Touiter\Infrastructure\Repository\Touit\TouitRepository;
use Hewsda64\Touiter\Infrastructure\Services\BcryptPasswordEncoder;
use Hewsda64\Touiter\Infrastructure\Services\UniqueEmailAddress;
use Hewsda64\Touiter\Security\Services\AccountChecker;
use Illuminate\Support\ServiceProvider;
use Thrust\Security\Contract\User\UserChecker;
use Thrust\Security\Contract\User\UserProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @var array
     */
    protected $services = [
        UserChecker::class => AccountChecker::class,
        PasswordEncoder::class => BcryptPasswordEncoder::class,
        EmailAddressAvailability::class => UniqueEmailAddress::class
    ];

    /**
     * @var array
     */
    protected $repositories = [
        UserProvider::class => AccountCollection::class,
        RoleList::class => RoleCacheCollection::class,
        RolableList::class => RolableCollection::class,
        ActivationCollection::class => ActivationRepository::class,
        TouitCollection::class => TouitRepository::class,
        FollowerCollection::class => FollowerRepository::class,
        AccountStatList::class => AccountStatRepository::class,
        FavoriteList::class => FavoriteRepository::class
    ];

    public function boot(): void
    {
        foreach ($this->services as $abstract => $concrete) {
            $this->app->bindIf($abstract, $concrete);
        }
    }

    public function register(): void
    {
        $this->registerRepositories();

        $this->registerAlias();
    }

    protected function registerRepositories(): void
    {
        foreach ($this->repositories as $abstract => $concrete) {
            $this->app->bindIf($abstract, $concrete);
        }
    }

    protected function registerAlias(): void
    {
        $this->app->alias(UserProvider::class, 'eloquent_account');
        $this->app->alias(UserProvider::class, AccountCollectionContract::class);
    }

    public function provides(): array
    {
        return array_merge(
            array_keys($this->repositories),
            [AccountCollectionContract::class, 'eloquent_account']
        );
    }
}