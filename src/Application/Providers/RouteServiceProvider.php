<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as BaseServiceProvider;

class RouteServiceProvider extends BaseServiceProvider
{
    /**
     * @var string
     */
    protected $namespace = 'Hewsda64\Touiter\Application\Http\Controllers';

    public function boot()
    {
        parent::boot();
    }

    public function map(): void
    {
        $this->mapFrontendRoutes();
    }

    protected function mapFrontendRoutes(): void
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('/routes/web.php'));
    }
}