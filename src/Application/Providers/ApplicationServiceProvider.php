<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use Hewsda64\Touiter\Application\Http\Response\JsonResponse;
use Hewsda64\Touiter\Application\Http\Response\ResponseStrategy;
use Hewsda64\Touiter\ServiceBus\Metadata\MetadataGatherer;
use Hewsda64\Touiter\ServiceBus\Metadata\NoOpMetadataGatherer;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Prooph\Common\Messaging\FQCNMessageFactory;
use Prooph\Common\Messaging\MessageConverter;
use Prooph\Common\Messaging\MessageFactory;
use Prooph\Common\Messaging\NoOpMessageConverter;
use Thrust\Firewall\Foundation\Http\Middleware\SessionContextAware;

class ApplicationServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    public function boot(Router $router): void
    {
        $router->prependMiddlewareToGroup('web', SessionContextAware::class);

        $this->loadRoutes();
        $this->loadViews();
    }

    protected function loadRoutes(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../../../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../../../routes/authentication.php');
        $this->loadRoutesFrom(__DIR__ . '/../../../routes/api.php');
    }

    protected function loadViews(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../../../resources/views', 'touiter');
    }

    public function register(): void
    {
        $this->registerApiDependencyMiddleware();
    }

    protected function registerApiDependencyMiddleware(): void
    {
        $this->app->bind(MessageFactory::class, FQCNMessageFactory::class);
        $this->app->bind(ResponseStrategy::class, JsonResponse::class);
        $this->app->bind(MessageConverter::class, NoOpMessageConverter::class);
        $this->app->bind(MetadataGatherer::class, NoOpMetadataGatherer::class);
    }
}