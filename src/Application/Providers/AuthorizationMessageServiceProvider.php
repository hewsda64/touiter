<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use Hewsda64\Touiter\Security\Authorization\AuthorizationMessageService;
use Illuminate\Support\ServiceProvider;
use Prooph\ServiceBus\Plugin\Guard\AuthorizationService;

class AuthorizationMessageServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->bind(AuthorizationService::class, AuthorizationMessageService::class);
    }
}