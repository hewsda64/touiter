<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use Illuminate\Support\AggregateServiceProvider;
use Laraprooph\ServiceBus\Provider\ServiceBusServiceProvider;
use Thrust\Firewall\Foundation\Providers\ThrustAggregate;

class TouiterServiceProvider extends AggregateServiceProvider
{
    /**
     * @var array
     */
    protected $providers = [
        ExternalDependencyServiceProvider::class,
        ApplicationServiceProvider::class,
        //RouteServiceProvider::class,
        TouiterComposerServiceProvider::class,
        TouiterArtisanCommandServiceProvider::class,
        RepositoryServiceProvider::class,
        AuthorizationMessageServiceProvider::class,
        AuthenticationServiceProvider::class,
        ThrustAggregate::class,
        VoterServiceProvider::class,
        ServiceBusServiceProvider::class,
        TouiterBusServiceProvider::class
    ];
}