<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use Hewsda64\Touiter\Security\Authentication\UsernamePassword\UsernamePasswordAuthenticationFactory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Thrust\Firewall\Factory\Manager\AuthenticationManager;

class AuthenticationServiceProvider extends ServiceProvider
{
    public function boot(AuthenticationManager $manager)
    {
        $manager->extend('frontend', 'account-login', function (Application $app) {
            return $app->make(UsernamePasswordAuthenticationFactory::class);
        });
    }
}