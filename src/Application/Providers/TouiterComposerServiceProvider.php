<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use Hewsda64\Touiter\Application\Composer\mod\AccountCardModuleComposer;
use Hewsda64\Touiter\Application\Composer\mod\SuggestToFollowModuleComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class TouiterComposerServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $composers = [
        'touiter::layout.mod.__account_to_follow_mod' => SuggestToFollowModuleComposer::class,
        'touiter::layout.partial.__user_profile_card' => AccountCardModuleComposer::class,
        'touiter::layout.partial.__top_profile' => AccountCardModuleComposer::class,
    ];

    public function boot(): void
    {
        foreach ($this->composers as $view => $composer) {
            View::composer($view, $composer);
        }
    }
}