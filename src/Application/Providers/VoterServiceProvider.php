<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use Hewsda64\Touiter\Security\Authorization\Voter\MessageVoter;
use Thrust\Firewall\Foundation\Providers\BaseProvider\BaseVoterServiceProvider;
use Thrust\Security\Authorization\Voter\AnonymousVoter;
use Thrust\Security\Authorization\Voter\AuthenticatedTokenVoter;
use Thrust\Security\Authorization\Voter\RoleHierarchyVoter;

class VoterServiceProvider extends BaseVoterServiceProvider
{
    /**
     * @var array
     */
    protected $voters = [
        AuthenticatedTokenVoter::class,
        AnonymousVoter::class,
        RoleHierarchyVoter::class,
        MessageVoter::class
    ];
}