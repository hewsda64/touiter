<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Providers;

use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;
use Hewsda64\Touiter\ServiceBus\Bus\QueryCacheBus;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Laraprooph\ServiceBus\CommandBusManager;
use Laraprooph\ServiceBus\EventBusManager;
use Laraprooph\ServiceBus\QueryBusManager;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\QueryBus;

class TouiterBusServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $bus = [
        'command' => [
            ['command_bus', CommandBus::class],
            ['async_command_bus', AsyncCommandBus::class],
        ],
        'event' => [
            ['event_bus', EventBus::class]
        ],
        'query' => [
            ['query_bus', QueryBus::class],
            ['query_cache_bus', QueryCacheBus::class]
        ]
    ];

    public function boot(): void
    {
        $this->registerBus();
    }

    protected function registerBus(): void
    {
        foreach ($this->bus as $bus => $message) {
            foreach ($message as [$alias, $concrete]) {

                switch ($bus) {
                    case 'command':
                        $this->app->singleton($alias, function (Application $app) use ($alias) {
                            return $app[CommandBusManager::class]->command($alias);
                        });
                        break;

                    case 'event':
                        $this->app->singleton($alias, function (Application $app) use ($alias) {
                            return $app[EventBusManager::class]->event($alias);
                        });
                        break;

                    case 'query':
                        $this->app->singleton($alias, function (Application $app) use ($alias) {
                            return $app[QueryBusManager::class]->query($alias);
                        });
                        break;
                }

                $this->app->alias($alias, $concrete);
            }
        }
    }
}