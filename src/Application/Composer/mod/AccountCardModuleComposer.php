<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Composer\mod;

use Hewsda64\Touiter\Domain\AccountStat\Repository\AccountStatList;
use Illuminate\View\View;
use Thrust\Firewall\Foundation\Support\Secure;

class AccountCardModuleComposer
{
    use Secure;

    /**
     * @var AccountStatList
     */
    private $statList;

    /**
     * AccountCardModuleComposer constructor.
     *
     * @param AccountStatList $statList
     */
    public function __construct(AccountStatList $statList)
    {
        $this->statList = $statList;
    }

    public function compose(View $view)
    {
        $this->requireGranted('ROLE_USER');

        $stat = $this->statList->accountOfId($this->identifyUser());

        $view
            ->with('accountCardTouitCount', $stat->countTouits())
            ->with('accountCardFollowerCount', $stat->countFollowers())
            ->with('accountCardFollowingCount', $stat->countFollowings())
            ->with('accountCardLikeCount', $stat->countLikes());
    }
}