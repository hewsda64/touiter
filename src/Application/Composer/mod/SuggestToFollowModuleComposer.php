<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Composer\mod;

use Hewsda64\Touiter\Domain\Follower\Query\SuggestFollowingsToAccount;
use Hewsda64\Touiter\ServiceBus\Bus\QueryCacheBus;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Prooph\ServiceBus\QueryBus;
use React\Promise\PromiseInterface;
use Thrust\Firewall\Foundation\Support\Secure;

class SuggestToFollowModuleComposer
{
    use Secure;

    /**
     * @var QueryCacheBus
     */
    private $queryCacheBus;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * SuggestToFollowModuleComposer constructor.
     *
     * @param QueryCacheBus $queryCacheBus
     * @param QueryBus $queryBus
     */
    public function __construct(QueryCacheBus $queryCacheBus, QueryBus $queryBus)
    {
        $this->queryCacheBus = $queryCacheBus;
        $this->queryBus = $queryBus;
    }

    public function compose(View $view): void
    {
        $suggestToFollow = $this->fromPromise(
            $this->queryBus->dispatch(
                new SuggestFollowingsToAccount($this->identifyUser())
            )
        );

        /*
        $suggestToFollow = $this->fromPromise(
            $this->queryCacheBus->dispatch(
                new SuggestFollowingsToAccountCache($this->identifyUser()))
        );*/

        $view->with(compact('suggestToFollow'));
    }

    private function fromPromise(PromiseInterface $promise): Collection
    {
        $result = new Collection();

        $promise->then(function ($data) use (&$result) {
            $result = $data;
        });

        return $result;
    }
}