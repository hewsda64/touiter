<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Controllers\Api;

use Hewsda64\Touiter\Application\Http\Response\ResponseStrategy;
use Hewsda64\Touiter\Domain\Touit\Query\GetFollowingTimelineOfAccount;
use Laraprooph\ServiceBus\HasBus;
use Symfony\Component\HttpFoundation\Response;

class FollowingTimelineOfProfileAction
{
    use HasBus;

    /**
     * @var ResponseStrategy
     */
    private $responseStrategy;

    /**
     * TimelineOfProfileAction constructor.
     *
     * @param ResponseStrategy $responseStrategy
     */
    public function __construct(ResponseStrategy $responseStrategy)
    {
        $this->responseStrategy = $responseStrategy;
    }

    public function __invoke(string $userName): Response
    {
        // time span
        // limit count
        // rel: reply
        return $this->responseStrategy->fromPromise(
            $this->dispatchQuery(new GetFollowingTimelineOfAccount($userName))
        );
    }
}