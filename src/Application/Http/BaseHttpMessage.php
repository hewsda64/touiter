<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http;

use Hewsda64\Touiter\Security\Authorization\AuthorizationMessage;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use React\Promise\PromiseInterface;
use Thrust\Security\Contract\Exception\SecurityException;

abstract class BaseHttpMessage
{
    use AuthorizationMessage;

    abstract protected function preAuthorize(): void;

    protected function handleExceptionMessage(CommandDispatchException $exception): string
    {
        $previousException = $exception->getPrevious();

        if (!$previousException) {
            return 'Something goes wrong ..!';
        }

        if ($previousException instanceof SecurityException) {
            throw $previousException;
        }

        return $exception->getPrevious()->getMessage();
    }

    protected function simpleQueryDispatcher($query, string $bus = null)
    {
        if (!is_object($query)) {
            throw new \RuntimeException('Dispatch from simple query must be an object.');
        }

        $this->preAuthorize();

        return $this->fromPromise($this->dispatchQuery($query, $bus));
    }

    protected function fromPromise(PromiseInterface $promise)
    {
        $result = null;

        $promise->then(function ($data) use (&$result) {
            $result = $data;
        });

        return $result;
    }
}