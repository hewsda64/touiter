<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Actions;

use Hewsda64\Touiter\Application\Http\BaseHttpMessage;
use Prooph\Common\Messaging\Message;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseAction extends BaseHttpMessage
{

    /**
     * Dispatch any message type through the authorization service
     *
     * @param Message $message
     * @param string|null $bus
     *
     * @return Response
     */
    protected function messageDispatcher(Message $message, string $bus = null): Response
    {
        $this->preAuthorize();

        try {
            if (Message::TYPE_QUERY === $message->messageType()) {
                return $this->fromPromise(
                    $this->dispatchAuthorizedMessage($message, $bus)
                );
            }

            $this->dispatchAuthorizedMessage($message, $bus);

            return $this->buildSuccessResponse();

        } catch (CommandDispatchException $exception) {
            return $this->buildFailureResponse($exception);
        }
    }

    protected function buildSuccessResponse(array $parameters = []): Response
    {
        return redirect(route($this->onRouteSuccess(), $parameters))
            ->with('message', $this->onSuccessMessage());
    }

    protected function buildFailureResponse(CommandDispatchException $exception, array $parameters = []): Response
    {
        return redirect(route($this->onRouteFailure(), $parameters))
            ->with('message', $this->handleExceptionMessage($exception));
    }

    abstract protected function onRouteSuccess(): string;

    abstract protected function onRouteFailure(): string;

    abstract protected function onSuccessMessage(): string;
}