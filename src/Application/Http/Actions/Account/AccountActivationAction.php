<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Actions\Account;

use Hewsda64\Touiter\Application\Http\Actions\BaseAction;
use Hewsda64\Touiter\Domain\Account\Command\ActivateAccount;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountActivationAction extends BaseAction
{
    public function __invoke(Request $request, string $activationToken): Response
    {
        $command = ActivateAccount::withActivationToken($activationToken);

        return $this->messageDispatcher($command);
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted( 'ROLE_USER');
    }

    protected function onRouteSuccess(): string
    {
        return 'frontend.login';
    }

    protected function onRouteFailure(): string
    {
        return $this->onRouteSuccess();
    }

    protected function onSuccessMessage(): string
    {
        return 'Account activation successful.';
    }
}