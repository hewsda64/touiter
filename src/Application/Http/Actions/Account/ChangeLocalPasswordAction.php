<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Actions\Account;

use Hewsda64\Touiter\Application\Http\Actions\BaseAction;
use Hewsda64\Touiter\Domain\Account\Command\ChangeLocalPassword;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ChangeLocalPasswordAction extends BaseAction
{

    public function __invoke(Request $request): Response
    {
        $command = ChangeLocalPassword::forAccount(
            $this->identifyUser(),
            $request->input('current_password'),
            $request->input('new_password'),
            $request->input('new_password_confirmation')
        );

        return $this->messageDispatcher($command);
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted( 'ROLE_USER');
    }

    protected function onRouteSuccess(): string
    {
        return 'frontend.auth.profile.password';
    }

    protected function onRouteFailure(): string
    {
        return $this->onRouteSuccess();
    }

    protected function onSuccessMessage(): string
    {
        return 'Account password successfully updated.';
    }
}