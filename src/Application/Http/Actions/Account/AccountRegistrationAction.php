<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Actions\Account;

use Hewsda64\Touiter\Application\Http\Actions\BaseAction;
use Hewsda64\Touiter\Domain\Account\Command\RegisterAccount;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountRegistrationAction extends BaseAction
{
    public function __invoke(Request $request): Response
    {
        $command = RegisterAccount::withData(
            AccountId::nextIdentity()->identify(),
            $request->input('name'),
            $request->input('identifier'),
            $request->input('password'),
            $request->input('password_confirmation')
        );

        return $this->messageDispatcher($command);
    }

    protected function preAuthorize(): void
    {
            $this->requireGranted( 'anonymous');
    }

    protected function onRouteSuccess(): string
    {
        return 'frontend.login';
    }

    protected function onRouteFailure(): string
    {
        return 'frontend.register';
    }

    protected function onSuccessMessage(): string
    {
        return 'Registration successful.';
    }
}