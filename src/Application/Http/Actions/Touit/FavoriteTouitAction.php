<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Actions\Touit;

use Hewsda64\Touiter\Application\Http\Actions\BaseAction;
use Hewsda64\Touiter\Domain\Favorite\Command\AccountFavoriteTouit;
use Symfony\Component\HttpFoundation\Response;

class FavoriteTouitAction extends BaseAction
{

    public function __invoke(string $touitId): Response
    {
        $command = AccountFavoriteTouit::forTouit($touitId, $this->identifyUser());

        return $this->messageDispatcher($command, 'async_command_bus');
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted( 'ROLE_USER');
    }

    protected function onRouteSuccess(): string
    {
        return 'frontend.home';
    }

    protected function onRouteFailure(): string
    {
        return $this->onRouteSuccess();
    }

    protected function onSuccessMessage(): string
    {
        return 'Touit was favorite.';
    }
}