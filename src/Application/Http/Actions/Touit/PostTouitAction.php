<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Actions\Touit;

use Hewsda64\Touiter\Application\Http\Actions\BaseAction;
use Hewsda64\Touiter\Domain\Touit\Command\PostTouit;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PostTouitAction extends BaseAction
{

    public function __invoke(Request $request): Response
    {
        $command = PostTouit::withData(
            TouitId::nextIdentity()->toString(),
            $this->identifyUser(),
            $request->input('body')
        );

        return $this->messageDispatcher($command, 'async_command_bus');
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted( 'ROLE_USER');
    }

    protected function onRouteSuccess(): string
    {
        return 'frontend.home';
    }

    protected function onSuccessMessage(): string
    {
        return 'touit posted.';
    }

    protected function onRouteFailure(): string
    {
        return 'frontend.home';
    }
}