<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Actions\Follow;

use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;
use Hewsda64\Touiter\Application\Http\Actions\BaseAction;
use Hewsda64\Touiter\Domain\Follower\Command\FollowAccount;

class FollowAccountAction extends BaseAction
{
    public function __invoke(string $followingId): Response
    {
        $command = FollowAccount::forAccount(
            Uuid::uuid4()->toString(),
            $this->identifyUser(),
            $followingId
        );

        return $this->messageDispatcher($command, 'async_command_bus');
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted( 'ROLE_USER');
    }

    protected function onRouteSuccess(): string
    {
        return 'frontend.home';
    }

    protected function onRouteFailure(): string
    {
        return $this->onRouteSuccess();
    }

    protected function onSuccessMessage(): string
    {
        return 'Account successfully followed.';
    }
}