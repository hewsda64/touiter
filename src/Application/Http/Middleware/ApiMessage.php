<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Middleware;

use Assert\InvalidArgumentException;
use Hewsda64\Touiter\Application\Http\Response\ResponseStrategy;
use Illuminate\Http\Request;
use Laraprooph\ServiceBus\HasBus;
use Prooph\Common\Messaging\Message;
use Prooph\Common\Messaging\MessageDataAssertion;
use Prooph\Common\Messaging\MessageFactory;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class ApiMessage
{
    use HasBus;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var ResponseStrategy
     */
    private $responseStrategy;

    /**
     * ApiMessage constructor.
     *
     * @param MessageFactory $messageFactory
     * @param ResponseStrategy $responseStrategy
     */
    public function __construct(MessageFactory $messageFactory, ResponseStrategy $responseStrategy)
    {
        $this->messageFactory = $messageFactory;
        $this->responseStrategy = $responseStrategy;
    }

    public function handle(Request $request): Response
    {
        $payload = null;
        $messageName = 'UNKNOWN';

        try {

            $payload = $request->json();

            if (is_array($payload) && isset($payload['message_name'])) {
                $messageName = $payload['message_name'];

                // CheckMe
                if (!isset($payload['payload'])) {
                    $payload['payload'] = $payload;
                }
            }

            $messageName = $request->get('message_name', $messageName);
            $payload['message_name'] = $messageName;

            if (!isset($payload['uuid'])) {
                $payload['uuid'] = Uuid::uuid4();
            }

            if (!isset($payload['created_at'])) {
                $payload['created_at'] = new \DateTimeImmutable('now', new \DateTimeZone('UTC'));
            }

            if (!isset($payload['metadata'])) {
                $payload['metadata'] = [];
            }

            MessageDataAssertion::assert($payload);

            $message = $this->messageFactory->createMessageFromArray($messageName, $payload);

            switch ($message->messageType()) {
                case Message::TYPE_COMMAND:
                    $this->dispatchCommand($message, 'async_command_bus'); // fixMe

                    return $this->responseStrategy->withStatus(Response::HTTP_ACCEPTED);
                case Message::TYPE_QUERY:
                    return $this->responseStrategy->fromPromise(
                        $this->dispatchQuery($message)
                    );
                default:
                    throw new \RuntimeException(
                        sprintf(
                            'Invalid message type "%s" for message "%s".',
                            $message->messageType(),
                            $messageName
                        ),
                        Response::HTTP_BAD_REQUEST
                    );
            }
        } catch (InvalidArgumentException $e) {
            throw new \RuntimeException(
                $e->getMessage(),
                Response::HTTP_BAD_REQUEST,
                $e
            );
        } catch (\Throwable $e) {
            throw new \RuntimeException(
                sprintf('An error occurred during dispatching of message "%s"', $messageName),
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $e
            );
        }
    }
}