<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Middleware;

use Hewsda64\Touiter\Application\Http\Response\ResponseStrategy;
use Hewsda64\Touiter\ServiceBus\Metadata\MetadataGatherer;
use Illuminate\Http\Request;
use Laraprooph\ServiceBus\HasBus;
use Prooph\Common\Messaging\MessageFactory;
use Symfony\Component\HttpFoundation\Response;

class ApiCommand
{
    use HasBus;

    const NAME_ATTRIBUTE = 'touiter_command_api';

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var ResponseStrategy
     */
    private $responseStrategy;

    /**
     * @var MetadataGatherer
     */
    private $metadataGatherer;

    /**
     * ApiCommand constructor.
     *
     * @param MessageFactory $messageFactory
     * @param ResponseStrategy $responseStrategy
     * @param MetadataGatherer $metadataGatherer
     */
    public function __construct(MessageFactory $messageFactory,
                                ResponseStrategy $responseStrategy,
                                MetadataGatherer $metadataGatherer)
    {
        $this->messageFactory = $messageFactory;
        $this->responseStrategy = $responseStrategy;
        $this->metadataGatherer = $metadataGatherer;
    }

    public function handle(Request $request): Response
    {
        $commandName = $request->get(self::NAME_ATTRIBUTE);

        if (!$commandName) {
            throw new \RuntimeException(
                sprintf('Command name attribute ("%s") was not found in request.', self::NAME_ATTRIBUTE),
                Response::HTTP_BAD_REQUEST
            );
        }

        try{
            $command = $this->messageFactory->createMessageFromArray(
                $commandName,[
                    'payload' => $request->json(),
                    'metadata' => $this->metadataGatherer->getFromRequest($request)
                ]
            );

            $this->dispatchCommand($command,'async_command_bus');

            return $this->responseStrategy->withStatus(Response::HTTP_ACCEPTED);
        }catch (\Throwable $exception){
            throw new \RuntimeException(
                sprintf('An error occurred during dispatching of command "%s"', $commandName),
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $exception
            );
        }
    }
}