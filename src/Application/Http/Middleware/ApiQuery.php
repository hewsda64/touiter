<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Middleware;

use Hewsda64\Touiter\Application\Http\Response\ResponseStrategy;
use Hewsda64\Touiter\ServiceBus\Metadata\MetadataGatherer;
use Illuminate\Http\Request;
use Laraprooph\ServiceBus\HasBus;
use Prooph\Common\Messaging\MessageFactory;
use Symfony\Component\HttpFoundation\Response;

class ApiQuery
{
    use HasBus;

    const NAME_ATTRIBUTE = 'touiter_query_api';

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    /**
     * @var ResponseStrategy
     */
    private $responseStrategy;

    /**
     * @var MetadataGatherer
     */
    private $metadataGatherer;

    /**
     * ApiQuery constructor.
     *
     * @param MessageFactory $messageFactory
     * @param ResponseStrategy $responseStrategy
     * @param MetadataGatherer $metadataGatherer
     */
    public function __construct(MessageFactory $messageFactory,
                                ResponseStrategy $responseStrategy,
                                MetadataGatherer $metadataGatherer)
    {
        $this->messageFactory = $messageFactory;
        $this->responseStrategy = $responseStrategy;
        $this->metadataGatherer = $metadataGatherer;
    }

    public function handle(Request $request): Response
    {
        $queryName = $request->get(self::NAME_ATTRIBUTE);

        if (null === $queryName) {
            throw new \RuntimeException(
                sprintf('Query name attribute ("%s") was not found in request.', self::NAME_ATTRIBUTE),
                Response::HTTP_BAD_REQUEST
            );
        }

        $payload = $request->query();

        if ($request->isMethod('post')) {
            $payload['data'] = $request->json();
        }

        try {
            $query = $this->messageFactory->createMessageFromArray($queryName, [
                'payload' => $payload,
                'metadata' => $this->metadataGatherer->getFromRequest($request),
            ]);

            return $this->responseStrategy->fromPromise($this->dispatchQuery($query));
        } catch (\Throwable $e) {
            throw new \RuntimeException(
                sprintf('An error occurred during dispatching of query "%s"', $queryName),
                Response::HTTP_INTERNAL_SERVER_ERROR,
                $e
            );
        }
    }
}