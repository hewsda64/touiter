<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Middleware;

use Illuminate\Http\Request;

class ApiQueryAware
{
    public function handle(Request $request, \Closure $next)
    {
        if ($attribute = $this->findQueryAttribute($request->route()->getName())) {
            $request->request->add([ApiQuery::NAME_ATTRIBUTE => $attribute]);
        }

        return $next($request);
    }

    private function findQueryAttribute(string $routeName = null): ?string
    {
        switch ($routeName) {

            case !$routeName:
                return null;
        }

        return null;
    }
}