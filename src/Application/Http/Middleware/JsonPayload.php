<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class JsonPayload
{
    public function handle(Request $request, \Closure $next)
    {
        if ($request->expectsJson()) {

            $payload = json_decode((string)$request->getContent(), true);

            $this->requireNoJsonError();

            $request->setJson($payload);
        }

        return $next($request);
    }

    private function requireNoJsonError(): void
    {
        $statusCode = Response::HTTP_BAD_REQUEST;

        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                throw new \RuntimeException('Invalid JSON, maximum stack depth exceeded.', $statusCode);
            case JSON_ERROR_UTF8:
                throw new \RuntimeException('Malformed UTF-8 characters, possibly incorrectly encoded.', $statusCode);
            case JSON_ERROR_SYNTAX:
            case JSON_ERROR_CTRL_CHAR:
            case JSON_ERROR_STATE_MISMATCH:
                throw new \RuntimeException('Invalid JSON.', $statusCode);
        }
    }
}