<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Middleware;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class JsonError
{
    public function handle(Request $request, \Closure $next)
    {
        try {
            return $next($request);
        } catch (\Throwable $exception) {
            if ($request->expectsJson()) {
                $data = [
                    'message' => $exception->getMessage(),
                    'trace' => $exception->getTraceAsString()
                ];

                // fix dev env
                return new JsonResponse($data, Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            throw $exception;
        }
    }
}