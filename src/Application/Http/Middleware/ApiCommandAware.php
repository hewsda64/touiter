<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Middleware;

use Hewsda64\Touiter\Domain\Touit\Command\PostTouit;
use Illuminate\Http\Request;

class ApiCommandAware
{
    public function handle(Request $request, \Closure $next)
    {
        if ($attribute = $this->findCommandAttribute($request->route()->getName())) {
            $request->request->add([ApiCommand::NAME_ATTRIBUTE => $attribute]);
        }

        return $next($request);
    }

    private function findCommandAttribute(string $routeName = null): ?string
    {
        switch ($routeName) {

            case !$routeName:
                return null;

            case 'command::send-touit':
                return PostTouit::class;
        }

        return null;
    }
}