<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Page;

use Hewsda64\Touiter\Application\Http\BaseHttpMessage;
use Illuminate\Contracts\View\View;

class LocalPasswordSettingsPage extends BaseHttpMessage
{

    public function __invoke(): View
    {
        $this->preAuthorize();

        return view('touiter::auth.profile.change_local_password');
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted('ROLE_USER');
    }
}