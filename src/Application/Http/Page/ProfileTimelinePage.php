<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Page;

use Hewsda64\Touiter\Application\Http\BaseHttpMessage;
use Hewsda64\Touiter\Domain\Touit\Query\GetFollowingTimelineOfAccount;
use Illuminate\Contracts\View\View;

class ProfileTimelinePage extends BaseHttpMessage
{

    // home page should display the profile timeline with username in uri

    public function __invoke(): View
    {
        $timeline = $this->simpleQueryDispatcher(
            new GetFollowingTimelineOfAccount($this->identifyUser())
        );

        return view('touiter::home')->with('profileTimeline', $timeline);
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted( 'ROLE_USER');
    }
}