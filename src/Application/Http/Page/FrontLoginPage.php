<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Page;

use Hewsda64\Touiter\Application\Http\BaseHttpMessage;
use Illuminate\Contracts\View\View;

class FrontLoginPage extends BaseHttpMessage
{

    public function __invoke(): View
    {
        $this->preAuthorize();

        return view('touiter::auth.login');
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted( 'anonymous');
    }
}