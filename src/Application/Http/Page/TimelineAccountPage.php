<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Page;

use Hewsda64\Touiter\Application\Http\BaseHttpMessage;
use Hewsda64\Touiter\Domain\Touit\Query\GetTimelineOfAccount;
use Illuminate\Contracts\View\View;

class TimelineAccountPage extends BaseHttpMessage
{

    public function __invoke(string $userName): View
    {
        $timeline = $this->simpleQueryDispatcher(new GetTimelineOfAccount($userName));

        return view('touiter::home')->with('profileTimeline', $timeline);
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted( 'ROLE_USER');
    }
}