<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Page;

use Hewsda64\Touiter\Application\Http\BaseHttpMessage;
use Hewsda64\Touiter\Domain\Follower\Query\GetFollowersForAccount;
use Illuminate\Contracts\View\View;

class AccountFollowersPage extends BaseHttpMessage
{

    public function __invoke(string $userName): View
    {
        $followAccounts = $this->simpleQueryDispatcher(
            new GetFollowersForAccount($this->identifyUser())
        );

        return view('touiter::follow.follow')->with(compact('followAccounts'));
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted( 'ROLE_USER');
    }
}