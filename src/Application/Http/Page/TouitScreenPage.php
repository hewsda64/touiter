<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Http\Page;

use Hewsda64\Touiter\Application\Http\BaseHttpMessage;
use Hewsda64\Touiter\Domain\Touit\Query\GetTimelineOfTouit;
use Illuminate\Contracts\View\View;

class TouitScreenPage extends BaseHttpMessage
{

    public function __invoke(string $touitId): View
    {
        $touitScreenTimeline = $this->simpleQueryDispatcher(
            new GetTimelineOfTouit($touitId)
        );

        return view('touiter::touit.touit_timeline')->with(compact('touitScreenTimeline'));
    }

    protected function preAuthorize(): void
    {
        $this->requireGranted('ROLE_USER');
    }
}