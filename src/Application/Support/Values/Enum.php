<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Support\Values;

use MabeEnum\Enum as MabeEnum;
use MabeEnum\EnumSerializableTrait;
use Serializable;
use Thrust\Security\Contract\Value\SecurityValue;

 abstract class Enum extends MabeEnum implements Serializable, SecurityValue
{
    use EnumSerializableTrait;

    public function toString(): string
    {
        return $this->getName();
    }

     public function sameValueAs(SecurityValue $aValue): bool
     {
         return $this->is($aValue);
     }
 }