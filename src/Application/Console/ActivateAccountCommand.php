<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Console;

use Hewsda64\Touiter\Domain\Account\Command\ActivateAccount;
use Hewsda64\Touiter\Domain\Activation\Activation;
use Hewsda64\Touiter\Domain\Activation\Repository\ActivationCollection;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;
use Illuminate\Console\Command;

class ActivateAccountCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'touiter:activate-account';

    /**
     * @var string
     */
    protected $description = 'Activate x random accounts';

    /**
     * @var AsyncCommandBus
     */
    private $asyncCommandBus;

    /**
     * @var ActivationCollection
     */
    private $activationCollection;

    /**
     * ActivateAccountCommand constructor.
     *
     * @param AsyncCommandBus $asyncCommandBus
     * @param ActivationCollection $activationCollection
     */
    public function __construct(AsyncCommandBus $asyncCommandBus, ActivationCollection $activationCollection)
    {
        parent::__construct();

        $this->asyncCommandBus = $asyncCommandBus;
        $this->activationCollection = $activationCollection;
    }

    public function handle(): void
    {
        $notActivated = $this->activationCollection->createModel()->get();

        $num = 0;
        $notActivated->each(function (Activation $activation) use (&$num) {

            if (random_int(0, 100) < 70) {
                $this->asyncCommandBus->dispatch(
                    ActivateAccount::withActivationToken($activation->getActivationToken()->token()));
                ++$num;
            }
        });

        $this->info($num . ' accounts has been activated.');
    }
}