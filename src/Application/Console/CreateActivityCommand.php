<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Console;

use Faker\Factory;
use Hewsda64\Touiter\Domain\Account\Account;
use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Favorite\Command\AccountFavoriteTouit;
use Hewsda64\Touiter\Domain\Favorite\Repository\FavoriteList;
use Hewsda64\Touiter\Domain\Follower\Command\FollowAccount;
use Hewsda64\Touiter\Domain\Follower\Repository\FollowerCollection;
use Hewsda64\Touiter\Domain\Touit\Command\PostTouit;
use Hewsda64\Touiter\Domain\Touit\Command\ReplyToTouit;
use Hewsda64\Touiter\Domain\Touit\Repository\TouitCollection;
use Hewsda64\Touiter\Domain\Touit\Touit;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;
use Illuminate\Console\Command;
use Ramsey\Uuid\Uuid;

class CreateActivityCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'touiter:create-activity';

    /**
     * @var string
     */
    protected $description = 'Create random activity like touiting, replying, liking, retouiting, following';


    /**
     * @var Factory
     */
    private $faker;

    /**
     * @var AsyncCommandBus
     */
    private $bus;

    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var TouitCollection
     */
    private $touitCollection;

    /**
     * @var FavoriteList
     */
    private $favoriteList;

    /**
     * @var FollowerCollection
     */
    private $followerCollection;

    public function __construct(AsyncCommandBus $bus,
                                Factory $faker,
                                AccountCollection $accountCollection,
                                TouitCollection $touitCollection,
                                FavoriteList $favoriteList,
                                FollowerCollection $followerCollection)
    {
        parent::__construct();

        $this->bus = $bus;
        $this->faker = $faker::create();
        $this->accountCollection = $accountCollection;
        $this->touitCollection = $touitCollection;
        $this->favoriteList = $favoriteList;
        $this->followerCollection = $followerCollection;
    }

    public function handle()
    {
        $accounts = $this->accountCollection->createModel()->get()->pluck('id')->toArray();
        $accountIds = array_random($accounts, count($accounts) / 10);

        $this->postTouitsFor($accountIds, 1);
        $this->likeTouits($accountIds, random_int(5, 10));
        $this->followAccounts($accountIds, random_int(5, 50));
        $this->replyToTouit($accountIds, random_int(1, 3));
    }

    private function postTouitsFor(array $accountIds, int $num = 1): void
    {
        foreach ($accountIds as $accountId) {
            $this->bus->dispatch(PostTouit::withData(
                TouitId::nextIdentity()->toString(),
                $accountId,
                $this->faker->sentence(25, true)
            ));
        }

        $this->info(count($accountIds) * $num . ' touits posted.');
    }

    private function likeTouits(array $accountIds, int $num): void
    {
        $countTouits = 0;
        foreach ($accountIds as $accountId) {
            $favorites = $this->favoriteList->favoriteTouitListOfAccountId($accountId)->pluck('touit_id')->toArray();

            $touits = $this->touitCollection->createModel()
                ->whereNotIn('id', $favorites)
                ->orderByDesc('created_at')
                ->take($num)
                ->get();

            $touits->each(function (Touit $touit) use ($accountId) {
                $this->bus->dispatch(AccountFavoriteTouit::forTouit(
                    $touit->getId()->toString(),
                    $accountId
                ));
            });

            $countTouits += $touits->count();
        }

        $this->info($countTouits . ' touits was liked');
    }

    private function followAccounts(array $accountIds, int $num): void
    {
        $followCount = 0;

        foreach ($accountIds as $followerId) {

            $followings = $this->followerCollection->followersForAccount($followerId)->pluck('following_id')->toArray();

            $accounts = $this->accountCollection
                ->createModel()
                ->whereNotIn('id', $followings)
                ->orderByDesc('created_at')
                ->take($num)
                ->get();

            $accounts->each(function (Account $account) use ($followerId) {
                $this->bus->dispatch(FollowAccount::forAccount(
                    Uuid::uuid4()->toString(),
                    $followerId,
                    $account->getId()->identify()
                ));
            });

            $followCount += $accounts->count();
        }

        $this->info($followCount . ' accounts followed');
    }

    private function replyToTouit(array $accountIds, int $num): void
    {
        $touits = $this->touitCollection->createModel()
            ->orderByDesc('created_at')
            ->take(50)
            ->get();


        $touits->each(function (Touit $touit) use ($accountIds, $num) {
            foreach ($accountIds as $accountId) {
                for ($i = 0; $i < $num; $i++) {
                    $this->bus->dispatch(
                        ReplyToTouit::forTouit(
                            Uuid::uuid4()->toString(),
                            $touit->getId()->toString(),
                            $accountId,
                            $this->faker->sentence(10, true)
                        )
                    );
                }
            }
        });

        $this->info($touits->count() * count($accountIds) * $num . ' replies posted.');
    }
}