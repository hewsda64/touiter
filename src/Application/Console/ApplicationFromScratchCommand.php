<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Console;

use Illuminate\Console\Command;
use Illuminate\Database\Connection;

class ApplicationFromScratchCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'touiter:app-from-scratch';

    /**
     * @var string
     */
    protected $description = 'install app';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * ApplicationFromScratchCommand constructor.
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        parent::__construct();
        $this->connection = $connection;
    }

    public function handle(): void
    {
        $this->resetDatabase();
        $time = time();
        $this->seedBase();
        $this->registerAccounts(50);
        $this->postTouits(20);
        $this->createActivity();

        $pastTime = (int)(time() - $time);
        $this->info('Operations completed in ' . $pastTime. 's');
    }

    private function resetDatabase(): void
    {
        if (!$this->confirm('Your are going to loose any data on touiter db if exists?')) {
            $this->info('Operation !omg! aborted.');
            exit(0);
        }

        $this->connection->getPdo()->exec('DROP DATABASE touiter');
        $this->connection->getPdo()->exec('CREATE DATABASE touiter
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;');

        $this->call('cache:clear');
        $this->call('queue:restart');
        $this->connection->reconnect();
        $this->call('migrate', ['--database' => 'mysql']);
    }

    private function seedBase(): void
    {
        $this->call('touiter:create-role', ['role' => ['ROLE_USER', 'basic user permissions']]);
    }

    private function registerAccounts(int $num): void
    {
        $this->call('touiter:register-accounts', ['num' => $num]);

        $sleep = (int)floor($num / 4);
        $this->info('Waiting ' . $sleep . ' seconds');

        sleep($sleep);

        $this->call('touiter:activate-account');
    }

    private function postTouits(int $num): void
    {
        for ($i = 0; $i < $num; $i++) {
            $this->call('touiter:create-touits');
        }
    }

    private function createActivity(): void
    {
        $this->call('touiter:create-activity');
    }
}