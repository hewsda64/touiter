<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Console;

use Faker\Factory;
use Hewsda64\Touiter\Domain\Account\Command\RegisterAccount;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Activation\Repository\ActivationCollection;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;
use Illuminate\Console\Command;
use Prooph\ServiceBus\Exception\CommandDispatchException;

class CreateAccountsCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'touiter:register-accounts {num?}';

    /**
     * @var string
     */
    protected $description = 'Create x account(s) with default user role';

    /**
     * @var AsyncCommandBus
     */
    private $asyncCommandBus;

    /**
     * @var Factory
     */
    private $faker;

    /**
     * @var ActivationCollection
     */
    private $activationCollection;

    /**
     * RegisterAccountsCommand constructor.
     *
     * @param AsyncCommandBus $asyncCommandBus
     * @param ActivationCollection $activationCollection
     * @param Factory $faker
     * @internal param Factory $faker
     */
    public function __construct(AsyncCommandBus $asyncCommandBus,
                                ActivationCollection $activationCollection,
                                Factory $faker)
    {
        parent::__construct();

        $this->asyncCommandBus = $asyncCommandBus;
        $this->faker = $faker::create();
        $this->activationCollection = $activationCollection;
    }

    public function handle(): void
    {
        $num = $this->argument('num') ?? 10;

        $this->info('Going to create ' . $num . ' posts.');

        $this->output->progressStart($num);

        try {
            for ($i = 0; $i < $num; $i++) {

                $accountId = AccountId::nextIdentity()->identify();

                $this->asyncCommandBus->dispatch(RegisterAccount::withData(
                    $accountId, $this->faker->name, $this->faker->email, 'password', 'password'
                ));

                $this->output->progressAdvance();
            }
        } catch (CommandDispatchException $exception) {
            $this->error($exception->getPrevious()->getMessage());
        }

        $this->output->progressFinish();

        $this->info('All accounts successfully registered.');
    }
}