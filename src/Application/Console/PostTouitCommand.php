<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Console;

use Faker\Factory;
use Hewsda64\Touiter\Domain\Account\Account;
use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Touit\Command\PostTouit;
use Hewsda64\Touiter\Domain\Touit\Values\TouitId;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;
use Illuminate\Console\Command;
use Prooph\ServiceBus\Exception\CommandDispatchException;

class PostTouitCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'touiter:create-touits';

    /**
     * @var string
     */
    protected $description = 'Create tweets per any account';


    /**
     * @var AsyncCommandBus
     */
    private $asyncCommandBus;

    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var Factory
     */
    private $faker;

    /**
     * PostTouitCommand constructor.
     *
     * @param AsyncCommandBus $asyncAsyncCommandBus
     * @param AccountCollection $accountCollection
     * @param Factory $faker
     */
    public function __construct(AsyncCommandBus $asyncAsyncCommandBus, AccountCollection $accountCollection, Factory $faker)
    {
        parent::__construct();

        $this->asyncCommandBus = $asyncAsyncCommandBus;
        $this->accountCollection = $accountCollection;
        $this->faker = $faker:: create();
    }

    public function handle(): void
    {
        $accounts = $this->accountCollection->createModel()->get();
        $postNum = 1; // fixMe

        $this->info('Going to create ' . $postNum . 'posts for ' . $accounts->count() . '  accounts.');

        try {

            $accounts->each(function (Account $author) use ($postNum) {
                for ($i = 0; $i < $postNum; $i++) {

                    $this->asyncCommandBus->dispatch(
                        PostTouit::withData(
                            TouitId::nextIdentity()->toString(),
                            $author->getId()->identify(),
                            $this->faker->sentence(25, true)
                        ));
                }
            });

        } catch (CommandDispatchException $exception) {

            $message = $exception->getPrevious() ? $exception->getPrevious()->getMessage() : $exception->getMessage();
            $this->error($message);
            exit(0);
        }

        $this->info('All touits was successfully posted.');
    }
}