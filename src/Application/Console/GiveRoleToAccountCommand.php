<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Console;

use Hewsda64\Touiter\Domain\Account\Command\GiveRole;
use Hewsda64\Touiter\Domain\Account\Repository\AccountCollection;
use Hewsda64\Touiter\Domain\Account\Values\AccountId;
use Hewsda64\Touiter\Domain\Role\Repository\RoleList;
use Hewsda64\Touiter\Domain\Role\Value\RoleName;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;
use Illuminate\Console\Command;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Thrust\Security\Foundation\Exception\SecurityValidationException;

class GiveRoleToAccountCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'touiter:give-role {account-role*}';

    /**
     * @var string
     */
    protected $description = 'Give role(s) to account id';

    /**
     * @var AsyncCommandBus
     */
    private $commandBus;

    /**
     * @var AccountCollection
     */
    private $accountCollection;

    /**
     * @var RoleList
     */
    private $roleList;


    /**
     * GiveRoleToAccountCommand constructor.
     * @param AsyncCommandBus $commandBus
     * @param AccountCollection $accountCollection
     * @param RoleList $roleList
     */
    public function __construct(AsyncCommandBus $commandBus, AccountCollection $accountCollection, RoleList $roleList)
    {
        parent::__construct();

        $this->commandBus = $commandBus;
        $this->accountCollection = $accountCollection;
        $this->roleList = $roleList;
    }

    public function handle(): void
    {
        try {
            [$accountId, $roles] = $this->handleParameters($this->arguments()['account-role']);

            $this->commandBus->dispatch(
                GiveRole::forAccount($accountId->identify(), $roles)
            );

            $this->info('Role(s) was given.');

        } catch (CommandDispatchException | SecurityValidationException $exception) {

            $message = $exception->getPrevious()
                ? $exception->getPrevious()->getMessage()
                : $exception->getMessage();

            $this->error($message);
        }
    }

    private function handleParameters(array $arguments)
    {
        $accountId = AccountId::fromString($arguments[0]);
        $role = RoleName::fromString($arguments[1])->toString();

        return [$accountId, [$role]];
    }
}