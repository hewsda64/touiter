<?php

declare(strict_types=1);

namespace Hewsda64\Touiter\Application\Console;

use Hewsda64\Touiter\Domain\Role\Command\CreateRole;
use Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus;
use Illuminate\Console\Command;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Ramsey\Uuid\Uuid;

class CreateRoleCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'touiter:create-role {role*}';

    /**
     * @var string
     */
    protected $description = 'Create role with name and description';


    /**
     * @var AsyncCommandBus
     */
    private $asyncCommandBus;

    /**
     * CreateRoleCommand constructor.
     *
     * @param AsyncCommandBus $asyncAsyncCommandBus
     */
    public function __construct(AsyncCommandBus $asyncAsyncCommandBus)
    {
        parent::__construct();

        $this->asyncCommandBus = $asyncAsyncCommandBus;
    }

    public function handle(): void
    {
        try {
            $this->asyncCommandBus->dispatch(CreateRole::withData(
                Uuid::uuid4()->toString(),
                $role = $this->arguments()['role'][0],
                $this->arguments()['role'][1]
            ));

            $this->info(sprintf('Role %s created', $role));
        } catch (CommandDispatchException $exception) {
            $this->error($exception->getPrevious()->getMessage());
        }
    }
}