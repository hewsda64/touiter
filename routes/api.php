<?php

Route::get('profile/{user_name}/following_timeline',
    'Hewsda64\Touiter\Application\Http\Controllers\Api\FollowingTimelineOfProfileAction')
    ->name('api::profile-following-timeline');

Route::get('profile/{user_name}/timeline',
    'Hewsda64\Touiter\Application\Http\Controllers\Api\TimelineOfProfileAction')
    ->name('api::profile-timeline');


Route::get('profile/{user_name}/followings')->name('api::profile-followings');
Route::get('profile/{user_name}/followers')->name('api::profile-followers');


Route::get('touit/{touit_id}/screen_play')->name('api::touit-screenplay');
Route::get('touit/{touit_id}')->name('api::touit-display');

/// get profile public user card