<?php

use Illuminate\Support\Facades\Route;

Route::get('/',
    ['uses' => \Hewsda64\Touiter\Application\Http\Page\ProfileTimelinePage::class] )
    ->name('frontend.home');

Route::get('favorite_touit/{touit_id}',
    ['uses' => \Hewsda64\Touiter\Application\Http\Actions\Touit\FavoriteTouitAction::class])
    ->name('frontend.touit.favorite.store');

Route::post('send_touit',
    ['uses' => \Hewsda64\Touiter\Application\Http\Actions\Touit\PostTouitAction::class])
    ->name('frontend.post.store');

Route::post('reply_to_touit/{touit_id}',
    ['uses' => \Hewsda64\Touiter\Application\Http\Actions\Touit\ReplyToTouitAction::class])
    ->name('frontend.post.reply.store');

Route::get('touit_screen/{touit_id}',
    ['uses' => \Hewsda64\Touiter\Application\Http\Page\TouitScreenPage::class])
    ->name('frontend.post.screen');

Route::get('follow_account/{following_id}',
    ['uses' => \Hewsda64\Touiter\Application\Http\Actions\Follow\FollowAccountAction::class])
    ->name('frontend.account.to_follow');

Route::get('account/settings',
    ['uses' => \Hewsda64\Touiter\Application\Http\Page\LocalPasswordSettingsPage::class])
    ->name('frontend.account.settings');

Route::get('auth/profile/change_password',
    ['uses' => \Hewsda64\Touiter\Application\Http\Page\LocalPasswordSettingsPage::class])
    ->middleware('grant:ROLE_USER')
    ->name('frontend.auth.profile.password');

Route::post('auth/profile/change_password',
    ['uses' => \Hewsda64\Touiter\Application\Http\Actions\Account\ChangeLocalPasswordAction::class])
    ->middleware('grant:ROLE_USER')
    ->name('frontend.auth.profile.password.post');

Route::get('signup',
    ['uses' => \Hewsda64\Touiter\Application\Http\Page\FrontAccountRegistrationPage::class])
    ->middleware('grant:anonymous')
    ->name('frontend.register');

Route::post('signup',
    ['uses' => \Hewsda64\Touiter\Application\Http\Actions\Account\AccountRegistrationAction::class])
    ->middleware('grant:anonymous')
    ->name('frontend.register.post');

Route::get('{user_name}/following',
    ['uses' => \Hewsda64\Touiter\Application\Http\Page\AccountFollowingsPage::class])
    ->name('frontend.profile.following.list');

Route::get('{user_name}/follower',
    ['uses' => \Hewsda64\Touiter\Application\Http\Page\AccountFollowersPage::class])
    ->name('frontend.profile.follower.list');

// must be last
Route::get('{user_name}',
    ['uses' => \Hewsda64\Touiter\Application\Http\Page\TimelineAccountPage::class])
    ->name('frontend.profile.timeline');
