<?php

use Illuminate\Support\Facades\Route;

Route::get('auth/login',
    ['uses' => \Hewsda64\Touiter\Application\Http\Page\FrontLoginPage::class])
    ->middleware('grant:anonymous')
    ->name('frontend.login');

Route::post('auth/login')
    ->middleware('grant:anonymous')
    ->name('frontend.auth.login.post');

Route::get('auth/account_activation/{activation_token}',
    ['uses' => \Hewsda64\Touiter\Application\Http\Actions\Account\AccountActivationAction::class])
    ->name('frontend.auth.account.activation');


