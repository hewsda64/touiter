<?php

return [
    'laraprooph' => [

        'bus_manager' => [

            'defaults' => [
                'command' => 'command_bus',
                'event' => 'event_bus',
                'query' => 'query_bus'
            ],

            'service_bus' => [

                'command' => [

                    /**
                     * Default bus
                     */
                    'command_bus' => [
                        'concrete' => \Prooph\ServiceBus\CommandBus::class,
                        'router' => [
                            'concrete' => \Prooph\ServiceBus\Plugin\Router\CommandRouter::class,
                        ],
                        'options' => [
                            'plugins' => [
                                \Hewsda64\Touiter\ServiceBus\Plugin\TransactionManager::class,
                                \Hewsda64\Touiter\ServiceBus\Plugin\RouteGuard::class,
                            ]
                        ]
                    ],

                    /**
                     * Async bus
                     */
                    'async_command_bus' => [
                        'concrete' => \Hewsda64\Touiter\ServiceBus\Bus\AsyncCommandBus::class,
                        'router' => [
                            'concrete' => \Prooph\ServiceBus\Plugin\Router\CommandRouter::class,
                            'async_switch' => \Hewsda64\Touiter\ServiceBus\Bus\LaravelMessageProducer::class
                        ],
                        'options' => [
                            'plugins' => [
                                \Prooph\ServiceBus\Plugin\MessageFactoryPlugin::class,
                                \Hewsda64\Touiter\ServiceBus\Plugin\TransactionManager::class,
                            ]
                        ]
                    ]
                ],

                'event' => [
                    'event_bus' => [
                        'concrete' => \Prooph\ServiceBus\EventBus::class,
                        'router' => [
                            'concrete' => \Prooph\ServiceBus\Plugin\Router\EventRouter::class,
                        ],
                        'options' => [
                            'plugins' => [
                                \Prooph\ServiceBus\Plugin\InvokeStrategy\OnEventStrategy::class,
                            ],
                        ]
                    ],

                ],

                'query' => [
                    'query_bus' => [
                        'concrete' => \Prooph\ServiceBus\QueryBus::class,
                        'router' => [
                            'concrete' => \Prooph\ServiceBus\Plugin\Router\QueryRouter::class,
                        ],
                    ],
                    'query_cache_bus' => [
                        'concrete' => \Hewsda64\Touiter\ServiceBus\Bus\QueryCacheBus::class,
                        'router' => [
                            'concrete' => \Prooph\ServiceBus\Plugin\Router\QueryRouter::class,
                        ],
                    ],
                ]
            ],


            'options' => [
                'enable_handler_location' => true,
                'message_factory' => \Prooph\Common\Messaging\MessageFactory::class,
                'plugins' => []
            ]
        ],

        'bus_router' => [

            'command_bus' => [
                'router' => [
                    'routes' => [
                        \Hewsda64\Touiter\Domain\Account\Command\RegisterAccount::class => \Hewsda64\Touiter\Domain\Account\Handler\RegisterAccountHandler::class,
                        \Hewsda64\Touiter\Domain\Account\Command\ChangeLocalPassword::class => \Hewsda64\Touiter\Domain\Account\Handler\ChangeLocalPasswordHandler::class,
                        \Hewsda64\Touiter\Domain\Account\Command\ActivateAccount::class => \Hewsda64\Touiter\Domain\Account\Handler\ActivateAccountHandler::class,

                    ],
                ],
            ],

            'async_command_bus' => [
                'router' => [
                    'routes' => [
                        \Hewsda64\Touiter\Domain\AccountStat\Command\CreateAccountStat::class => \Hewsda64\Touiter\Domain\AccountStat\Handler\CreateAccountStatHandler::class,
                        \Hewsda64\Touiter\Domain\Account\Command\GiveRole::class => \Hewsda64\Touiter\Domain\Account\Handler\GiveRoleHandler::class,
                        \Hewsda64\Touiter\Domain\Activation\Command\RegisterNotActivatedAccount::class => \Hewsda64\Touiter\Domain\Activation\Handler\RegisterNotActivatedAccountHandler::class,
                        \Hewsda64\Touiter\Domain\Activation\Command\DeleteAccountActivation::class => \Hewsda64\Touiter\Domain\Activation\Handler\DeleteAccountActivationHandler::class,
                        \Hewsda64\Touiter\Domain\Account\Command\ActivateAccount::class => \Hewsda64\Touiter\Domain\Account\Handler\ActivateAccountHandler::class,


                        \Hewsda64\Touiter\Domain\Role\Command\CreateRole::class => \Hewsda64\Touiter\Domain\Role\Handler\CreateRoleHandler::class,
                        \Hewsda64\Touiter\Domain\Touit\Command\PostTouit::class => \Hewsda64\Touiter\Domain\Touit\Handler\PostTouitHandler::class,
                        \Hewsda64\Touiter\Domain\AccountStat\Command\IncrementTouitCounter::class => \Hewsda64\Touiter\Domain\AccountStat\Handler\IncrementTouitCounterHandler::class,

                        \Hewsda64\Touiter\Domain\Follower\Command\FollowAccount::class => \Hewsda64\Touiter\Domain\Follower\Handler\FollowAccountHandler::class,

                        \Hewsda64\Touiter\Domain\Favorite\Command\AccountFavoriteTouit::class => \Hewsda64\Touiter\Domain\Favorite\Handler\AccountFavoriteTouitHandler::class,
                        \Hewsda64\Touiter\Domain\Touit\Command\IncrementFavoritesTouit::class => \Hewsda64\Touiter\Domain\Touit\Handler\IncrementFavoritesTouitHandler::class,
                        \Hewsda64\Touiter\Domain\AccountStat\Command\IncrementFavoritesAccount::class => \Hewsda64\Touiter\Domain\AccountStat\Handler\IncrementFavoritesAccountHandler::class,


                        \Hewsda64\Touiter\Domain\Touit\Command\ReplyToTouit::class => \Hewsda64\Touiter\Domain\Touit\Handler\ReplyToTouitHandler::class,
                        \Hewsda64\Touiter\Domain\Touit\Command\IncrementRepliesTouit::class => \Hewsda64\Touiter\Domain\Touit\Handler\IncrementRepliesTouitHandler::class,
                    ],
                ],
            ],

            'event_bus' => [
                'router' => [
                    'routes' => [
                        \Hewsda64\Touiter\Domain\Account\Event\AccountWasRegistered::class => [
                            \Hewsda64\Touiter\Projection\Account\AccountStore::class,
                            \Hewsda64\Touiter\Domain\Account\Process\CompleteRegistrationProcess::class,
                        ],

                        \Hewsda64\Touiter\Domain\AccountStat\Event\AccountStatWasCreated::class => [
                            \Hewsda64\Touiter\Projection\AccountStat\AccountStatStore::class,
                        ],

                        \Hewsda64\Touiter\Domain\Rolable\Event\RoleWasGiven::class => [
                            \Hewsda64\Touiter\Projection\Rolable\RolableStore::class
                        ],

                        \Hewsda64\Touiter\Domain\Role\Event\RoleWasCreated::class => [
                            \Hewsda64\Touiter\Projection\Role\RoleStore::class
                        ],

                        \Hewsda64\Touiter\Domain\Activation\Event\NotActivatedAccountWasRegistered::class => [
                            \Hewsda64\Touiter\Projection\Activation\ActivationStore::class
                        ],

                        \Hewsda64\Touiter\Domain\Account\Event\AccountWasActivated::class => [
                            \Hewsda64\Touiter\Projection\Account\AccountStore::class,
                            \Hewsda64\Touiter\Domain\Account\Process\CompleteActivationAccountProcess::class
                        ],

                        \Hewsda64\Touiter\Domain\Activation\Event\AccountActivationWasDeleted::class => [
                            \Hewsda64\Touiter\Projection\Activation\ActivationStore::class
                        ],

                        \Hewsda64\Touiter\Domain\Account\Event\LocalPasswordWasChanged::class => [
                            \Hewsda64\Touiter\Projection\Account\AccountStore::class
                        ],

                        \Hewsda64\Touiter\Domain\Touit\Event\TouitWasPosted::class => [
                            \Hewsda64\Touiter\Projection\Touit\TouitStore::class,
                            \Hewsda64\Touiter\Domain\Touit\Process\CompletePostTouitProcess::class,
                            //\Hewsda64\Touiter\Projection\TouiterCacheStore::class,
                        ],

                        \Hewsda64\Touiter\Domain\Touit\Event\ReplyToTouitWasPosted::class => [
                            \Hewsda64\Touiter\Projection\Touit\TouitStore::class,
                            \Hewsda64\Touiter\Domain\Touit\Process\CompleteReplyToTouitProcess::class,
                        ],

                        \Hewsda64\Touiter\Domain\Follower\Event\AccountWasFollowed::class => [
                            \Hewsda64\Touiter\Projection\Follower\FollowerStore::class,
                            \Hewsda64\Touiter\Projection\AccountStat\AccountStatStore::class,
                            //\Hewsda64\Touiter\Projection\TouiterCacheStore::class
                        ],

                        \Hewsda64\Touiter\Domain\AccountStat\Event\TouitCounterWasIncremented::class => [
                            \Hewsda64\Touiter\Projection\AccountStat\AccountStatStore::class
                        ],

                        \Hewsda64\Touiter\Domain\Favorite\Event\TouitWasFavoriteByAccount::class => [
                            \Hewsda64\Touiter\Projection\Favorite\FavoriteStore::class,
                            \Hewsda64\Touiter\Domain\Favorite\Process\FavoriteTouitProcess::class
                        ],

                        \Hewsda64\Touiter\Domain\Touit\Event\TouitWasFavorite::class => [
                            \Hewsda64\Touiter\Projection\Touit\TouitStore::class
                        ],

                        \Hewsda64\Touiter\Domain\AccountStat\Event\AccountFavoritesWasIncremented::class => [
                            \Hewsda64\Touiter\Projection\AccountStat\AccountStatStore::class
                        ],

                        \Hewsda64\Touiter\Domain\Touit\Event\RepliesCounterWasIncremented::class => [
                            \Hewsda64\Touiter\Projection\Touit\TouitStore::class
                        ]
                    ],
                ],
            ],

            'query_bus' => [
                'router' => [
                    'routes' => [
                        \Hewsda64\Touiter\Domain\Touit\Query\GetTimelineOfAccount::class => \Hewsda64\Touiter\Domain\Touit\Handler\GetTimelineOfAccountHandler::class,
                        \Hewsda64\Touiter\Domain\Touit\Query\GetFollowingTimelineOfAccount::class => \Hewsda64\Touiter\Domain\Touit\Handler\GetFollowingTimelineOfAccountHandler::class,
                        \Hewsda64\Touiter\Domain\Follower\Query\SuggestFollowingsToAccount::class => \Hewsda64\Touiter\Domain\Follower\Handler\SuggestFollowingsToAccountHandler::class,
                        \Hewsda64\Touiter\Domain\Touit\Query\GetTimelineOfTouit::class => \Hewsda64\Touiter\Domain\Touit\Handler\GetTimelineOfTouitHandler::class,
                        \Hewsda64\Touiter\Domain\Follower\Query\GetFollowersForAccount::class => \Hewsda64\Touiter\Domain\Follower\Handler\GetFollowersForAccountHandler::class,
                        \Hewsda64\Touiter\Domain\Follower\Query\GetFollowingsForAccount::class => \Hewsda64\Touiter\Domain\Follower\Handler\GetFollowingsForAccountHandler::class,
                        \Hewsda64\Touiter\Domain\Account\Query\GetAccountByUserName::class => \Hewsda64\Touiter\Domain\Account\Handler\GetAccountByUserNameHandler::class,
                    ],
                ],
            ],

            'query_cache_bus' => [
                'router' => [
                    'routes' => [
                        \Hewsda64\Touiter\Domain\Touit\Query\GetTimelineOfAccountCache::class => \Hewsda64\Touiter\Domain\Touit\Handler\GetTimelineOfAccountCacheHandler::class,
                        \Hewsda64\Touiter\Domain\Touit\Query\GetFollowingTimelineOfAccountCache::class => \Hewsda64\Touiter\Domain\Touit\Handler\GetFollowingTimelineOfAccountCacheHandler::class,
                        \Hewsda64\Touiter\Domain\Follower\Query\SuggestFollowingsToAccountCache::class => \Hewsda64\Touiter\Domain\Follower\Handler\SuggestFollowingsToAccountCacheHandler::class,

                    ]
                ]
            ]
        ]
    ]
];