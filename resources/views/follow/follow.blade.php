@extends('touiter::layout.full_with_top_profile')

@section('content')

    @if(isset($followAccounts) && $followAccounts->isNotEmpty())

        <div class="columns is-multiline is-mobile">

            @foreach($followAccounts as $followAccount)

                <div class="column is-half-mobile is-half-tablet is-one-third-desktop">

                    {{-- fixme --}}
                    @if(request()->is('*/follower'))
                        @include('touiter::follow.partial.__account_card', ['account' => $followAccount->follower])
                    @else
                        @include('touiter::follow.partial.__account_card', ['account' => $followAccount->following])
                    @endif
                </div>

            @endforeach

        </div>

    @else

        <p>No account to display</p>

    @endif

@endsection