<div class="card is-fullwidth">

    <header class="card-header"></header>

    <div class="card-content">

        <a class="card-avatar">

            <img src="{{ \Avatar::create($account->getUserName()->__toString())->toBase64() }}"/>

        </a>

        <div class="card-user">

            <div class="card-user-name">

                <a href="{{ route('frontend.profile.timeline', [$account->getId()]) }}">

                    {{ $account->getUserName() }}

                </a>

            </div>

            <span>

                  <a href="#">

                      @<span>{{ $account->getUserName() }}</span>

                  </a>

            </span>

            <span>Registered at {{ $account->createdAt()->format('M Y') }}</span>

        </div>

        <div class="card-stats">

            <ul class="card-stats-list">
                <li class="card-stats-item">
                    <a href="#" title="9.840 Tweet">
                        <span class="card-stats-key">Tweets</span>
                        <span class="card-stats-val">{{ $account->statistic->countTouits() }}</span>
                    </a>
                </li>
                <li class="card-stats-item">
                    <a href="#" title="885 Following">
                        <span class="card-stats-key">Following</span>
                        <span class="card-stats-val">{{ $account->statistic->countFollowings() }}</span>
                    </a>
                </li>
                <li class="card-stats-item">
                    <a href="#">
                        <span class="card-stats-key">Followers</span>
                        <span class="card-stats-val">{{ $account->statistic->countFollowers() }}</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>