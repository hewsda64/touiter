
@extends('touiter::layout.main')

@section('content')

    @include('touiter::touit.partial.__post_touit')

    @include('touiter::touit.profile_timeline')

@endsection