@extends('touiter::layout.full')

@section('content')

    <form action="{{ route('frontend.auth.login.post') }}" method="post">

        {{ csrf_field() }}

        <div class="field">
            <label class="label">Email address</label>
            <div class="control">
                <input class="input" name="identifier" placeholder="Email address" value="hewsda64@gmail.com">
            </div>
        </div>

        <div class="field">
            <label class="label">Password</label>
            <div class="control">
                <input class="input" name="password" type="password" placeholder="xxxxxxxxxx" value="password">
            </div>
        </div>

        <div class="control">
            <button class="button is-primary">Login</button>
        </div>

        <div class="field is-grouped">
            <div class="control">
                <a href="{{ route('frontend.register') }}">
                    Register
                </a>
            </div>
            <div class="control">
                Forgot credentials
            </div>
        </div>

    </form>

@endsection