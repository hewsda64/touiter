@extends('touiter::layout.main')

@section('content')

    <h2>Change password</h2>

    <form action="{{ route('frontend.auth.profile.password.post') }}" method="post">

        {{ csrf_field() }}

        <div class="field">

            <label class="label">Current password</label>

            <div class="control">

                <input class="input" name="current_password" type="password">

            </div>

        </div>

        <div class="field">

            <label class="label">New password</label>

            <div class="control">

                <input class="input" name="new_password" type="password">

            </div>

        </div>

        <div class="field">

            <label class="label">New password confirmation</label>

            <div class="control">

                <input class="input" name="new_password_confirmation" type="password">

            </div>

        </div>

        <div class="control">

            <button class="button is-primary">Change password</button>

        </div>

    </form>

@endsection