@extends('touiter::layout.full')

@section('content')

    <form action="{{ route('frontend.register.post') }}" method="post">

        {{ csrf_field() }}

        <div class="field">
            <label class="label">Email address</label>
            <div class="control">
                <input class="input" name="identifier" placeholder="Email address" value="hewsda64@gmail.com">
            </div>
        </div>

        <div class="field">
            <label class="label">User name</label>
            <div class="control">
                <input class="input" name="name" placeholder="Email address" value="hewsda64">
            </div>
        </div>

        <div class="field">
            <label class="label">Password</label>
            <div class="control">
                <input class="input" name="password" type="password" placeholder="xxxxxxxxxx" value="password">
            </div>
        </div>

        <div class="field">
            <label class="label">Confirm Password</label>
            <div class="control">
                <input class="input" name="password_confirmation" type="password" placeholder="xxxxxxxxxx" value="password">
            </div>
        </div>

        <div class="control">
            <button class="button is-primary">Login</button>
        </div>

    </form>

@endsection
