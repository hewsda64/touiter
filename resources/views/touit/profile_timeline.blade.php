<div class="box">

    @if(isset($profileTimeline) && $profileTimeline->isNotEmpty())

        @each('touiter::touit.partial.__touit', $profileTimeline, 'post')

    @else

        <p>No touit to display.</p>

    @endif

</div>