@extends('touiter::layout.full')

@section('content')

    <div class="box">

        @if(isset($touitScreenTimeline) && null !== $touitScreenTimeline)

            @include('touiter::touit.partial.__touit', ['post' => $touitScreenTimeline])

            @each('touiter::touit.partial.__touit', $touitScreenTimeline->replies, 'post')

        @else

            <p>No touit to display.</p>

        @endif

    </div>

@endsection