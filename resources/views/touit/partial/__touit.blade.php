@isset($post)

    <article class="media">

        <div class="media-left">

            <figure class="image is-64x64">

                <img src="{{ \Avatar::create($post->author->getUserName()->__toString())->toBase64() }}"/>

            </figure>

        </div>

        <div class="media-content" style="overflow: hidden;">

            <div class="content">

                <p>
                    <strong>{{ $post->author->getUserName() }}</strong>

                    <small>{{ $post->author->getEmail() }}</small>

                    <small style="float:right;">

                        {{ \Carbon\Carbon::createFromTimestamp($post->createdAt()->getTimestamp())->diffForHumans() }}

                    </small>

                    @if(null !== $post->reply_to_touit)

                        <br>
                        Response to ...

                    @endif

                    <br>

                    {!! Markdown::convertToHtml($post->body) !!}

                </p>

            </div>

            <nav class="level">

                <div class="level-left">

                    <a class="level-item" href="{{ route('frontend.post.screen', [$post->id]) }}">

                        <span class="icon is-small">

                            <i class="fa fa-reply"></i>

                        </span>

                        @if($post->getReplyCount() > 0)

                            {{ $post->getReplyCount() }}

                        @endif

                    </a>

                    <a class="level-item">

                        <span class="icon is-small">

                            <i class="fa fa-retweet"></i>

                        </span>

                    </a>

                    <a class="level-item" href="{{ route('frontend.touit.favorite.store', [$post->id]) }}">

                        <span class="icon is-small">

                            <i class="fa fa-heart"></i>

                        </span>

                        @if($post->getFavoriteCount() > 0)

                            {{ $post->getFavoriteCount() }}

                        @endif

                    </a>

                </div>

            </nav>

            <div class="media-content">

                @include('touiter::touit.partial.__reply_to_touit', ['touit' => $post])

            </div>

        </div>

    </article>

    @else

        <p>Touit is not available.</p>

        @endisset

