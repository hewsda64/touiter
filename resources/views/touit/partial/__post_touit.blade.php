<form action="{{ route('frontend.post.store') }}" method="post">

    {{ csrf_field() }}

    <div class="field">

        <p class="control">

            <textarea id="post_touit" class="textarea" placeholder="What's new today?" name="body"></textarea>

        </p>

    </div>

    <div class="field is-clearfix">

        <p class="control is-pulled-right">

            <button class="button is-primary">Post</button>

        </p>

    </div>

    <br>

</form>

<script>
    var simplemde = new SimpleMDE({ element: document.getElementById("post_touit") });
</script>