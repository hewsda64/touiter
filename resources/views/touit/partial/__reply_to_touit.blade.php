<form action="{{ route('frontend.post.reply.store',[$touit->getId()->toString()]) }}" method="post">

    {{ csrf_field() }}

    <div class="field">

        <p class="control">

            <input class="input" placeholder="Reply to {{ $touit->author->getUsername() }}" name="body">

        </p>

    </div>

    <div class="field is-clearfix">

        <p class="control is-pulled-right">

            <button class="button is-primary">Reply</button>

        </p>

    </div>

    <br>

</form>