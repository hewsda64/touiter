@extends('touiter::layout.template_home')

@section('template')

    <section class="section main">

        <div class="container">

            <div class="columns">

                <div class="column is-12">

                    @include('touiter::layout.common.notification')

                    @yield('content')

                </div>

            </div>

        </div>

    </section>

@endsection

