@if(Session::has('message'))

    <div class="notification is-primary">

        <button class="delete" onclick="((this).parentNode.remove())"></button>

        {{ Session::get('message') }}

    </div>

@endif