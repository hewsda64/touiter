<p>Accounts Suggest</p>

@if($suggestToFollow->isNotEmpty())

    @foreach($suggestToFollow as $toFollow)

        <div class="columns">

            <div class="column is-3 is-marginless">

                <div class="image">

                    <img src="{{ \Avatar::create($toFollow->getUserName()->__toString())->toBase64() }}" />

                </div>

            </div>

            <div class="column is-9">

                <p>

                    <a href="#">
                        <strong>{{ $toFollow->getUserName() }}</strong>
                        &commat;{{ $toFollow->getUserName() }}
                    </a>

                    <a href="#">
                        <i class="fa fa-times"></i>
                    </a>

                </p>

                <a class="button is-primary is-small" href="{{ route('frontend.account.to_follow',[$toFollow->getId()->identify()]) }}">
                  <span>
                    + Follow
                  </span>
                </a>

            </div>

        </div>

    @endforeach

@else

    <p>No suggestion.</p>

@endif
