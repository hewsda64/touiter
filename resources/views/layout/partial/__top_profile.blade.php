<div class="section profile-heading">

    <div class="columns">

        <div class="column is-2">

            <div class="image is-128x128 avatar">

                <img src="{{ \Avatar::create(getUser()->getUserName()->__toString())->toBase64() }}"/>

            </div>

        </div>

        <div class="column is-4 name">
            <p>
                <span class="title is-bold">{{ getUser()->getUserName() }}</span>
                <span class="button is-primary is-outlined follow">Follow</span>
            </p>
            <p class="tagline">The users profile bio would go here, of course. It could be two lines</p>
        </div>

        <div class="column is-2 likes has-text-centered">
            <p class="stat-val">{{ $accountCardLikeCount }}</p>
            <p class="stat-key">likes</p>
        </div>

        <div class="column is-2 followers has-text-centered">
            <p class="stat-val">{{ $accountCardFollowerCount }}</p>
            <p class="stat-key">followers</p>
        </div>

        <div class="column is-2 following has-text-centered">
            <p class="stat-val">{{ $accountCardFollowingCount }}</p>
            <p class="stat-key">following</p>
        </div>


    </div>

</div>