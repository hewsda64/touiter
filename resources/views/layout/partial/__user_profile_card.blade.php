<div class="card is-fullwidth">

    <header class="card-header"></header>

    <div class="card-content">

        <a class="card-avatar">
            <img src="https://marketplace.canva.com/MAB6v043Ud8/1/thumbnail/canva-robot-electric-avatar-icon-MAB6v043Ud8.png"
                 class="card-avatar-img">
        </a>

        <div class="card-user">
            <div class="card-user-name">
                <a href="{{ route('frontend.profile.timeline', [getUser()->getId()->identify()]) }}">
                    {{ getUser()->getUserName() }}
                </a>
            </div>
            <span>
                  <a href="#">
                      @<span>{{ getUser()->getUserName() }}</span>
                  </a>
            </span>
            <span>Registered at {{ getUser()->createdAt()->format('M Y') }}</span>
        </div>

        <div class="card-stats">

            <ul class="card-stats-list">
                <li class="card-stats-item">
                    <a href="#" title="9.840 Tweet">
                        <span class="card-stats-key">Tweets</span>
                        <span class="card-stats-val">{{ $accountCardTouitCount }}</span>
                    </a>
                </li>
                <li class="card-stats-item">
                    <a href="{{ route('frontend.profile.following.list', getUser()->getId()->identify()) }}">
                    <span class="card-stats-key">Following</span>
                        <span class="card-stats-val">{{ $accountCardFollowingCount }}</span>
                    </a>
                </li>
                <li class="card-stats-item">
                    <a href="{{ route('frontend.profile.follower.list', getUser()->getId()->identify()) }}">
                        <span class="card-stats-key">Followers</span>
                        <span class="card-stats-val">{{ $accountCardFollowerCount  }}</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>




