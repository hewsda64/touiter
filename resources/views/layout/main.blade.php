@extends('touiter::layout.template_home')

@section('template')

    <section class="section main">

        <div class="container">

            <div class="columns">

                <div class="column is-3">

                    @include('touiter::layout.partial.__col_left')

                </div>

                <div class="column is-6">

                    @include('touiter::layout.common.notification')

                    @yield('content')

                </div>

                <div class="column is-3">

                    @include('touiter::layout.partial.__col_right')

                </div>

            </div>

        </div>

    </section>

@endsection