<!doctype html>

<html lang="{{ config('app.locale') }}">

    @include('touiter::layout.common.__header')

    <body>

    @include('touiter::layout.common.navbar')

    @yield('template')

    @include('touiter::layout.common.__scripts')

    </body>

</html>