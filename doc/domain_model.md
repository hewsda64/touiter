# Domain Model

## Account
* User can register
* User can give role
* User can change password
* User can post touit
* User can delete touit
* User can reply to touit
* User can (un)favorite touit
* User can (un)follow user

### Local Registration
* User must be anonymous
* User register email, password, username, name, country

### User can follow user
* Followed User must exist
* Followed User must be fully enabled
* Following User must exist
* Following User must be fully enabled
* User can not follow himself
* Followed User must be unique