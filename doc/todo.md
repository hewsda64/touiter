# Todo

## Refactor 
* Consistency routes

## Account profile
* Add userName (constraint unique)
* Add country
* Add extended bio (account bio, interest ...)
    * Needed for trendings, suggest followings
* config profile homepage (background, avatar)

## Account Activation
* Created on user registration
* field: account_id, email, activation_token, created_at 

## Touit
* Parse touit (mention, short url, hashtag)
* Trends with elastic search (hashtag)

## Service
* elastic search
* Cdn with docker
* touit localization based user settings/profile

## Repository
* push http message middleware to his own repo
* Refactor model repo with decorator, event publisher, transaction manager

## Authorization
* Include authorization service